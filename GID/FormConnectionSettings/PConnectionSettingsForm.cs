﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GID;

namespace TaskApp.FormConnectionSettings
{
    public class PConnectionSettingsForm
    {
        IConnectionSettingsForm _connectionSettingsForm;
        GID.PMainForm _pMainForm;
        MConnectionSettingsForm mConnectionSettingsForm;



        public PConnectionSettingsForm(IConnectionSettingsForm connectionSettingsForm, GID.PMainForm pMainForm)
        {
            _connectionSettingsForm = connectionSettingsForm;
            _pMainForm = pMainForm;
            mConnectionSettingsForm = new MConnectionSettingsForm();

            _connectionSettingsForm.formLoad += _connectionSettingsForm_formLoad;
            _connectionSettingsForm.buttonOkClick += _connectionSettingsForm_buttonOkClick;
        }
        
        private void PChangePasswordForm_changePasswordSuccessfull(object sender, EventArgs e)
        {
            _connectionSettingsForm.textBoxPassword.Text = (string)sender;
            _pMainForm.model._ConnectionSettings.Password = (string)sender;
            SaveData();
        }

        private void _connectionSettingsForm_buttonOkClick(object sender, EventArgs e)
        {
            SaveData();
            _connectionSettingsForm.FormClose();
        }

        private void SaveData()
        {
            if (_pMainForm.model._ConnectionSettings.ServerName != _connectionSettingsForm.textBoxServerName.Text)
                _pMainForm.model._ConnectionSettings.ServerName = _connectionSettingsForm.textBoxServerName.Text;
            if (_pMainForm.model._ConnectionSettings.ServerPort != _connectionSettingsForm.textBoxPort.Text)
                _pMainForm.model._ConnectionSettings.ServerPort = _connectionSettingsForm.textBoxPort.Text;
            if (_pMainForm.model._ConnectionSettings.LoginName != _connectionSettingsForm.textBoxLogin.Text)
                _pMainForm.model._ConnectionSettings.LoginName = _connectionSettingsForm.textBoxLogin.Text.Trim();
            if (_pMainForm.model._ConnectionSettings.Password != _connectionSettingsForm.textBoxPassword.Text)
                _pMainForm.model._ConnectionSettings.Password = _connectionSettingsForm.textBoxPassword.Text;

            String plainText = _pMainForm.model._ConnectionSettings.Password;
            if (plainText != null)
            {
                String entropyText = Environment.UserName;
                byte[] plainBytes = Encoding.Unicode.GetBytes(plainText);
                byte[] entropyBytes = Encoding.Unicode.GetBytes(entropyText);
                byte[] encryptedBytes = ProtectedData.Protect(plainBytes, entropyBytes, DataProtectionScope.CurrentUser);
                _pMainForm.model.WriteBytesToFile(encryptedBytes);
            }

            _pMainForm.model.UpdateConnectionString();
        }

        private void _connectionSettingsForm_formLoad(object sender, EventArgs e)
        {
            _connectionSettingsForm.textBoxServerName.Text = _pMainForm.model._ConnectionSettings.ServerName;
            _connectionSettingsForm.textBoxPort.Text = _pMainForm.model._ConnectionSettings.ServerPort;
            _connectionSettingsForm.textBoxBase.Text = _pMainForm.model._ConnectionSettings.DatabaseName;
            _connectionSettingsForm.textBoxLogin.Text = _pMainForm.model._ConnectionSettings.LoginName;
            _connectionSettingsForm.textBoxPassword.Text = _pMainForm.model._ConnectionSettings.Password;
        }
    }
}

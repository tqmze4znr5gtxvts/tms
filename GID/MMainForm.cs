﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml.Serialization;
using TaskApp;
using TaskApp.FormConnectionSettings;

namespace GID
{
    public class MMainForm
    {
        public TaskPattern _TaskPattern { get; set; }

        public OleDbConnection myOleDbConnection { get; set; }
        public DataTable schemaTable { get; set; }
        public OleDbDataAdapter myOleDbDataAdapter { get; set; }
        
        public static bool BmcAutentication { get; set; }//индикатор авторизации
        public static bool AutoModeIndicator { get; set; }//индикатор автоматического режима(true - создаются инциденты, false - не создаются)
        public string Filename { get; set; }

        string connectionString = "";
        public string ConnectionString { get { return connectionString; } }

        private ConnectionSettings connectionSettings;
        public ConnectionSettings _ConnectionSettings { get { return connectionSettings; } }

        private SerializableDictionary<int, Service> service;
        public SerializableDictionary<int, Service> _Service { get { return service; } }
        
        private SerializableDictionary<int, WorkGroup> workGroup;
        public SerializableDictionary<int, WorkGroup> _WorkGroup { get { return workGroup; } }

        private SerializableDictionary<int, User> user;
        public SerializableDictionary<int, User> _User { get { return user; } }

        private string currentUser;
        public string CurrentUser { get { return currentUser; } }
        
        public readonly string tamplateRedFieldsErrorMessage = "Обязательные поля шаблона заполнены не корректно." + Environment.NewLine + "Заполните корректно шаблон и повторите попытку.";
        public readonly string tamplateFieldsErrorMessage = "Необязательные поля шаблона заполнены не корректно." + Environment.NewLine + "Заполните корректно шаблон и повторите попытку.";

        public event EventHandler<EventArgs> updateLists;
        public event EventHandler<ExtendedEventArgs> updateItemText;


        public MMainForm()
        {
            service = new SerializableDictionary<int, Service>();
            workGroup = new SerializableDictionary<int, WorkGroup>();
            user = new SerializableDictionary<int, User>();
        }

        public void GetConnectionSettings()
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(ConnectionSettings));
                if (File.Exists("ConnectionSettings.xml"))
                {
                    StreamReader reader = new StreamReader("ConnectionSettings.xml");
                    connectionSettings = (ConnectionSettings)serializer.Deserialize(reader);
                    reader.Close();
                }
                else
                {
                    connectionSettings = new ConnectionSettings();
                    //connectionSettings.DatabaseName = "DatabaseName";
                    connectionSettings.DatabaseName = "task";
                    connectionSettings.LoginName = "LoginName";
                    connectionSettings.ServerName = "ServerName";
                    connectionSettings.ServerPort = "ServerPort";
                }

                byte[] entropyBytes = Encoding.Unicode.GetBytes(Environment.UserName);
                byte[] encryptedData = ReadBytesFromFile();
                if (encryptedData != null)
                {
                    try
                    {
                        byte[] decryptedData = ProtectedData.Unprotect(encryptedData, entropyBytes, DataProtectionScope.CurrentUser);
                        connectionSettings.Password = Encoding.Unicode.GetString(decryptedData, 0, decryptedData.Length);
                    }
                    catch (Exception ex)
                    {
                        ErrorEventLog.LogError(ex.Message, "DeserializationSettings.txt");
                        MessageBox.Show(ex.Message);
                    }
                }
                connectionString = connectionSettings.UpdateConnectionString();

                if (GetDataFromDataBase())
                {
                    updateLists?.Invoke(null, EventArgs.Empty);
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "DeserializationSettings.txt");
                MessageBox.Show(ex.Message);
            }
        }

        public TaskPattern GetTaskPatternFromSelectedListViewItem(ListView listViewWithExcelList)
        {
            TaskPattern taskPattern = new TaskPattern();

            taskPattern.Theme = FormatString(_TaskPattern.Theme, listViewWithExcelList).Replace("\n", Environment.NewLine);
            taskPattern.Description = FormatString(_TaskPattern.Description, listViewWithExcelList).Replace("\n", Environment.NewLine);
            taskPattern._Service = FormatString(_TaskPattern._Service, listViewWithExcelList).Replace("\n", Environment.NewLine);
            taskPattern._WorkGroup = FormatString(_TaskPattern._WorkGroup, listViewWithExcelList).Replace("\n", Environment.NewLine);
            taskPattern.Executant = FormatString(_TaskPattern.Executant, listViewWithExcelList).Replace("\n", Environment.NewLine);
            taskPattern.AtachComment = FormatString(_TaskPattern.AtachComment, listViewWithExcelList).Replace("\n", Environment.NewLine);

            return taskPattern;
        }

        public Task GetTaskFromTaskPattern(TaskPattern pattern, ListViewItem item)
        {
            Service service = null;
            var tempval1 = _Service.Where(v => v.Value.Name == pattern._Service).ToArray();
            if (tempval1.Length > 0)
                service = tempval1[0].Value;
            if (service == null && tempval1.Length == 0 && pattern._Service != "")
            {
                updateItemText?.Invoke(item, new ExtendedEventArgs("   |Некорректный сервис"));
                //item.Text = item.Text + "   |Некорректный сервис";
                return null;
            }

            WorkGroup workGroup = null;
            var tempval2 = _WorkGroup.Where(v => v.Value.Name == pattern._WorkGroup).ToArray();
            if (tempval2.Length > 0)
                workGroup = tempval2[0].Value;
            if (workGroup == null && tempval2.Length == 0 && pattern._WorkGroup != "")
            {
                updateItemText?.Invoke(item, new ExtendedEventArgs("   |Некорректная рабочая группа"));
                //item.Text = item.Text + "   |Некорректная рабочая группа";
                return null;
            }

            User user = null;
            var tempval3 = _User.Where(v => v.Value.FullName == pattern.Executant).ToArray();
            if (tempval3.Length > 0)
                user = tempval3[0].Value;
            if (user == null && tempval3.Length == 0 && pattern.Executant != "")
            {
                updateItemText?.Invoke(item, new ExtendedEventArgs("   |Некорректный исполнитель"));
                //item.Text = item.Text + "   |Некорректный исполнитель";
                return null;
            }

            List<Comment> comments = new List<Comment>();
            if (pattern.AtachComment != "")
                comments.Add(new Comment(null, null, pattern.AtachComment, DateTime.Now));

            Status status = new Status();
            status.Id = 1;

            Task task = new Task(0, DateTime.Now, pattern.Theme, pattern.Description, comments, null, user, status, null, false, service, workGroup);

            return task;
        }

        public void CreateTask(Task task, ListViewItem item)
        {
            try
            {
                using (MySqlConnection conn = new MySqlConnection(ConnectionString))
                {
                    try
                    {
                        string sql = "INSERT INTO task (CreationDate, TaskText, CreatorUserID, ExecutantUserID, Status, Theme, Service, Workgroup, Critical, Source) VALUES ('" + 
                            DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "', '" +
                            task.Text + "', '" +
                            CurrentUser + "', " +
                            (task.ExecutantUser == null ? "null" : task.ExecutantUser.Id.ToString()) + ", '" +
                            task._Status.Id + "', '" +
                            task.Theme + "', " +
                            (task._Service == null ? "null" : task._Service.Id.ToString()) + ", " +
                            (task._WorkGroup == null ? "null" : task._WorkGroup.Id.ToString()) + ", 0, 2);select last_insert_id();";

                        MySqlCommand myCommand = new MySqlCommand(sql, conn);
                        conn.Open();

                        int id = Convert.ToInt32(myCommand.ExecuteScalar());


                        updateItemText?.Invoke(item, new ExtendedEventArgs("   |" + id));

                        if (task.Comments.Count > 0)
                        {
                            var res = AddComment(id, conn, task.Comments[0].CommentText);
                            if (res != "")
                                updateItemText?.Invoke(item, new ExtendedEventArgs(" | Ошибка добавления комментария: " + res));
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorEventLog.LogError(ex.Message, "addTask.txt");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private string AddComment(int taskID, MySqlConnection conn, string comment)
        {
            try
            {
                string sql = "INSERT INTO comment (taskId, userId, comentText, date) VALUES (" +
                    taskID + ", " + CurrentUser + ", '" + comment + "', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "')";
                MySqlCommand myCommand = new MySqlCommand(sql, conn);
                myCommand.ExecuteScalar();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return "";
        }

        public bool GetDataFromDataBase()
        {
            string CommandTextService = "SELECT * FROM service";
            string CommandTextWorkGroup = "SELECT * FROM workgroup";
            string CommandTextUser = "SELECT * FROM user ORDER BY fullName";

            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            {
                try
                {
                    conn.Open();
                    MySqlCommand myCommand;

                    myCommand = new MySqlCommand(CommandTextUser, conn);
                    var res = getUsers(conn, myCommand);

                    myCommand = new MySqlCommand(CommandTextService, conn);
                    var res2 = getServices(conn, myCommand);

                    myCommand = new MySqlCommand(CommandTextWorkGroup, conn);
                    var res3 = getWorkGroup(conn, myCommand);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    ErrorEventLog.LogError(ex.Message, "GetDataFromMySQL.txt");
                    return false;
                }
            }
            return true;
        }

        public bool getUsers(MySqlConnection conn, MySqlCommand myCommand)
        {
            try
            {
                using (MySqlDataReader Reader = myCommand.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        user.Add(Reader.GetInt32(0), new User(Reader.GetInt32(0), Reader.GetValue(1).ToString(), Reader.GetValue(2).ToString(), Reader.GetValue(3).ToString(), Reader.GetValue(4).ToString(), Reader.GetInt32(5), Reader.GetBoolean(6), Reader.GetInt32(7), Reader.GetBoolean(8)));
                        if (user.ElementAt(user.Count - 1).Value.Login == connectionSettings.LoginName)
                            currentUser = user.ElementAt(user.Count - 1).Value.Id.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "getUsers.txt");
                throw ex;
            }
            return true;
        }

        public bool getServices(MySqlConnection conn, MySqlCommand myCommand)
        {
            try
            {
                using (MySqlDataReader Reader = myCommand.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        int serviceId = Reader.GetInt32(0);
                        service.Add(serviceId, new Service(serviceId, Reader.GetValue(1).ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "getServices.txt");
                throw ex;
            }
            return true;
        }

        public bool getWorkGroup(MySqlConnection conn, MySqlCommand myCommand)
        {
            try
            {
                using (MySqlDataReader Reader = myCommand.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        int wgId = Reader.GetInt32(0);
                        workGroup.Add(wgId, new WorkGroup(wgId, Reader.GetValue(1).ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "getServices.txt");
                throw ex;
            }
            return true;
        }

        public byte[] ReadBytesFromFile()
        {
            string path = "TaskManager.dat";

            if (!File.Exists(path))
            {
                return null;
            }

            int nRead = 0;
            byte[] b = new byte[1024];
            using (FileStream fs = File.OpenRead(path))
            {
                nRead = fs.Read(b, 0, b.Length);
            }

            if (nRead > 0)
            {
                byte[] b2 = new byte[nRead];
                Array.Copy(b, b2, nRead);
                return b2;
            }
            else
                return null;

        }

        public void SaveConnectionjSettings()
        {
            try
            {
                XmlSerializer formatter = new XmlSerializer(typeof(ConnectionSettings));

                using (FileStream fs = new FileStream("ConnectionSettings.xml", FileMode.Create))
                {
                    formatter.Serialize(fs, connectionSettings);
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "SerilizationSettings.txt");
                MessageBox.Show(ex.Message);
            }
        }

        public void WriteBytesToFile(byte[] bytes)
        {
            string path = "TaskManager.dat";

            if (File.Exists(path))
                File.Delete(path);

            using (FileStream fs = File.Create(path))
            {
                try
                {
                    fs.Write(bytes, 0, bytes.Length);
                }
                catch (Exception ex)
                {
                    ErrorEventLog.LogError(ex.Message, "WriteBytesToFile.txt");
                }
            }
        }

        //Чтение из файла
        public static List<string[]> ReadFile(string filename)
        {
            List<string[]> res = new List<string[]>();
            try
            {
                using (StreamReader sr = new StreamReader(filename))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        res.Add(line.Split(new char[] { '~', ';', '\t' }));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return res;
        }

        //Инициализация Excel файла 
        public void LoadFile(string filename) //Открываем файл
        {
            // Строка подключения
            //НЕ РАБОТАЕТ
            //string ConnectionString = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties=\"Excel 8.0;HDR=NO\"; Data Source={0}", filename);
            //string ConnectionString = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties=\"Excel 8.0;HDR=NO;IMEX=1\"; Data Source={0}", filename);
            //string ConnectionString = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties=\"Excel 12.0 Xml;HDR=NO;IMEX=1\"; Data Source={0}", filename);

            string ConnectionString = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Extended Properties=\"Excel 12.0; HDR=NO; IMEX=1;\"; Data Source={0}", filename);

            // открываем соединение
            myOleDbConnection = new OleDbConnection(ConnectionString);
        }

        //Извлекаем информацию из файла
        public void GetData(CheckedListBox chlb)
        {
            if (myOleDbConnection.State == ConnectionState.Closed)
                myOleDbConnection.Open();

            // схема листов в документе
            schemaTable = myOleDbConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
            // Берем имя первого листа

            for (int i = 0; i < schemaTable.Rows.Count; i++)
            {
                chlb.Items.Add((string)schemaTable.Rows[i].ItemArray[2]);
            }

            string sheet1 = (string)schemaTable.Rows[0].ItemArray[2];

            if (myOleDbConnection.State == ConnectionState.Open)
                myOleDbConnection.Close();
        }

        /// <summary>
        /// Форматирование строки
        /// </summary>
        /// <param name="str">индекс в фигруных скобках из шаблона, который будет заменен на сообветствующую инфу из таблицы с информацией по инцидентам</param>
        /// <param name="selectOrIndex">не обязательный параметр индекс айтема в листвью при автоматическом режиме, в ручном режиме данный параметр не указывается.</param>
        /// <returns>Отформатированая строка, в которой индекс заменен соотвествующей информацией</returns>
        public string FormatString(string str, ListView lv, int selectOrIndex = -1, bool automode = false)
        {
            string res = "";

            try
            {
                Regex reg = new Regex(@"{+[\d]+}", RegexOptions.IgnoreCase);
                MatchCollection mc = reg.Matches(str);
                List<string> resList = new List<string>();
                foreach (Match mat in mc)
                {
                    if (mat.ToString().Length >= 3)
                    {
                        resList.Add(mat.ToString().Substring(1, mat.ToString().Length - 2));
                    }
                }

                res = str;
                
                for (int i = 0; i < resList.Count; i++)
                {
                    int indexResList = int.Parse(resList[i]);

                    if (selectOrIndex == -1)
                    {
                        if (indexResList < lv.SelectedItems[0].SubItems.Count)
                        {

                            if (indexResList == 0)
                            {
                                res = res.Replace("{" + resList[i] + "}", (string)lv.Invoke(new Func<string>(() => lv.SelectedItems[0].Text.Trim())));
                                int index = res.IndexOf('#');
                                if (index > 0)
                                    res = res.Substring(0, index);
                            }
                            else
                            {
                                res = res.Replace("{" + resList[i] + "}", (string)lv.Invoke(new Func<string>(() => lv.SelectedItems[0].SubItems[indexResList].Text.Trim())));
                            }
                        }
                        else
                            return "";
                    }
                    else
                    {
                        if (indexResList == 0)
                        {
                            res = res.Replace("{" + resList[i] + "}", (string)lv.Invoke(new Func<string>(() => lv.Items[selectOrIndex].Text.Trim())));
                            int index = res.IndexOf('#');
                            if (index > 0)
                                res = res.Substring(0, index);
                        }
                        else
                        {
                            res = res.Replace("{" + resList[i] + "}", (string)lv.Invoke(new Func<string>(() =>
                                         ((indexResList < lv.Items[selectOrIndex].SubItems.Count) ? lv.Items[selectOrIndex].SubItems[indexResList].Text.Trim() : ""))));
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Проверьте корректность настройки шаблона. " + ex.Message);
            }

            return res;
        }

        /// <summary>
        /// Сознание инцидента в системе BMC
        /// </summary>
        /// <param name="bmcPat">Ссылка на экземпляр класса шаблона</param>
        /// <param name="selectOrIndex">Не обязательный параметр индекс айтема в листвью</param>
        public void CreateIncidentBMC(TaskPattern bmcPat, ListView lv, int selectOrIndex = -1)
        {
            try
            {

            }
            catch (Exception ex)
            {
                if (selectOrIndex == -1)
                {
                    if (lv.SelectedItems.Count > 0)
                    {
                        lv.SelectedItems[0].Text = lv.SelectedItems[0].Text + " # " + ex.Message.Replace('~', '-');
                    }
                }
                else
                {
                    lv.Invoke(new System.Action(() => lv.Items[selectOrIndex].Text = lv.Items[selectOrIndex].Text + " # " + ex.Message.Replace('~', '-')));
                }
            }
        }

        //Добаление комментария к инциденту
        private string AddComment(string comment, string inc, string filename = "")
        {
            try
            {
              
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return "";
        }

        public void UpdateConnectionString(string pass = "", string user = "")
        {
            connectionString = "server=" + connectionSettings.ServerName +
                (connectionSettings.ServerPort != "" ? (";port=" + connectionSettings.ServerPort) : "") +
                ";user=" + (user == "" ? connectionSettings.LoginName : user) +
                ";database=" + connectionSettings.DatabaseName +
                ";password=" + (pass == "" ? connectionSettings.Password : pass) +
                ";" + "Allow Zero Datetime = True;charset=utf8;";
        }


        /// <summary>
        /// выгрузка в csv
        /// </summary>
        /// <param name="dg"></param>
        /// <param name="filename"></param>
        public void ExportCSV(ListView lw, string filename)
        {

            if (File.Exists(filename))
            {
                File.Delete(filename);
            }

            using (StreamWriter sw = new StreamWriter(filename, true))
            {
                for (int j = 0; j <= lw.Columns.Count - 1; j++)
                {
                    sw.Write(lw.Columns[j].Text + "~");
                }

                for (int i = 0; i <= lw.Items.Count - 1; i++)
                {
                    sw.WriteLine();
                    string str = "";

                    for (int j = 0; j <= lw.Columns.Count; j++)
                    {
                        if (j == lw.Columns.Count)
                        {
                            str = lw.Items[i].Tag != null ? lw.Items[i].Tag.ToString() + "1~" : "0~";
                            sw.Write(str);
                        }
                        else
                        {
                            str = lw.Items[i].SubItems[j].Text.Replace('\n', ' ') + "~";
                            sw.Write(str);
                        }
                    }
                }
            }

            MessageBox.Show("Выгрузка в csv файл завершена");
        }

        /// <summary>
        /// Логика используемая при автоматическом создании инцидентов.
        /// </summary>
        public void AutoCreateIncidents(ListView lv, ProgressBar pb, int timeout, bool flagTask)
        {
            for (int i = 0; i < lv.Items.Count; i++)
            {
                if (!AutoModeIndicator)
                    break;
                
                TaskPattern taskPattern = new TaskPattern();

                taskPattern.Description = FormatString(_TaskPattern.Description, lv, i).Replace("\n", Environment.NewLine);

                taskPattern.Theme = FormatString(_TaskPattern.Theme, lv, i).Replace("\n", Environment.NewLine) + ((flagTask) ? " [Задача]" : "");
                taskPattern._Service = FormatString(_TaskPattern._Service, lv, i).Replace("\n", Environment.NewLine);
                taskPattern.Executant = FormatString(_TaskPattern.Executant, lv, i).Replace("\n", Environment.NewLine);
                taskPattern._WorkGroup = FormatString(_TaskPattern._WorkGroup, lv, i).Replace("\n", Environment.NewLine);
                taskPattern.AtachComment = FormatString(_TaskPattern.AtachComment, lv, i).Replace("\n", Environment.NewLine);

                ListViewItem item = null;
                lv.Invoke(new Action(() => item = lv.Items[i]));

                var task = GetTaskFromTaskPattern(taskPattern, item);
                if (task != null)
                    CreateTask(task, item);

                System.Threading.Thread.Sleep(int.Parse(_TaskPattern.Timeout));
                
                UpdateStatistics(lv, pb);
            }
        }

        /// <summary>
        /// Обновление статистики
        /// </summary>
        private void UpdateStatistics(ListView lv, ProgressBar pb)
        {
            string str = "Всего - " + lv.Items.Count.ToString();
            int finishCount = 0;

            if (lv.Items.Count > 0)
            {
                lv.Invoke(new System.Action(() =>
                {
                    foreach (ListViewItem lvi in lv.Items)
                    {
                        if (lvi.Tag != null)
                            if ((int)lvi.Tag == 1)
                                finishCount++;
                    }
                }));

                str = str + " Обработанно - " + finishCount.ToString();
            }
            pb.Invoke(new System.Action(() =>  pb.Value = ((finishCount) * 100) / lv.Items.Count));
            //MessageBox.Show(str);
        }

        public SerializableDictionary<string, User> GetPeoples(string name)
        {
            try
            {
                SerializableDictionary<string, User> res = new SerializableDictionary<string, User>();

                
                return res;
            }
            catch(Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "Log\\GetPeoples.txt");
                return new SerializableDictionary<string, User>();
            }
        }

        public string changeUserPassword(MySqlConnection conn, MySqlCommand myCommand)
        {
            try
            {
                myCommand.ExecuteNonQuery();
                return "Пароль изменен.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        /// <summary>
        /// Получение версии программы
        /// </summary>
        /// <returns>Версия программы</returns>
        public string GetProgramVersion()
        {
            Assembly assem = Assembly.GetEntryAssembly();
            AssemblyName assemName = assem.GetName();
            Version ver = assemName.Version;
            return ver.ToString();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GID
{
    public class ExtendedEventArgs : EventArgs
    {
        private string text;
        public string Text { get { return text; } }

        public ExtendedEventArgs(string text)
        {
            this.text = text;
        }
    }
}

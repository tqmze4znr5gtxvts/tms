@echo off
mkdir C:\TMS\Upgrade\TaskApp
del /F /S /Q C:\TMS\Upgrade\TaskApp\*.*
copy "C:\Project\Visual Studio 2015\TaskProject\TaskApp\bin\Debug\*.*" C:\TMS\Upgrade\TaskApp\
del C:\TMS\Upgrade\TaskApp\*.pdb
del C:\TMS\Upgrade\TaskApp\Update.exe
del C:\TMS\Upgrade\TaskApp\Ionic.Zip.dll
del C:\TMS\Upgrade\TaskApp\DotNetZip.dll
del C:\TMS\Upgrade\TaskApp\ConnectionSettings.xml
del C:\TMS\Upgrade\TaskApp\TaskManager.dat
del C:\TMS\Upgrade\TaskApp\CreatorUsers.xml
del C:\TMS\Upgrade\TaskApp\ExecutantUsers.xml
del C:\TMS\Upgrade\TaskApp\Services.xml
del C:\TMS\Upgrade\TaskApp\Settings.xml
del C:\TMS\Upgrade\TaskApp\Statuses.xml
del C:\TMS\Upgrade\TaskApp\WorkGroup.xml
del C:\TMS\Upgrade\TaskApp\version
del C:\TMS\Upgrade\TaskApp.zip
rem -mx9: This means �ultra� compression. You probably want to use this.
"C:\Program Files\7-Zip\7z.exe" a -mx9 -r C:\TMS\Upgrade\TaskApp.zip C:\TMS\Upgrade\TaskApp\*.*
del /F /S /Q C:\TMS\Upgrade\TaskApp\*.*
rmdir C:\TMS\Upgrade\TaskApp
C:\Application\putty\pscp.exe -i C:\Application\putty\private_key.ppk C:\TMS\Upgrade\*.* cs@10.8.0.45:/var/local/www/html/Upgrade/
pause
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskApp.DBClasses;

namespace TaskApp
{
    public class Service : IValue, IComparable<Service>
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Service(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public string getValue()
        {
            return Name;
        }

        public int CompareTo(Service other)
        {
            return Name.CompareTo(other.Name);
        }

        public int getId()
        {
            return Id;
        }

        public Service()
        {

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskApp.DBClasses
{
    public class Event
    {
        public int EventID { get; set; }
        public Task _Task { get; set; }
        public DateTime CreationTime { get; set; }
        public User CreatorUser { get; set; }
        public DateTime EventTime { get; set; }
        public string EventText { get; set; }
        public int EventType { get; set; }
        public Dictionary<int, EventUser> ExecutantUsers { get; set; }



        public Event(int eventID, Task task, DateTime creationTime, User creatorUser, DateTime eventTime, string eventText, int eventType)
        {
            EventID = eventID;
            _Task = task;
            CreationTime = creationTime;
            CreatorUser = creatorUser;
            EventTime = eventTime;
            EventText = eventText;
            EventType = eventType;

            ExecutantUsers = new Dictionary<int, EventUser>();

            // _Task.eventCountChange(null, EventArgs.Empty);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskApp.DBClasses;

namespace TaskApp 
{
    [Serializable]
    public class Status : IValue, IComparable<Status>
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Status(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public string getValue()
        {
            return Name;
        }

        public int CompareTo(Status other)
        {
            return this.Name.CompareTo(other.Name);
        }

        public int getId()
        {
            return Id;
        }

        public Status()
        {

        }
    }
}

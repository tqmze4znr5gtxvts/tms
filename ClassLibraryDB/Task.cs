﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskApp.DBClasses;

namespace TaskApp
{
    public class Task : IComparable
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public string Theme { get; set; }
        public string  Text { get; set; }
        public List<Comment> Comments { get; set; }
        public User CreatorUser { get; set; }
        public User ExecutantUser { get; set; }
        public Status _Status { get; set; }
        public List<History> _History { get; set; }
        public bool Critical { get; set; }
        public Service _Service { get; set; }
        public WorkGroup _WorkGroup { get; set; }
        public Dictionary<int, Event> EventList { get; set; }

        public Task(int id, DateTime creationdate, string theme, string text, List<Comment> comments, User creatorUser, User executantUser, Status status, List<History> history, bool critical, Service service, WorkGroup workGroup)
        {
            Id = id;
            CreationDate = creationdate;
            Theme = theme;
            Text = text;
            Comments = comments;
            CreatorUser = creatorUser;
            ExecutantUser = executantUser;
            _Status = status;
            _History = history;
            Critical = critical;
            _Service = service;
            _WorkGroup = workGroup;
            EventList = new Dictionary<int, Event>();
        }

        public int CompareTo(object obj)
        {
            return this.CompareTo(obj);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskApp.DBClasses;

namespace TaskApp
{
    [Serializable]
    public class WorkGroup : IValue, IComparable<WorkGroup>
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public WorkGroup(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public string getValue()
        {
            return Name;
        }

        public int CompareTo(WorkGroup other)
        {
            return this.Name.CompareTo(other.Name);
        }

        public int getId()
        {
            return Id;
        }

        public WorkGroup()
        {

        }
    }
}

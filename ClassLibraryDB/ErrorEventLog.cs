﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskApp
{
    public static class ErrorEventLog
    {
        public static void LogError(string errorText, string filename)
        {
            if (!Directory.Exists("Log\\"))
            {
                Directory.CreateDirectory("Log\\");
            }

            using (StreamWriter sw = File.AppendText(filename == "" ? "Log\\Erorlog.txt" : "Log\\" + filename))
            {
                try
                {
                    sw.WriteLine(DateTime.Now.ToString() + ": " + errorText);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}

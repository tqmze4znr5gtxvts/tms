﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskApp.DBClasses;

namespace TaskApp
{
    public class Comment : IValue
    {
        public Task _Task { get; set; }
        public User _User { get; set; }
        public string CommentText { get; set; }
        public DateTime _DateTime { get; set; }

        public Comment(Task task, User user, string commentText, DateTime dateTime)
        {
            _Task = task;
            _User = user;
            CommentText = commentText;
            _DateTime = dateTime;
        }

        public string getValue()
        {
            return CommentText;
        }

        public int getId()
        {
            throw new NotImplementedException();
        }
    }
}

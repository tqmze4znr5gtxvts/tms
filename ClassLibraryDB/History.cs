﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskApp.DBClasses;

namespace TaskApp
{
    public class History : IValue
    {
        public Task _Task { get; set; }
        public DateTime _DateTime { get; set; }
        public User _User { get; set; }
        public string Description { get; set; }
        

        public History(Task task, User user, string description, DateTime dateTime)
        {
            _Task = task;
            _User = user;
            Description = description;
            _DateTime = dateTime;
        }

        public string getValue()
        {
            return Description;
        }

        public int getId()
        {
            throw new NotImplementedException();
        }
    }
}

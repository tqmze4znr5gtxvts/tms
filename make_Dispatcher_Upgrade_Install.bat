@echo off
mkdir C:\TMS\Upgrade_Master\TaskApp
del /F /S /Q C:\TMS\Upgrade_Master\TaskApp\*.*
copy "C:\Project\Visual Studio 2015\TaskProject\GID\bin\Debug\GID.exe" C:\TMS\Upgrade_Master\TaskApp\
copy "C:\Project\Visual Studio 2015\TaskProject\GID\bin\Debug\GID.vshost.exe" C:\TMS\Upgrade_Master\TaskApp\
copy "C:\Project\Visual Studio 2015\TaskProject\GID\bin\Debug\GID.exe.config" C:\TMS\Upgrade_Master\TaskApp\
copy "C:\Project\Visual Studio 2015\TaskProject\GID\bin\Debug\GID.vshost.exe.config" C:\TMS\Upgrade_Master\TaskApp\
copy "C:\Project\Visual Studio 2015\TaskProject\GID\bin\Debug\GID.vshost.exe.manifest" C:\TMS\Upgrade_Master\TaskApp\
copy "C:\Project\Visual Studio 2015\TaskProject\TaskApp\bin\Debug\*.*" C:\TMS\Upgrade_Master\TaskApp\

del C:\TMS\Upgrade_Master\TaskApp\*.pdb
del C:\TMS\Upgrade_Master\TaskApp\ConnectionSettings.xml
del C:\TMS\Upgrade_Master\TaskApp\TaskManager.dat
del C:\TMS\Upgrade_Master\TaskApp\CreatorUsers.xml
del C:\TMS\Upgrade_Master\TaskApp\ExecutantUsers.xml
del C:\TMS\Upgrade_Master\TaskApp\Services.xml
del C:\TMS\Upgrade_Master\TaskApp\Settings.xml
del C:\TMS\Upgrade_Master\TaskApp\Statuses.xml
del C:\TMS\Upgrade_Master\TaskApp\WorkGroup.xml
del C:\TMS\Upgrade_Master\TaskApp\version

del C:\TMS\Upgrade_Master\TaskApp_Install.zip
rem -mx9: This means �ultra� compression. You probably want to use this.
"C:\Program Files\7-Zip\7z.exe" a -mx9 -r C:\TMS\Upgrade_Master\TaskApp_Install.zip C:\TMS\Upgrade_Master\TaskApp\*.*

del C:\TMS\Upgrade_Master\TaskApp\Update.exe
del C:\TMS\Upgrade_Master\TaskApp\Ionic.Zip.dll
del C:\TMS\Upgrade_Master\TaskApp\DotNetZip.dll
del C:\TMS\Upgrade_Master\TaskApp\DotNetZip.xml

del C:\TMS\Upgrade_Master\TaskApp.zip
"C:\Program Files\7-Zip\7z.exe" a -mx9 -r C:\TMS\Upgrade_Master\TaskApp.zip C:\TMS\Upgrade_Master\TaskApp\*.*

del /F /S /Q C:\TMS\Upgrade_Master\TaskApp\*.*
rmdir C:\TMS\Upgrade_Master\TaskApp

C:\Application\putty\pscp.exe -i C:\Application\putty\private_key.ppk C:\TMS\Upgrade_Master\*.* cs@10.8.0.45:/var/www/local/html/Upgrade_Master/
pause
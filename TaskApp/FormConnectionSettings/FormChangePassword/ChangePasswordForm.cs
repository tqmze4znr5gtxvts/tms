﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskApp.FormConnectionSettings.FormChangePassword
{
    public partial class ChangePasswordForm : Form, IChangePasswordForm
    {
        public ChangePasswordForm()
        {
            InitializeComponent();
        }

        Button IChangePasswordForm.buttonOk
        {
            get { return buttonOk; }
            set { buttonOk = value; }
        }

        TextBox IChangePasswordForm.textBoxLogin
        {
            get { return textBoxLogin; }
            set { textBoxLogin = value; }
        }

        TextBox IChangePasswordForm.textBoxNewPassword
        {
            get { return textBoxNewPassword; }
            set { textBoxNewPassword = value; }
        }

        TextBox IChangePasswordForm.textBoxNewPasswordAgain
        {
            get { return textBoxNewPasswordAgain; }
            set { textBoxNewPasswordAgain = value; }
        }

        TextBox IChangePasswordForm.textBoxOldPassword
        {
            get { return textBoxOldPassword; }
            set { textBoxOldPassword = value; }
        }

        public event EventHandler<EventArgs> formLoad;

        public void formClose()
        {
            Close();
        }

        private void ChangePasswordForm_Load(object sender, EventArgs e)
        {
            formLoad?.Invoke(sender, e);
        }
    }
}

﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskApp.FormConnectionSettings.FormChangePassword
{
    public class PChangePasswordForm
    {
        IChangePasswordForm _changePasswordForm;
        PMainForm _pMainForm;
        MChangePasswordForm mChangePasswordForm;
        string userName;

        public event EventHandler<EventArgs> changePasswordSuccessfull;

        public PChangePasswordForm(IChangePasswordForm changePasswordForm, PMainForm pMainForm, string userName)
        {
            _changePasswordForm = changePasswordForm;
            _pMainForm = pMainForm;
            mChangePasswordForm = new MChangePasswordForm();
            this.userName = userName;

            _changePasswordForm.formLoad += _changePasswordForm_formLoad;
            _changePasswordForm.buttonOk.Click += ButtonOk_Click;
        }

        private async void ButtonOk_Click(object sender, EventArgs e)
        {
            if (_changePasswordForm.textBoxNewPassword.Text != _changePasswordForm.textBoxNewPasswordAgain.Text)
            {
                MessageBox.Show("Введенные пароли не совпадают.");
            }
            else
            {
                _pMainForm.model.UpdateConnectionString(_changePasswordForm.textBoxOldPassword.Text, _changePasswordForm.textBoxLogin.Text);
                using (MySqlConnection conn = new MySqlConnection(_pMainForm.model.ConnectionString))
                {
                    try
                    {
                        await conn.OpenAsync();
                        MySqlCommand command = new MySqlCommand("set password for '" + _changePasswordForm.textBoxLogin.Text + "' = '" + _changePasswordForm.textBoxNewPassword.Text + "';", conn);
                        string res = _pMainForm.model.changeUserPassword(conn, command);
                        changePasswordSuccessfull?.Invoke(_changePasswordForm.textBoxNewPassword.Text, EventArgs.Empty);
                        MessageBox.Show(res);
                        _changePasswordForm.formClose();
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException.Message.Contains("Access denied for user '"))
                            MessageBox.Show("Старый пароль для пользователя \"" + _changePasswordForm.textBoxLogin.Text + "\" задан не верно." + Environment.NewLine + "Или имя пользователя указано не верно.");
                        else
                            MessageBox.Show(ex.Message);
                        
                        _pMainForm.model.UpdateConnectionString(_pMainForm.model._ConnectionSettings.UpdateConnectionString());
                    }
                }
            }
        }

        private void _changePasswordForm_formLoad(object sender, EventArgs e)
        {
            _changePasswordForm.textBoxLogin.Text = userName;
        }
    }
}

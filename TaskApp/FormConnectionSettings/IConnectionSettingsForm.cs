﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskApp.FormConnectionSettings
{
    public interface IConnectionSettingsForm
    {
        //СВОЙСТВА//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        TextBox textBoxServerName { get; set; }
        TextBox textBoxPort { get; set; }
        TextBox textBoxBase { get; set; }
        TextBox textBoxLogin { get; set; }
        TextBox textBoxPassword { get; set; }


        //СОБЫТИЯ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        event EventHandler<EventArgs> formLoad;
        event EventHandler<EventArgs> buttonOkClick;
        event EventHandler<EventArgs> changePasswordLabelClick;

        //МЕТОДЫ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        void FormClose();
    }
}

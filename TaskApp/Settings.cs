﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskApp
{
    [Serializable]
    public class Settings
    {
        public int FormX1 { get; set; }
        public int FormY1 { get; set; }
        public int FormX2 { get; set; }
        public int FormY2 { get; set; }

        public int Splitter1 { get; set; }
        public int Splitter2 { get; set; }

        public int AutoUpdateInterval { get; set; }
        public bool AutoupdateIntervalState { get; set; }

        //размеры столбцов(ширина) таблиц
        public int TaskID { get; set; }
        public int TaskDate { get; set; }
        public int TaskTheme { get; set; }
        public int TaskDescription { get; set; }
        public int TaskCreator { get; set; }
        public int TaskExecutant { get; set; }
        public int TaskStatus { get; set; }
        public int TaskService { get; set; }
        public int TaskWorkGroup { get; set; }

        public int TaskFontSize { get; set; }
        public int HistoryFontSize { get; set; }
        public int CommentFontSize { get; set; }

        public int CommentDate { get; set; }
        public int CommentUser { get; set; }
        public int CommentDescription { get; set; }

        public int HistoryDate { get; set; }
        public int HistoryUser { get; set; }
        public int HistoryDescription { get; set; }

        public int[] DisplayIndexs { get; set; }

        //////////////////////////////////////////////

        public string UpdateServerURL { get; set; }


        public Settings()
        {

        }
    }
}

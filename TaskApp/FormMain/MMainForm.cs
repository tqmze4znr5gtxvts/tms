﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using TaskApp.DBClasses;
using System.Security.Cryptography;
using System.Reflection;
using TaskApp.FormReports;
using GID;
using System.Data;
using UpdateMe;

namespace TaskApp
{
    public class MMainForm
    {
        public readonly TimeSpan BeforeEventTime = new TimeSpan(0, 15, 0); //предаварийный таймаут(предупреждение)

        private Dictionary<int, string> eventType;
        public Dictionary<int, string> _EventType { get { return eventType; } }

        public delegate void updateComponents(bool state);

        string connectionString = "";

        string currentUser = "";//"125596413";
        public string CurrentUserID { get { return currentUser; } }

        int currentServices = 0;
        public int CurrentServices { get { return currentServices; } }

        int SortingField { get; set; }//индекс поля по которому будет происходить сортировка

        private List<Comment> comments;

        private SerializableDictionary<int, Status> status;
        public SerializableDictionary<int, Status> _Status { get { return status; } }

        private SerializableDictionary<int, Status> selectedStatus;
        public SerializableDictionary<int, Status> SelectedStatus { get { return selectedStatus; } }

        private SerializableDictionary<int, Service> service; 
        public SerializableDictionary<int, Service> _Service { get { return service; } }

        private SerializableDictionary<int, Service> allService;
        public SerializableDictionary<int, Service> _AllService { get { return allService; } }

        private SerializableDictionary<int, Source> source;
        public SerializableDictionary<int, Source> _Source { get { return source; } }

        private SerializableDictionary<int, WorkGroup> workGroup;
        public SerializableDictionary<int, WorkGroup> _WorkGroup { get { return workGroup; } }

        private SerializableDictionary<int, WorkGroup> selectedWorkGroup;
        public SerializableDictionary<int, WorkGroup> SelectedWorkGroup { get { return selectedWorkGroup; } }

        private SerializableDictionary<int, Service> selectedService;
        public SerializableDictionary<int, Service> SelectedService { get { return selectedService; } }

        private KeyValuePair<int, Task>[] taskSnapShot;
        public KeyValuePair<int, Task>[] TaskSnapShot { get { return taskSnapShot; } }

        private Dictionary<int, Task> dictionaryTempTask = new Dictionary<int, Task>();



        //Список всех пользователей в базе
        private SerializableDictionary<int, User> user;
        public SerializableDictionary<int, User> _User { get { return user; } }

        //Список всех неудаленных пользователей в базе
        private SerializableDictionary<int, User> realUser;
        public SerializableDictionary<int, User> _RealUser { get { return realUser; } }

        //Список исполнителей - только у кого есть хотя бы один сервис за который он отвечает
        private SerializableDictionary<int, User> executantUser;
        public SerializableDictionary<int, User> _ExecutantUser { get { return executantUser; } }

        //Список неудаленных исполнителей - только у кого есть хотя бы один сервис за который он отвечает
        private SerializableDictionary<int, User> realExecutantUser;
        public SerializableDictionary<int, User> _RealExecutantUser { get { return realExecutantUser; } }



        //Список неудаленных согласующих в базе
        private SerializableDictionary<int, User> userApproval;
        public SerializableDictionary<int, User> _UserApproval { get { return userApproval; } }


        private SerializableDictionary<int, User> selectedCreatorUser;
        public SerializableDictionary<int, User> SelectedCreatorUser { get { return selectedCreatorUser; } }

        private SerializableDictionary<int, User> selectedExecutantUser;
        public SerializableDictionary<int, User> SelectedExecutantUser { get { return selectedExecutantUser; } }

        public List<Comment> _Comment { get { return comments; } }

        public string ConnectionString { get { return connectionString; } }

        private readonly string botId = "https://api.telegram.org/bot263264363:AAGQ4TWM4Br9zZ5uUudpDJQW_zjUpcBamm8/sendMessage?chat_id=";//task
        

        public event EventHandler<EventArgs> updateListViewVirtualItemsSize;
        public event EventHandler<EventArgs> enabledListViewCommentAndHistory;
        public event EventHandler<EventArgs> updateStatusLabelComment;
        public event EventHandler<EventArgs> updateEventList;
        public event EventHandler<EventArgs> updateStatusLabelEvent;
        public event EventHandler<EventArgs> updateStatusLabelEventError;
        public event EventHandler<EventArgs> reloadTaskFromDataBase;
        public event EventHandler<EventArgs> startSerialization;

        public event EventHandler<EventArgs> endCreationEvent;
        public event EventHandler<EventArgs> endUpdateAcknowledge;
        
        private Settings settings;
        public Settings _Settings { get { return settings; } }

        private ConnectionSettings connectionSettings;
        public ConnectionSettings _ConnectionSettings { get { return connectionSettings; } }

        private List<string> version;
        public List<string> Version { get { return version; } }
        
        public MMainForm()
        {
            eventType = new Dictionary<int, string>();
            eventType.Add(0, "Для каждого");
            eventType.Add(1, "Для любого");

            settings = new Settings();
            SortingField = 3;
             status = new SerializableDictionary<int, Status>();
            user = new SerializableDictionary<int, User>();
            realUser = new SerializableDictionary<int, User>();
            executantUser = new SerializableDictionary<int, User>();
            realExecutantUser = new SerializableDictionary<int, User>();
            userApproval = new SerializableDictionary<int, User>();
            comments = new List<Comment>();
            service = new SerializableDictionary<int, Service>();
            allService = new SerializableDictionary<int, Service>();
            workGroup = new SerializableDictionary<int, WorkGroup>();
            source = new SerializableDictionary<int, Source>();
        }

        public bool getUsers(MySqlConnection conn, MySqlCommand myCommand)
        {
            try
            {
                using (MySqlDataReader Reader = myCommand.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        User new_user=new User(Reader.GetInt32(0), Reader.GetValue(1).ToString(), Reader.GetValue(2).ToString(), Reader.GetValue(3).ToString(), Reader.GetValue(4).ToString(), Reader.GetInt32(5), Reader.GetBoolean(6), Reader.GetInt32(7), Reader.GetBoolean(8) );
                        int id = Reader.GetInt32(0);
                        user.Add(id, new_user);
                        if (!new_user.Delete)
                        {
                            if (new_user.Approval)
                            {
                                userApproval.Add(id, new_user);
                            }
                            realUser.Add(id, new_user);
                        }

                        if (new_user.Services!=0)
                        {
                            executantUser.Add(id, new_user);
                            if (!new_user.Delete)
                            {
                                realExecutantUser.Add(id, new_user);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "getUsers.txt");
                throw ex;
            }
            return true;
        }

        public bool getStatuses(MySqlConnection conn, MySqlCommand myCommand)
        {
            try
            {
                using (MySqlDataReader Reader = myCommand.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        status.Add(Reader.GetInt32(0), new Status(Reader.GetInt32(0), Reader.GetValue(1).ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "getStatuses.txt");
                throw ex;
            }
            return true;
        }

        public bool getSource(MySqlConnection conn, MySqlCommand myCommand)
        {
            try
            {
                using (MySqlDataReader Reader = myCommand.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        source.Add(Reader.GetInt32(0), new Source(Reader.GetInt32(0), Reader.GetValue(1).ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "getSource.txt");
                throw ex;
            }
            return true;
        }

        public bool getServices(MySqlConnection conn, MySqlCommand myCommand)
        {
            try
            {
                using (MySqlDataReader Reader = myCommand.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        int serviceId = Reader.GetInt32(0);
                        Service _service = new Service(serviceId, Reader.GetValue(1).ToString());
                        allService.Add(serviceId, _service);
                        if (IsBitSet(CurrentServices, serviceId - 1))
                        {
                            service.Add(serviceId, _service);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "getServices.txt");
                throw ex;
            }
            return true;
        }

        public bool getWorkGroup(MySqlConnection conn, MySqlCommand myCommand)
        {
            try
            {
                using (MySqlDataReader Reader = myCommand.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        int wgId = Reader.GetInt32(0);
                        workGroup.Add(wgId, new WorkGroup(wgId, Reader.GetValue(1).ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "getServices.txt");
                throw ex;
            }
            return true;
        }

        public string changeUserPassword(MySqlConnection conn, MySqlCommand myCommand)
        {
            try
            {
                myCommand.ExecuteNonQuery();
                return "Пароль изменен.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public Dictionary<int, ReportTask> getReports(MySqlConnection conn, MySqlCommand myCommand)
        {
            Dictionary<int, ReportTask> reportsTask = new Dictionary<int, ReportTask>();
            myCommand.CommandTimeout = 3000;// В секундах = 50 минут
            //myCommand.CommandTimeout = 0;//Без TimeOut

            using (MySqlDataReader Reader = myCommand.ExecuteReader())
            {
                while (Reader.Read())
                {
                    Boolean errorRecord = false;
                    int id = Reader.GetInt32(0);
                    string tempDt = Reader.GetValue(1).ToString();
                    var dt = tempDt == "00.00.0000 0:00:00" ? new DateTime(2016, 01, 01, 0, 0, 0, 0) : DateTime.Parse(tempDt);
                    string text = Reader.GetValue(2).ToString();

                    int creator = Reader.GetInt32(3);
                    var temp = Reader.GetValue(4).ToString();
                    int executant = temp == "" ? 0 : Int32.Parse(temp);

                    User creatorUr = null;
                    try
                    {
                        creatorUr = user.Where((v) => v.Value.Id == creator).ToArray()[0].Value;
                    }
                    catch (Exception e)
                    {
                        Logger.Log.ErrorFormat("ID={0} creatorUr creator={1}", id, creator);
                        errorRecord = true;
                    };

                    User executantUr = null;
                    try
                    {
                        executantUr = executant != 0 ? executantUser.Where((v) => v.Value.Id == executant).ToArray()[0].Value : null;
                    }
                    catch (Exception e)
                    {
                        Logger.Log.ErrorFormat("ID={0} executantUr creator={1}", id, executant);
                        errorRecord = true;
                    };

                    var temp2 = Reader.GetInt32(5);
                    Status st = null;
                    try
                    {
                        st = status.Where((v) => v.Key == temp2).ToArray()[0].Value;
                    }
                    catch (Exception e)
                    {
                        Logger.Log.ErrorFormat("ID={0} Status temp2={1}", id, temp2);
                        errorRecord = true;
                    };

                    string th = Reader.GetValue(6).ToString();
                    bool critical = Reader.GetBoolean(7);

                    var serviceId = Reader.GetValue(8);
                    var t = serviceId.GetType();
                    Service _service = null;
                    if (!(serviceId is DBNull))
                    {
                        try
                        {
                            var sr = allService.Where(v => v.Value.Id == Convert.ToInt32(serviceId)).ToArray();
                            _service = sr[0].Value;
                        }
                        catch (Exception e)
                        {
                            Logger.Log.ErrorFormat("ID={0} Service serviceId={1}", id, serviceId);
                            errorRecord = true;
                        };
                    }

                    var sourceId = Reader.GetValue(9);
                    var tempsource = sourceId.GetType();
                    Source _source = null;
                    if (!(sourceId is DBNull))
                    {
                        try
                        {
                            var sr = source.Where(v => v.Value.Id == Convert.ToInt32(sourceId)).ToArray();
                            _source = sr[0].Value;
                        }
                        catch (Exception e)
                        {
                            Logger.Log.ErrorFormat("ID={0} Source sourceId={1}", id, sourceId);
                            errorRecord = true;
                        };
                    }

                    var workGroupId = Reader.GetValue(10);
                    var t2 = workGroupId.GetType();
                    WorkGroup _workGroup = null;
                    if (!(workGroupId is DBNull))
                    {
                        try
                        {
                            var twg = workGroup.Where(v => v.Value.Id == Convert.ToInt32(workGroupId)).ToArray();
                            _workGroup = twg[0].Value;
                        }
                        catch (Exception e)
                        {
                            Logger.Log.ErrorFormat("ID={0} workGroup workGroupId={1}", id, workGroupId);
                            errorRecord = true;
                        };
                    }

                    ReportTask reportTask = new ReportTask(id, _workGroup, _source, th, _service, text, critical, st, creatorUr, executantUr, dt, new List<Comment>(), new List<History>());
                    if (!errorRecord)
                    {
                        if (!reportsTask.ContainsKey(id))
                        {
                            reportsTask.Add(id, reportTask);
                        }
                    }
                }
            }

            return reportsTask;
        }

        public string GetTasksIdWithNotAcknowledgeEvents(MySqlConnection conn)
        {
            string result = "";
            try
            {
                string sql = "select DISTINCT task.TaskID from task, event, eventUsers where task.TaskID = event.TaskID and event.EventID = eventUsers.EventID and "+
                            "((event.Type = 0 and eventUsers.AcknowledgeComment is null) or "+
                            "(event.Type = 1 and (select count(*) from eventUsers where task.TaskID = event.TaskID and event.EventID = eventUsers.EventID and eventUsers.AcknowledgeComment is not null) = 0)) and"+
                            "(event.CreatorUser='" + CurrentUserID + "' or (select count(*) from eventUsers where task.TaskID = event.TaskID and event.EventID = eventUsers.EventID and eventUsers.UserID='" + CurrentUserID + "') > 0)" +
                            "order by task.TaskID";

                MySqlCommand myCommand = new MySqlCommand(sql, conn);

                using (MySqlDataReader Reader = myCommand.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        int id = Reader.GetInt32(0);
                        result += "TaskID=" + id + " OR ";
                    }
                }
            }
            catch(Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "GetTasksIdWithNotAcknowledgeEvents.txt");
            }
            return result.Length > 0 ? result.Substring(0, result.Length - 4) : result;
        }


        public bool getTasks(MySqlConnection conn, MySqlCommand myCommand)
        {
            const string txt = " Конфигурация в БД была изменена, для корректной работы перезапустите программу.";
            string errorText = "";
            try
            {
                myCommand.CommandTimeout = 300;
                dictionaryTempTask = new SerializableDictionary<int, Task>();
                using (MySqlDataReader Reader = myCommand.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        Boolean errorRecord = false;
                        int id = Reader.GetInt32(0);
                        string tempDt = Reader.GetValue(1).ToString();
                        var dt = tempDt == "00.00.0000 0:00:00" ? new DateTime(2016, 01, 01, 0, 0, 0, 0) : DateTime.Parse(tempDt);
                        string text = Reader.GetValue(2).ToString();

                        int creator = Reader.GetInt32(3);
                        var temp = Reader.GetValue(4).ToString();
                        int executant = temp == "" ? 0 : Int32.Parse(temp);

                        User creatorUr = null;
                        try
                        {
                            var creatorUsers = user.Where((v) => v.Value.Id == creator).ToArray();
                            errorText = creatorUsers.Length == 0 ? txt : "";
                            creatorUr = creatorUsers[0].Value;
                        }
                        catch (Exception e)
                        {
                            Logger.Log.ErrorFormat("ID={0} creatorUr creator={1}", id, creator);
                            errorRecord = true;
                        };

                        User executantUr = null;
                        try
                        {
                            executantUr = executant != 0 ? user.Where((v) => v.Value.Id == executant).ToArray()[0].Value : null;
                        }
                        catch (Exception e)
                        {
                            Logger.Log.ErrorFormat("ID={0} executantUr executant={1}", id, executant);
                            errorRecord = true;
                        };

                        var temp2 = Reader.GetInt32(5);

                        Status st = null;
                        try
                        {
                            st = status.Where((v) => v.Key == temp2).ToArray()[0].Value;
                        }
                        catch (Exception e)
                        {
                            Logger.Log.ErrorFormat("ID={0} Status temp2={1}", id, temp2);
                            errorRecord = true;
                        };

                        string th = Reader.GetValue(6).ToString();
                        bool critical = Reader.GetBoolean(7);

                        var serviceId = Reader.GetValue(8);
                        var t = serviceId.GetType();
                        Service _service = null;
                        if (!(serviceId is DBNull))
                        {
                            try
                            {
                                var sr = allService.Where(v => v.Value.Id == Convert.ToInt32(serviceId)).ToArray();
                                errorText = sr.Length == 0 ? txt : "";
                                _service = sr[0].Value;
                            }
                            catch (Exception e)
                            {
                                Logger.Log.ErrorFormat("ID={0} Service serviceId={1}", id, serviceId);
                                errorRecord = true;
                            };
                        }

                        var workGroupId = Reader.GetValue(10);
                        var t2 = workGroupId.GetType();
                        WorkGroup _workGroup = null;
                        if (!(workGroupId is DBNull))
                        {
                            try
                            {
                                var twg = workGroup.Where(v => v.Value.Id == Convert.ToInt32(workGroupId)).ToArray();
                                errorText = twg.Length == 0 ? txt : "";
                                _workGroup = twg[0].Value;
                            }
                            catch (Exception e)
                            {
                                Logger.Log.ErrorFormat("ID={0} workGroup workGroupId={1}", id, workGroupId);
                                errorRecord = true;
                            };

                        }

                        if (!errorRecord)
                        {
                            dictionaryTempTask.Add(id, new Task(id, dt, th, text, new List<Comment>(), creatorUr, executantUr, st, new List<History>(), critical, _service, _workGroup));
                        }
                    }
                    Reader.Close();
                }


                if (dictionaryTempTask.Count > 0)
                {
                    updateStatusLabelComment?.Invoke("Получение комментариев к задачам...", EventArgs.Empty);
                   // updateListViewCommentVirtualItemsSize?.Invoke(0, EventArgs.Empty);
                    var res = getComments(conn, new MySqlCommand("SELECT taskId, userId, comentText, date FROM comment" + getTasksIdForSqlWhere("taskId"), conn));
                    
                    updateStatusLabelComment?.Invoke("Получение истории изменений к задачам...", EventArgs.Empty);
                  //  updateListViewHistoryVirtualItemsSize?.Invoke(0, EventArgs.Empty);
                    var historyRes = getHistory(conn, new MySqlCommand("SELECT * FROM history" + getTasksIdForSqlWhere("Id"), conn));

                    updateStatusLabelComment?.Invoke("Получение событий...", EventArgs.Empty);
                    var EventsRes = getEvents(conn, new MySqlCommand("SELECT * FROM event" + getTasksIdForSqlWhere("TaskId"), conn));

                    updateStatusLabelComment?.Invoke("Получение списков пользователей для событий...", EventArgs.Empty);
                    GetEventUsers(conn);

                    enabledListViewCommentAndHistory?.Invoke(true, EventArgs.Empty);

                    updateEventList?.Invoke(null, EventArgs.Empty);
                }
                Sort(SortingField);
            }
            catch (Exception ex)
            {
                ex.Source = errorText;
                ErrorEventLog.LogError(ex.Message, "getTasks.txt");
                throw ex;
            }
            return true;
        }

        private bool getEvents(MySqlConnection conn, MySqlCommand myCommand)
        {
            try
            {
                using (MySqlDataReader Reader = myCommand.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        int tempID = Reader.GetInt32(4);
                        var user = _User.Where(v => v.Value.Id == tempID).ToArray();
                        if (user.Length <= 0)
                        {
                            ErrorEventLog.LogError("Пользователь не найден", "getEvent.txt");
                            continue;
                        }

                        User creatorUser = user[0].Value;

                        int taskId = Reader.GetInt32(1);
                        var ts = dictionaryTempTask.Where(v => v.Value.Id == taskId);
                        if (ts.Count() <= 0)
                            continue;
                        Task eventTask = ts.ElementAt(0).Value;

                        int id = Reader.GetInt32(0);
                        string dt = Reader.GetValue(2).ToString();
                        var creationTime = dt == "00.00.0000 0:00:00" ? new DateTime(2016, 01, 01, 0, 0, 0, 0) : DateTime.Parse(dt);
                        int type = Reader.GetInt32(3);
                        string dt2 = Reader.GetValue(5).ToString();
                        var eventTime = dt2 == "00.00.0000 0:00:00" ? new DateTime(2016, 01, 01, 0, 0, 0, 0) : DateTime.Parse(dt2);
                        string eventText = Reader.GetValue(6).ToString();

                        if (!eventTask.EventList.ContainsKey(id))
                            eventTask.EventList.Add(id, new Event(id, eventTask, creationTime, creatorUser, eventTime, eventText, type));
                        else
                            eventTask.EventList[id] = new Event(id, eventTask, creationTime, creatorUser, eventTime, eventText, type);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "getEvent.txt");
                throw ex;
            }
            return true;
        }

        private void GetEventUsers(MySqlConnection conn)
        {
            var userTaskWithEvents = dictionaryTempTask.Where(v => v.Value.EventList.Count > 0);


            string sql = "SELECT * from eventUsers WHERE ";

            if (userTaskWithEvents.Count() > 0)
            {
                sql = "SELECT * from eventUsers WHERE ";
                foreach (var userTask in userTaskWithEvents)
                {
                    foreach (var userEvent in userTask.Value.EventList)
                    {
                        sql += "EventID=" + userEvent.Value.EventID + " OR ";
                    }
                }
                sql = sql.Substring(0, sql.Length - 4);
            }
            else
                sql = "SELECT * from eventUsers";

            

            MySqlCommand myCommand = new MySqlCommand(sql, conn);

            try
            {
                using (MySqlDataReader Reader = myCommand.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        int id = Reader.GetInt32(0);
                        var temp = dictionaryTempTask.Where(v => v.Value.EventList.Where(v2 => v2.Value.EventID == id).Count() > 0).ToArray();
                        if (temp.Count() == 0)
                            continue;
                        var _event = temp[0].Value.EventList.Where(v => v.Value.EventID == id).ToArray();
                        int userId = Reader.GetInt32(1);
                        var tu = _User.Where(v => v.Value.Id == userId).ToArray();
                        if (tu.Length <= 0)
                        {
                            ErrorEventLog.LogError("Неудалось получить пользователя для события  id:" + id, "GetEventUsers.txt");
                            continue;
                        }

                        var tv = Reader.GetValue(2);

                        DateTime? acknowledgeTime = tv is DBNull ? null : (DateTime?)DateTime.Parse(tv.ToString());

                        var tempString = Reader.GetValue(3);
                        string acknowledgeText = tempString is DBNull ? null : tempString.ToString();

                        EventUser eu = new EventUser(_event[0].Value, tu[0].Value, acknowledgeTime, acknowledgeText);
                        if (!_event[0].Value.ExecutantUsers.ContainsKey(eu._User.Id))
                            _event[0].Value.ExecutantUsers.Add(eu._User.Id, eu);
                    }
                }

                RemoveEventWithOutMe();
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "GetEventUsers.txt");
                throw ex;
            }
        }

        public async Task<object> AcknowledgeEvent(string sql, Task eventForTask)
        {
            try
            {
                using (MySqlConnection conn = new MySqlConnection(ConnectionString))
                {
                    conn.Open();
                    MySqlCommand myCommand = new MySqlCommand(sql, conn);

                    int id = Convert.ToInt32(await myCommand.ExecuteScalarAsync());
                    getEventsForTask(eventForTask.Id.ToString(), id.ToString(), conn);

                //    RemoveEventWithOutMe(eventForTask);

                    updateEventList?.Invoke(null, EventArgs.Empty);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }

            endUpdateAcknowledge?.Invoke(null, EventArgs.Empty);
            return true;
        }

        public async Task<object> addEvent(string eventText, Dictionary<int, User> users, Task eventForTask, string eventTime, int type)
        {
            string sql = "INSERT INTO event (TaskID, CreatorUser, EventTime, EventText, Type) VALUES (\"" +
                 eventForTask.Id.ToString() + "\", \"" +
                 CurrentUserID + "\", '" +
                 eventTime + "', \"" +
                 eventText + "\", '" +
                 type.ToString() + "');select last_insert_id();";


            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            {
                try
                {
                    {
                        MySqlCommand myCommand = new MySqlCommand(sql, conn);
                        conn.Open();
                        int id = Convert.ToInt32(await myCommand.ExecuteScalarAsync());

                        string sqlUsers = "INSERT INTO eventUsers (EventID, UserID) VALUES ";

                        foreach (var user in users)
                        {
                            sqlUsers += "(" + id.ToString() + ", " + user.Key + "),";
                        }

                        sqlUsers = sqlUsers.Substring(0, sqlUsers.Length - 1) + ";";

                        myCommand = new MySqlCommand(sqlUsers, conn);
                        await myCommand.ExecuteScalarAsync();

                        getEventsForTask(eventForTask.Id.ToString(), id.ToString(), conn);

                      //  RemoveEventWithOutMe();

                        endCreationEvent?.Invoke(eventForTask, EventArgs.Empty);
                        updateEventList?.Invoke(null, EventArgs.Empty);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return false;
                }
            }
            return true;
        }

        private void getEventsForTask(string taskId, string eventId, MySqlConnection conn)
        {
            string sql = "select DISTINCT * from event, eventUsers where event.TaskID='" + taskId + "' and event.EventID = eventUsers.EventID and " +
                        "(event.CreatorUser='" + CurrentUserID + "' or (select count(*) from eventUsers where event.EventID = eventUsers.EventID and eventUsers.UserID='" + CurrentUserID + "') > 0) " +
                        "order by event.TaskID";

            try
            {
                Task eventTask = null;
                Dictionary<int, Event> eventList = new Dictionary<int, Event>();
                MySqlCommand myCommand = new MySqlCommand(sql, conn);
                using (MySqlDataReader Reader = myCommand.ExecuteReader())
                {

                    while (Reader.Read())
                    {
                        int tempID = Reader.GetInt32(4);
                        var user = _User.Where(v => v.Value.Id == tempID).ToArray();
                        if (user.Length <= 0)
                        {
                            ErrorEventLog.LogError("Пользователь не найден", "getEventsForTask.txt");
                            continue;
                        }

                        User creatorUser = user[0].Value;

                        int tId = Reader.GetInt32(1);
                        var ts = dictionaryTempTask.Where(v => v.Value.Id == tId);
                        if (ts.Count() <= 0)
                            continue;
                        eventTask = ts.ElementAt(0).Value;

                        int id = Reader.GetInt32(0);
                        string dt = Reader.GetValue(2).ToString();
                        var creationTime = dt == "00.00.0000 0:00:00" ? new DateTime(2016, 01, 01, 0, 0, 0, 0) : DateTime.Parse(dt);
                        int type = Reader.GetInt32(3);
                        string dt2 = Reader.GetValue(5).ToString();
                        var eventTime = dt2 == "00.00.0000 0:00:00" ? new DateTime(2016, 01, 01, 0, 0, 0, 0) : DateTime.Parse(dt2);
                        string eventText = Reader.GetValue(6).ToString();

                        if (!eventList.ContainsKey(id))
                            eventList.Add(id, new Event(id, eventTask, creationTime, creatorUser, eventTime, eventText, type));
                        else
                            eventList[id] = new Event(id, eventTask, creationTime, creatorUser, eventTime, eventText, type);
                    }
                }

                if (eventList.Count > 0)
                {
                    sql = "SELECT * FROM eventUsers WHERE ";

                    for (int i = 0; i < eventList.Count; i++)
                    {
                        sql += "EventID=" + eventList.ElementAt(i).Value.EventID + " OR ";
                    }

                    if (eventTask != null && eventList.Count > 0)
                        eventTask.EventList = eventList;

                    sql = sql.Substring(0, sql.Length - 4);

                    myCommand = new MySqlCommand(sql, conn);
                    using (MySqlDataReader Reader = myCommand.ExecuteReader())
                    {
                        while (Reader.Read())
                        {
                            int id = Reader.GetInt32(0);
                            var temp = dictionaryTempTask.Where(v => v.Value.EventList.Where(v2 => v2.Value.EventID == id).Count() > 0).ToArray();
                            if (temp.Count() == 0)
                                continue;
                            var _event = temp[0].Value.EventList.Where(v => v.Value.EventID == id).ToArray();
                            int userId = Reader.GetInt32(1);
                            var tu = _User.Where(v => v.Value.Id == userId).ToArray();
                            if (tu.Length <= 0)
                            {
                                ErrorEventLog.LogError("Неудалось получить пользователя для события  id:" + id, "getEvent.txt");
                                continue;
                            }


                            var tv = Reader.GetValue(2);
                            DateTime? acknowledgeTime = tv is DBNull ? null : (DateTime?)DateTime.Parse(tv.ToString());

                            var tempString = Reader.GetValue(3);
                            string acknowledgeText = tempString is DBNull ? null : (string)tempString;

                            EventUser eu = new EventUser(_event[0].Value, tu[0].Value, acknowledgeTime, acknowledgeText);
                            if (!_event[0].Value.ExecutantUsers.ContainsKey(eu._User.Id))
                                _event[0].Value.ExecutantUsers.Add(eu._User.Id, eu);
                            else
                                _event[0].Value.ExecutantUsers[eu._User.Id] = eu;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RemoveEventWithOutMe(Task eventTask = null)
        {
            if (eventTask == null)
            {
                var userTaskWithEvents = dictionaryTempTask.Where(v => v.Value.EventList.Count > 0);
                ///тут нужно проверять...........................................................................................................................
                foreach (var task in userTaskWithEvents)
                {
                    var eventsWithOutMe = task.Value.EventList.Where(v => v.Value.CreatorUser.Id.ToString() != CurrentUserID && v.Value.ExecutantUsers.Where(v2 => v2.Value._User.Id.ToString() == CurrentUserID).Count() == 0);
                    for (int i = eventsWithOutMe.Count() - 1; i >= 0; i--)
                    {
                        var v = eventsWithOutMe.ElementAt(i);
                        v.Value._Task.EventList.Remove(v.Key);
                    }
                }
            }
            else
            {
                var eventsWithOutMe = eventTask.EventList.Where(v => v.Value.CreatorUser.Id.ToString() != CurrentUserID && v.Value.ExecutantUsers.Where(v2 => v2.Value._User.Id.ToString() == CurrentUserID).Count() == 0);
                for (int i = eventsWithOutMe.Count() - 1; i >= 0; i--)
                {
                    var v = eventsWithOutMe.ElementAt(i);
                    v.Value._Task.EventList.Remove(v.Key);
                }
            }
        }

        private bool getComments(MySqlConnection conn, MySqlCommand myCommand)
        {
            try
            {
                using (MySqlDataReader Reader = myCommand.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        int id = Reader.GetInt32(0);
                        string tempDt = Reader.GetValue(3).ToString();
                        var dt = tempDt == "00.00.0000 0:00:00" ? new DateTime(2016, 01, 01, 0, 0, 0, 0) : DateTime.Parse(tempDt);
                        int userID = Reader.GetInt32(1);
                        var arrayUser = _User.Where(v => v.Value.Id == userID).ToArray();
                        if (arrayUser.Length <= 0)
                            continue;
                        User user = arrayUser[0].Value;
                        //var arrTask = TaskList.Where(v => v.Id == id).ToArray();
                        var arrTask = dictionaryTempTask.Where(v => v.Key == id).Select(v => v.Value).ToArray();
                        Task task = arrTask.Count() > 0 ? arrTask[0] : null;
                        if (task == null)
                            continue;
                        string text = Reader.GetValue(2).ToString();
                        Comment comment = new Comment(task, user, text, dt);
                        task.Comments.Add(comment);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "getComments.txt");
                throw ex;
            }
            return true;
        }

        private bool getHistory(MySqlConnection conn, MySqlCommand myCommand)
        {
            try
            {
                using (MySqlDataReader Reader = myCommand.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        int id = Reader.GetInt32(0);
                        string tempDt = Reader.GetValue(1).ToString();
                        var dt = tempDt == "00.00.0000 0:00:00" ? new DateTime(2016, 01, 01, 0, 0, 0, 0) : DateTime.Parse(tempDt);
                        int userID = Reader.GetInt32(2);

                        var arrayUser = _User.Where(v => v.Value.Id == userID).ToArray();
                        if (arrayUser.Length <= 0)
                            continue;

                        User user = arrayUser[0].Value;
                        string text = Reader.GetValue(3).ToString();
                        var arrTask = dictionaryTempTask.Where(v => v.Key == id).Select(v => v.Value).ToArray();
                        Task task = arrTask.Count() > 0 ? arrTask[0] : null;
                        if (task == null)
                            continue;
                        History history = new History(task, user, text, dt);
                        task._History.Add(history);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "getHistory.txt");
                throw ex;
            }
            return true;
        }

        
        public void addTask(string theme, string taskText, string creatorUserID, string executantUserID, string status, string service, bool critical, string workGroupId, bool tlgUnswer)
        {
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            {
                string stat = "";
                try
                {
                    string sql = (executantUserID == "" ? "INSERT INTO task (TaskText, CreatorUserID, Status, Theme, Critical, Service, Workgroup) VALUES (\"" :
                        "INSERT INTO task (TaskText, CreatorUserID, ExecutantUserID, Status, Theme, Critical, Service, Workgroup) VALUES (\"") +
                        taskText.Replace("\"", "\"\"") + "\", \"" +
                        creatorUserID + "\", \"" +
                        (executantUserID == "" ? "" : (executantUserID + "\", \"")) +
                        status + "\", \"" +
                        theme.Replace("\"", "\"\"") + "\", " +
                        (critical ? "1" : "0") + ", \"" +
                        service + "\", \"" + 
                        workGroupId + "\"); " + "select last_insert_id();";

                    MySqlCommand myCommand = new MySqlCommand(sql, conn);
                    conn.Open();

                    int id = Convert.ToInt32(myCommand.ExecuteScalar());

                    stat = DateTime.Now + " Добавление в БД - ОК"; 

                    var ur = _User.Where((v) => v.Value.Id.ToString() == creatorUserID).ToArray();

                    if (executantUserID != "")
                    {
                        string msg = "<b>Пользователь</b>: " + ur[0].Value.FullName +
                            " назначил задачу Вам на исполнение:\r\n" +
                            "<b>ID</b>: " + id + "\r\n" +
                            "<b>Тема</b>: " + theme + "\r\n" +
                            "<b>Описание</b>: " + taskText;

                        string key = "{\"inline_keyboard\":[[{\"text\":\"ID:" + id + "\", \"callback_data\":\"" + id + "\"}]]}";
                        string req = botId + executantUserID + "&text=" + msg.Replace("\"","").Replace("'", "") + "&parse_mode=HTML&reply_markup=" + key;
                        if (tlgUnswer)
                            stat += ". Отправка уведомления telegram - " + SendTelegramMessage(req);
                    }
                    updateStatusLabelEvent?.Invoke(stat, EventArgs.Empty);
                    reloadTaskFromDataBase?.Invoke(null, EventArgs.Empty);
                }
                catch (Exception ex)
                {
                    ErrorEventLog.LogError(ex.Message, "addTask.txt");
                    updateStatusLabelEventError?.Invoke(ex.Message, EventArgs.Empty);
                }
            }
        }

        public void addComment(Task task, string commentText)
        {
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            {
                string stat = "";
                try
                {
                    string sql = "INSERT INTO comment (taskId, userId, comentText) VALUES (\"" + task.Id + "\", \"" + currentUser + "\", \"" + commentText.Replace("\"", "\"\"") + "\")";
                    MySqlCommand myCommand = new MySqlCommand(sql, conn);
                    conn.Open();
                    int id = Convert.ToInt32(myCommand.ExecuteScalar());
                    stat = DateTime.Now + " Добавление в БД - ОК";
                    updateStatusLabelEvent?.Invoke(stat, EventArgs.Empty);

                    reloadTaskFromDataBase?.Invoke(null, EventArgs.Empty);

                    if (task.ExecutantUser != null)
                    {
                        var user = GetCurrentUserFullNameFromCurrentUserID();
                        string msg = "К Задаче ID: " + task.Id + " \nТема: " + task.Theme + " \nОписание: " +task.Text + 
                            (user != null ? "\nПользователем: " + user : "" )+
                            "\nдобавлен комментарий: " + commentText;
                        string key = "{\"inline_keyboard\":[[{\"text\":\"ID:" + task.Id + "\", \"callback_data\":\"" + task.Id + "\"}]]}";
                        string req = botId + task.ExecutantUser.Id + "&text=" + msg.Replace("\"", "").Replace("'", "") + "&reply_markup=" + key;
                        stat += ". Отправка уведомления telegram - " + SendTelegramMessage(req);
                    }
                    updateStatusLabelEvent?.Invoke(stat, EventArgs.Empty);
                }
                catch (Exception ex)
                {
                    ErrorEventLog.LogError(ex.Message, "addComment.txt");
                    updateStatusLabelEventError?.Invoke(ex.Message, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// Обновляет списки выбранными элементами в списках статус, пользователи, те что в параметрах запроса.
        /// </summary>
        /// <param name=""></param>
        public void UpdateSelectedValues(SerializableDictionary<int, Status> status, SerializableDictionary<int, Service> service, SerializableDictionary<int, User> creatorUsers, SerializableDictionary<int, User> executantUsers, SerializableDictionary<int, WorkGroup> workGroup)
        {
            selectedStatus = status;
            selectedService = service;
            selectedCreatorUser = creatorUsers;
            selectedExecutantUser = executantUsers;
            selectedWorkGroup = workGroup;
        }

        private string SendTelegramMessage(string request)
        {
            string result = "";
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(request);
            WebResponse response = null;
            try
            {
                response = httpWebRequest.GetResponse();
                result = ((HttpWebResponse)response).StatusDescription;

            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "SendTelegramMessage.txt");
                throw ex;
            }
            finally
            {
                if (response != null)
                    response.Close();
                httpWebRequest = null;
            }
            return result;
        }

        public void closeTask(Task curTask, bool tlgUnswer)
        {
            int idCloseStatus = (from t in status where t.Value.Name.Contains("Закрыт") select t).First().Value.Id;

            updTask(curTask.Theme, curTask.Text, curTask.CreatorUser.getId().ToString(), curTask.ExecutantUser.getId().ToString(), //curTask._Status.getId().ToString(),
                idCloseStatus.ToString(), 
                curTask._Service.getId().ToString(), curTask.Critical, curTask._WorkGroup.getId().ToString(), curTask, tlgUnswer);
            //В Telegram идёт отправка данных
        }

        public string GenerateToken(int length)
        {
            RNGCryptoServiceProvider cryptRNG = new RNGCryptoServiceProvider();
            byte[] tokenBuffer = new byte[length];
            cryptRNG.GetBytes(tokenBuffer);
            return Convert.ToBase64String(tokenBuffer).Replace("+", "0").Replace("/", "1");
        }

        public void updTask(string theme, string taskText, string creatorUserID, string executantUserID, string status, string service, bool critical, string workGroup, Task task, bool tlgUnswer)
        {
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            {
                string stat = "";
                try
                {
                    List<string> changes = new List<string>();

                    Boolean flagApprovalTask = false;

                    if (theme != task.Theme)
                        changes.Add("тема - старое значение: " + task.Theme.Replace("\"", "\"\"") + ", новое значение: " + theme.Replace("\"", "\"\""));
                    if (taskText != task.Text)
                        changes.Add("описание - старое значение: " + task.Text.Replace("\"", "\"\"") + ", новое значение: " + taskText.Replace("\"", "\"\""));
                    if (executantUserID != (task.ExecutantUser == null ? "" : task.ExecutantUser.Id.ToString()))
                    {
                        changes.Add("исполнитель - старое значение: " + (task.ExecutantUser == null ? "" : task.ExecutantUser.FullName) + ", новое значение: " + user.Where(v => v.Key.ToString() == executantUserID).ToArray()[0].Value.FullName);

                        //Если вдруг изменился исполнитель - Согласование?
                        flagApprovalTask = true;

                    }

                    if (status != task._Status.Id.ToString())
                    {
                        Status curStatus = this._Status.Where(v => v.Value.Id.ToString() == status).ToArray()[0].Value;
                        changes.Add("статус - старое значение: " + task._Status.Name + ", новое значение: " + curStatus.Name);
                        
                        //Если задача снята со согласования - вызов ХП для удаления из списка согласования
                        if ((task._Status.Id == 4) && (curStatus.Id != 4))
                        {
                            if (conn.State != ConnectionState.Open)
                                conn.Open();
                            MySqlCommand myCommand = new MySqlCommand("delete_task_approval", conn);
                            myCommand.CommandType = CommandType.StoredProcedure;
                            //myCommand.CommandText = "delete_task_approval";
                            myCommand.Parameters.Add(new MySqlParameter("@_TaskID", task.Id));
                            myCommand.ExecuteNonQuery();
                        }

                        flagApprovalTask = true;
                    }

                    if (critical != task.Critical)
                        changes.Add("критичность - старое значение: " + (task.Critical ? "критично" : "не критично") + ", новое значение: " + (critical ? "критично" : "не критично"));
                    if (task._Service == null || (service != task._Service.Id.ToString()))
                    {
                        var newService = _Service.Where(v => v.Value.Id.ToString() == service).ToArray();
                        changes.Add("сервис - старое значение: " + (task._Service == null ? "" : task._Service.Name) + ", новое значение: " + (newService.Length > 0 ? newService[0].Value.Name : "определить не удалось"));
                    }
                    if (task._WorkGroup == null || (workGroup != task._WorkGroup.Id.ToString()))
                    {
                        var newWorkGroup = _WorkGroup.Where(v => v.Value.Id.ToString() == workGroup).ToArray();
                        changes.Add("рабочая группа - старое значение: " + (task._WorkGroup == null ? "" : task._WorkGroup.Name) + ", новое значение: " + (newWorkGroup.Length > 0 ? newWorkGroup[0].Value.Name : "определить не удалось"));
                    }

                    //Если задача отправлена или непенаправлена на согласование - отправка письма
                    if ((flagApprovalTask) && (status.Equals("4")))
                    {
                        //Если статус Согласование - добавить в таблицу задач на согласование
                        if (conn.State != ConnectionState.Open)
                            conn.Open();
                        MySqlCommand myCommand = new MySqlCommand();
                        myCommand.Connection = conn;
                        myCommand.CommandType = CommandType.StoredProcedure;
                        myCommand.CommandText = "add_task_approval";
                        myCommand.Parameters.Add(new MySqlParameter("@_TaskID", task.Id));
                        //myCommand.Parameters.AddWithValue("@_TaskID", task.Id);
                        //myCommand.Parameters["@_TaskID"].Direction = ParameterDirection.Input;

                        myCommand.Parameters.Add(new MySqlParameter("@_UserID", currentUser));
                        //myCommand.Parameters.AddWithValue("@_UserID", currentUser);
                        //myCommand.Parameters["@_UserID"].Direction = ParameterDirection.Input;

                        //Защита на случай попытки одобрить задачу обманом и защита от переназначения на другого пользователя
                        string code = GenerateToken(12);//16 символов на выходе
                        myCommand.Parameters.Add(new MySqlParameter("@_CODE", code));

                        myCommand.Parameters.Add(new MySqlParameter("@_executantUserID", executantUserID));
                        myCommand.Parameters.Add(new MySqlParameter("@_Theme", theme));
                        myCommand.Parameters.Add(new MySqlParameter("@_TaskText", taskText));

                        //myCommand.ExecuteScalar();
                        myCommand.ExecuteNonQuery();
                    }


                    if (changes.Count > 0)
                    {
                        string sql = "UPDATE task SET Theme=\"" + theme.Replace("\"", "\"\"") + "\", TaskText=\"" + taskText.Replace("\"", "\"\"") + "\", Status=\"" + status + "\", Critical=" + (critical ? "1" : "0") + ", Service=\"" + service + "\"" + (executantUserID != "" ? ", ExecutantUserID=\"" + executantUserID + "\"" : "") + ", Workgroup=\"" + workGroup + "\" WHERE TaskID=" + task.Id.ToString();
                        //string sql = "UPDATE task SET Theme=\"" + theme + "\", TaskText=\"" + taskText + "\", Status=\"" + status + " WHERE TaskID=" + task.Id.ToString();
                     
                        MySqlCommand myCommand = new MySqlCommand(sql, conn);
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Open();
                        }

                        int id = Convert.ToInt32(myCommand.ExecuteScalar());
                        
                        sql = "INSERT INTO history (ID, UserID, Description) VALUES ";
                        foreach (var v in changes)
                        {
                            sql += "(\"" + task.Id + "\", \"" + currentUser + "\", \"" + v + "\"), ";
                        }

                        sql = sql.Substring(0, sql.Length - 2);
                        sql += ";";

                        myCommand = new MySqlCommand(sql, conn);
                        
                        id = Convert.ToInt32(myCommand.ExecuteScalar());
                        stat = DateTime.Now + " Обновление в БД - ОК";
                        updateStatusLabelEvent?.Invoke(stat, EventArgs.Empty);

                        if (executantUserID != "")
                        {
                            string msg = "Задача ID: " + task.Id + ", \nТема: " + task.Theme + ", \nОписание: " + taskText + "\r\n" + "Пользователь:\n" + GetCurrentUserFullNameFromCurrentUserID().ToString() +
                                "\nВнес изменения:\r\n";
                            foreach (var v in changes)
                            {
                                msg += v + "\r\n";
                            }

                            string key = "{\"inline_keyboard\":[[{\"text\":\"ID:" + task.Id + "\", \"callback_data\":\"" + task.Id + "\"}]]}";
                            string req = botId + executantUserID + "&text=" + msg.Replace("\"", "").Replace("'", "") + "&reply_markup=" + key;
                            if (tlgUnswer)
                                stat += ". Отправка уведомления telegram - " + SendTelegramMessage(req);
                            updateStatusLabelEvent?.Invoke(stat, EventArgs.Empty);
                        }
                    }
                    
                    reloadTaskFromDataBase?.Invoke(null, EventArgs.Empty);
                }
                catch (Exception ex)
                {
                    ErrorEventLog.LogError(ex.Message, "updTask.txt");
                    updateStatusLabelEventError?.Invoke(ex.Message, EventArgs.Empty);
                    MessageBox.Show(ex.Message);
                }
            }
        }

        public void DownloadAttachment(Comment comment, string fileName, string path)
        {
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            {
                int i = 0;
                try
                {
                    MySqlCommand logoCMD = new MySqlCommand("SELECT atachment FROM comment WHERE taskId=" + comment._Task.Id + " AND comentText='" + comment.CommentText + "'", conn);

                    FileStream fs;                          // Writes the BLOB to a file (*.bmp).
                    BinaryWriter bw;                        // Streams the BLOB to the FileStream object.
                    int bufferSize = 100;                    // Size of the BLOB buffer.
                    byte[] outbyte = new byte[bufferSize];  // The BLOB byte[] buffer to be filled by GetBytes.
                    long retval;                            // The bytes returned from GetBytes.
                    long startIndex = 0;                    // The starting position in the BLOB output.

                    conn.Open();
                    var myReader = logoCMD.ExecuteReader(CommandBehavior.SequentialAccess);

                    while (myReader.Read())
                    {
                        fs = new FileStream(path + "\\" + fileName, FileMode.Create, FileAccess.Write);
                        bw = new BinaryWriter(fs);

                        startIndex = 0;
                        retval = myReader.GetBytes(0, startIndex, outbyte, 0, bufferSize);

                        bool flag = true;
                        while (retval == bufferSize)
                        {
                            i++;
                            bw.Write(outbyte);
                            bw.Flush();
                            startIndex += bufferSize;
                            try
                            {
                                retval = myReader.GetBytes(0, startIndex, outbyte, 0, (int)retval);
                            }
                            catch
                            {
                                startIndex -= bufferSize;
                                flag = false;
                                break;
                            }
                            //if (i == 1526)
                            //{

                            //}
                            //if (retval != bufferSize)
                            //{

                            //}
                        }

                        if (flag)
                            bw.Write(outbyte, 0, (int)retval);
                        bw.Flush();
                        bw.Close();
                        fs.Close();
                    }

                    MessageBox.Show("Файл загружен.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        public void addAttachment(Task task, string filePath)
        {
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            {
                string stat = "";
                try
                {
                    string sql = "INSERT INTO comment (taskId, userId, comentText, atachment) VALUES (\"" + task.Id + "\", \"" + currentUser + "\", \"[Вложение]:" + Path.GetFileName(filePath) + "\", ?file)";
                    MySqlCommand myCommand = new MySqlCommand(sql, conn);
                    conn.Open();
                    byte[] file = File.ReadAllBytes(filePath);
                    MySqlParameter fileContentParameter = new MySqlParameter("?file", MySqlDbType.Blob, file.Length);
                    fileContentParameter.Value = file;
                    myCommand.Parameters.Add(fileContentParameter);

                    int id = Convert.ToInt32(myCommand.ExecuteScalar());
                    stat = DateTime.Now + " Добавление в БД - ОК";
                    updateStatusLabelEvent?.Invoke(stat, EventArgs.Empty);

                    reloadTaskFromDataBase?.Invoke(null, EventArgs.Empty);

                    if (task.ExecutantUser != null)
                    {
                        var user = GetCurrentUserFullNameFromCurrentUserID();
                        string msg = "К Задаче ID: " + task.Id + " \nТема: " + task.Theme + " \nОписание: " + task.Text + (user != null ? "\nПользователем: " + user : "") + "\nдобавлен комментарий в виде вложеного файла: " + Path.GetFileName(filePath);

                        string key = "{\"inline_keyboard\":[[{\"text\":\"ID:" + task.Id + "\", \"callback_data\":\"" + task.Id + "\"}]]}";
                        string req = botId + task.ExecutantUser.Id + "&text=" + msg.Replace("\'", "\\'").Replace("\"", "\\\"") + "&reply_markup=" + key;
                        stat += ". Отпкавка уведомления telegram - " + SendTelegramMessage(req);
                    }
                    updateStatusLabelEvent?.Invoke(stat, EventArgs.Empty);
                }
                catch (Exception ex)
                {
                    ErrorEventLog.LogError(ex.Message, "addComment.txt");
                    updateStatusLabelEventError?.Invoke(ex.Message, EventArgs.Empty);
                }
            }
        }


        public void ListViewRetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            try
            {
                ListViewItem lvi = new ListViewItem(new string[] { TaskSnapShot.ElementAt(e.ItemIndex).Value.Id.ToString(),
                    TaskSnapShot.ElementAt(e.ItemIndex).Value.CreationDate.ToString(),
                    TaskSnapShot.ElementAt(e.ItemIndex).Value.Theme,
                    TaskSnapShot.ElementAt(e.ItemIndex).Value.Text,
                    TaskSnapShot.ElementAt(e.ItemIndex).Value.CreatorUser.FullName,
                    TaskSnapShot.ElementAt(e.ItemIndex).Value.ExecutantUser == null ? "" : TaskSnapShot.ElementAt(e.ItemIndex).Value.ExecutantUser.FullName,
                    TaskSnapShot.ElementAt(e.ItemIndex).Value._Status.Name,
                    TaskSnapShot.ElementAt(e.ItemIndex).Value._Service == null ? "" : TaskSnapShot.ElementAt(e.ItemIndex).Value._Service.Name,
                    TaskSnapShot.ElementAt(e.ItemIndex).Value._WorkGroup == null ? "" : TaskSnapShot.ElementAt(e.ItemIndex).Value._WorkGroup.Name,
                    TaskSnapShot.ElementAt(e.ItemIndex).Value.EventList.Count.ToString()});

                e.Item = lvi;
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "ListViewRetrieveVirtualItem.txt");
            }
        }

        public void ListViewCommentRetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            try
            {
                int index = (int)sender;
                ListViewItem lvi = new ListViewItem(new string[] { TaskSnapShot.ElementAt(index).Value.Comments[e.ItemIndex]._DateTime.ToString(),
                    TaskSnapShot.ElementAt(index).Value.Comments[e.ItemIndex]._User.FullName,
                    TaskSnapShot.ElementAt(index).Value.Comments[e.ItemIndex].CommentText});
                e.Item = lvi;
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "ListViewCommentRetrieveVirtualItem.txt");
            }
        }

        public void ListViewHistoryRetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            try
            {
                int index = (int)sender;
                ListViewItem lvi = new ListViewItem(new string[] { TaskSnapShot.ElementAt(index).Value._History[e.ItemIndex]._DateTime.ToString(),
           TaskSnapShot.ElementAt(index).Value._History[e.ItemIndex]._User.FullName,
           TaskSnapShot.ElementAt(index).Value._History[e.ItemIndex].Description});
                e.Item = lvi;
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "ListViewHistoryRetrieveVirtualItem.txt");
            }
        }

        public string GetCurrentUserFullNameFromCurrentUserID()
        {
            var res = _User.Where(v => v.Key.ToString() == CurrentUserID).ToArray();
            return res.Length > 0 ? res[0].Value.FullName : null;
        }


        /// <summary>
        /// Обновляет айтемы переданного в параметре комбобокса, айтамами из словаря переданного во втором параметре
        /// </summary>
        /// <typeparam name="T">Класс который имплементирует интерфейс IValue</typeparam>
        /// <param name="cb">ComboBox который необходимо обновлять</param>
        /// <param name="list">Сериализуемый словарь, ключ которого типа int, значение класс имплементирующий интерфейс IValue</param>
        /// <param name="selectFirst">Не обязательный параметр, если указан true, то будет выбран первый элемент</param>
        public void updateComboBoxItems<T>(ComboBox cb, SerializableDictionary<int, T> list, bool selectFirst = false) where T : IValue
        {
            if (list.Count > 0)
            {
                cb.Invoke(new Action(() =>
                {
                    cb.Items.Clear();
                    cb.Items.Add("Все");
                    foreach (var obj in list)
                    {
                        cb.Items.Add(obj.Value.getValue());
                    }
                    if (selectFirst)
                        cb.SelectedIndex = 0;
                }));
            }
        }

        /// <summary>
        /// Универсальный метод инициализации чекед листбоксов
        /// </summary>
        /// <typeparam name="T">Тип с которым метод будет работать</typeparam>
        /// <param name="cb">Список для инициализации</param>
        /// <param name="list">Сериализуемый словарь элементами которого будет заполнен список</param>
        /// <param name="selectedList">Сериализуемый словарь элементов которые необходимо отметить в списке</param>
        /// <param name="emptyElement">параметр равен нулю по умолчанию, но если в списке претпологается пустой сервис, рабочая группа, или пользователь, то должен быть равен 1</param>
        public void updateCheckedListBoxItems<T>(CheckedListBox cb, Dictionary<int, T> list, SerializableDictionary<int, T> selectedList = null, int emptyElement = 0) where T : IValue
        {
            if (list.Count > 0)
            {
                cb.Invoke(new Action(() =>
                {
                    cb.Items.Clear();
                    if (selectedList != null)
                    {
                        if (list.Count != (selectedList.Count + emptyElement))
                            cb.Items.Add(new CheckListBoxItem(0, "Все"), selectedList.Count == 0 ? true : (selectedList.Count > list.Count ? true : false));
                        else
                            cb.Items.Add(new CheckListBoxItem(0, "Все"), true);
                    }
                    else
                    {
                        cb.Items.Add(new CheckListBoxItem(0, "Все"), true);
                    }
                    foreach (var obj in list)
                    {
                        bool selflag = selectedList != null ? (selectedList.Count > 0 ? false : true) : true;
                        if (selectedList != null && selectedList.Count > 0)
                        foreach(var selObj in selectedList)
                        {
                            if (selObj.Value != null)//в случае с сервисами например, где последний элемент пустой, для поиска задач с отсутствующим сервисом, валуе будет null, поэтому проверка
                                if ((obj.Value.getId() == selObj.Value.getId()) & (obj.Value.getValue() == selObj.Value.getValue()))
                                {
                                    selflag = true;
                                }
                        }
                        cb.Items.Add(new CheckListBoxItem(obj.Value.getId(), obj.Value.getValue()), selflag);
                    }
                }));
            }
        }

        /// <summary>
        /// Перебирает все задачи в списке собирая id и формируя строку к запросам извлечения комментариев и истории
        /// </summary>
        /// <param name="idName">Т.к. в БД поля id в таблицах называются по разному, данным параметром передается названия поля id таблицы, для которой формируется запрос</param>
        /// <returns>Строка содержащая часть sql запроса, начиная с " WHERE" или пустая строка, если список задач по каким-то причинам пуст</returns>
        private string getTasksIdForSqlWhere(string idName)
        {
            string res = "";

            foreach (var task in dictionaryTempTask)
            {
                res += idName + "=" + task.Value.Id + " OR ";
            }

            return res.Length > 0 ? " WHERE " + res.Substring(0, res.Length - 4) : "";
        }

        /// <summary>
        /// Сортировка
        /// </summary>
        /// <param name="field">Поле по которому требуется отсортировать соответствует индексу столбца</param>
        public void Sort(int field = 0)
        {
            if (SortingField != field)
                SortingField = field;

            if (dictionaryTempTask == null)
                return;

            enabledListViewCommentAndHistory?.Invoke(false, EventArgs.Empty);
            updateListViewVirtualItemsSize?.Invoke(null, EventArgs.Empty);

            taskSnapShot = dictionaryTempTask.ToArray();

            switch (field)
            {
                case 0:
                    taskSnapShot = taskSnapShot.OrderBy(v => v.Value.Id).OrderByDescending(v => v.Value.Critical).ToArray();
                    break;
                case 1:
                    taskSnapShot = taskSnapShot.OrderBy(v => v.Value.CreationDate).OrderByDescending(v => v.Value.Critical).ToArray();
                    break;
                case 2:
                    taskSnapShot = taskSnapShot.OrderBy(v => v.Value.Theme).OrderByDescending(v => v.Value.Critical).ToArray();
                    break;
                case 3:
                    taskSnapShot = taskSnapShot.OrderBy(v => v.Value.Text).OrderByDescending(v => v.Value.Critical).ToArray();
                    break;
                case 4:
                    taskSnapShot = taskSnapShot.OrderByDescending(v => v.Value.CreatorUser).OrderByDescending(v => v.Value.Critical).ToArray();
                    break;
                case 5:
                    taskSnapShot = taskSnapShot.OrderBy(v => v.Value.ExecutantUser).OrderByDescending(v => v.Value.Critical).ToArray();
                    break;
                case 6:
                    taskSnapShot = taskSnapShot.OrderBy(v => v.Value._Status).OrderByDescending(v => v.Value.Critical).ToArray();
                    break;
                case 7:
                    taskSnapShot = taskSnapShot.OrderBy(v => v.Value._Service).OrderByDescending(v => v.Value.Critical).ToArray();
                    break;
                case 8:
                    taskSnapShot = taskSnapShot.OrderBy(v => v.Value._WorkGroup).OrderByDescending(v => v.Value.Critical).ToArray();
                    break;
            }

            updateListViewVirtualItemsSize?.Invoke(taskSnapShot.Length, EventArgs.Empty);
            
        }

        /// <summary>
        /// Функция инициализирует поле currentUser, пользователем из таблицы в БД user который соответствует логину(юзеру в mysql), от имени этого пользователя будут рассылаться сообщения в телеграм
        /// </summary>
        public void UpdateCurrentUser()
        {
            if (_ConnectionSettings.LoginName != "")
            {
                var res = user.Where((v) => v.Value.Login.Equals(connectionSettings.LoginName)).ToArray();
                if (res.Length > 0)
                {
                    currentUser = res[0].Value.Id.ToString();
                    currentServices = res[0].Value.Services;
                }
            }
        }

        public void DeserializationSettings()
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Settings));
                if (File.Exists("Settings.xml"))
                {
                    StreamReader reader = new StreamReader("Settings.xml");
                    settings = (Settings)serializer.Deserialize(reader);
                    reader.Close();
                }
                else
                {
                    settings = new Settings();
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "DeserializationSettings.txt");
                MessageBox.Show(ex.Message);
            }

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(ConnectionSettings));
                if (File.Exists("ConnectionSettings.xml"))
                {
                    StreamReader reader = new StreamReader("ConnectionSettings.xml");
                    connectionSettings = (ConnectionSettings)serializer.Deserialize(reader);
                    reader.Close();
                }
                else
                {
                    connectionSettings = new ConnectionSettings();
                    connectionSettings.DatabaseName = "task";
                    connectionSettings.LoginName = "LoginName";
                    connectionSettings.ServerName = "ServerName";
                    connectionSettings.ServerPort = "ServerPort";
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "DeserializationSettings.txt");
                MessageBox.Show(ex.Message);
            }

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SerializableDictionary<int, Status>));
                if (File.Exists("Statuses.xml"))
                {
                    StreamReader reader = new StreamReader("Statuses.xml");
                    selectedStatus = (SerializableDictionary<int, Status>)serializer.Deserialize(reader);
                    reader.Close();
                }
                else
                {
                    selectedStatus = new SerializableDictionary<int, Status>();
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "DeserializationSettings.txt");
                MessageBox.Show(ex.Message);
            }
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SerializableDictionary<int, Service>));
                if (File.Exists("Services.xml"))
                {
                    StreamReader reader = new StreamReader("Services.xml");
                    selectedService = (SerializableDictionary<int, Service>)serializer.Deserialize(reader);
                    reader.Close();
                }
                else
                {
                    selectedService = new SerializableDictionary<int, Service>();
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "DeserializationSettings.txt");
                MessageBox.Show(ex.Message);
            }
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SerializableDictionary<int, WorkGroup>));
                if (File.Exists("WorkGroup.xml"))
                {
                    StreamReader reader = new StreamReader("WorkGroup.xml");
                    selectedWorkGroup = (SerializableDictionary<int, WorkGroup>)serializer.Deserialize(reader);
                    reader.Close();
                }
                else
                {
                    selectedWorkGroup = new SerializableDictionary<int, WorkGroup>();
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "DeserializationSettings.txt");
                MessageBox.Show(ex.Message);
            }
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SerializableDictionary<int, User>));
                if (File.Exists("CreatorUsers.xml"))
                {
                    StreamReader reader = new StreamReader("CreatorUsers.xml");
                    selectedCreatorUser = (SerializableDictionary<int, User>)serializer.Deserialize(reader);
                    reader.Close();
                }
                else
                {
                    selectedCreatorUser = new SerializableDictionary<int, User>();
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "DeserializationSettings.txt");
                MessageBox.Show(ex.Message);
            }
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SerializableDictionary<int, User>));
                if (File.Exists("ExecutantUsers.xml"))
                {
                    StreamReader reader = new StreamReader("ExecutantUsers.xml");
                    selectedExecutantUser = (SerializableDictionary<int, User>)serializer.Deserialize(reader);
                    reader.Close();
                }
                else
                {
                    selectedExecutantUser = new SerializableDictionary<int, User>();
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "DeserializationSettings.txt");
                MessageBox.Show(ex.Message);
            }


            byte[] entropyBytes = Encoding.Unicode.GetBytes(Environment.UserName);
            byte[] encryptedData = ReadBytesFromFile();
            if (encryptedData != null)
            {
                try
                {
                    byte[] decryptedData = ProtectedData.Unprotect(encryptedData, entropyBytes, DataProtectionScope.CurrentUser);
                    connectionSettings.Password = Encoding.Unicode.GetString(decryptedData, 0, decryptedData.Length);
                }
                catch (Exception ex)
                {
                    ErrorEventLog.LogError(ex.Message, "DeserializationSettings.txt");
                    MessageBox.Show(ex.Message);
                }
            }

            connectionString = connectionSettings.UpdateConnectionString();
        }

        public void UpdateConnectionString(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void UpdateConnectionString(string pass, string user)
        {
            connectionString = connectionSettings.UpdateConnectionString(pass, user);
        }
        
        public void SerilizationSettings()
        {
            try
            {
                XmlSerializer formatter = new XmlSerializer(typeof(Settings));

                using (FileStream fs = new FileStream("Settings.xml", FileMode.Create))
                {
                    formatter.Serialize(fs, settings);
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "SerilizationSettings.txt");
                MessageBox.Show(ex.Message);
            }

            try
            {
                XmlSerializer formatter = new XmlSerializer(typeof(ConnectionSettings));

                using (FileStream fs = new FileStream("ConnectionSettings.xml", FileMode.Create))
                {
                    formatter.Serialize(fs, connectionSettings);
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "SerilizationSettings.txt");
                MessageBox.Show(ex.Message);
            }


            startSerialization?.Invoke(null, EventArgs.Empty);

            if (selectedStatus != null)
            {
                try
                {
                    XmlSerializer formatter = new XmlSerializer(typeof(SerializableDictionary<int, Status>));

                    using (FileStream fs = new FileStream("Statuses.xml", FileMode.Create))
                    {
                        formatter.Serialize(fs, selectedStatus);
                    }
                }
                catch (Exception ex)
                {
                    ErrorEventLog.LogError(ex.Message, "SerilizationSettings.txt");
                    MessageBox.Show(ex.Message);
                }
            }

            if (selectedService != null)
            {
                try
                {
                    XmlSerializer formatter = new XmlSerializer(typeof(SerializableDictionary<int, Service>));

                    using (FileStream fs = new FileStream("Services.xml", FileMode.Create))
                    {
                        formatter.Serialize(fs, selectedService);
                    }
                }
                catch (Exception ex)
                {
                    ErrorEventLog.LogError(ex.Message, "SerilizationSettings.txt");
                    MessageBox.Show(ex.Message);
                }
            }

            if (selectedWorkGroup != null)
            {
                try
                {
                    XmlSerializer formatter = new XmlSerializer(typeof(SerializableDictionary<int, WorkGroup>));

                    using (FileStream fs = new FileStream("WorkGroup.xml", FileMode.Create))
                    {
                        formatter.Serialize(fs, selectedWorkGroup);
                    }
                }
                catch (Exception ex)
                {
                    ErrorEventLog.LogError(ex.Message, "SerilizationSettings.txt");
                    MessageBox.Show(ex.Message);
                }
            }

            if (selectedCreatorUser != null)
            {
                try
                {
                    XmlSerializer formatter = new XmlSerializer(typeof(SerializableDictionary<int, User>));

                    using (FileStream fs = new FileStream("CreatorUsers.xml", FileMode.Create))
                    {
                        formatter.Serialize(fs, selectedCreatorUser);
                    }
                }
                catch (Exception ex)
                {
                    ErrorEventLog.LogError(ex.Message, "SerilizationSettings.txt");
                    MessageBox.Show(ex.Message);
                }
            }

            if (selectedExecutantUser != null)
            {
                try
                {
                    XmlSerializer formatter = new XmlSerializer(typeof(SerializableDictionary<int, User>));

                    using (FileStream fs = new FileStream("ExecutantUsers.xml", FileMode.Create))
                    {
                        formatter.Serialize(fs, selectedExecutantUser);
                    }
                }
                catch (Exception ex)
                {
                    ErrorEventLog.LogError(ex.Message, "SerilizationSettings.txt");
                    MessageBox.Show(ex.Message);
                }
            }
        }

        public byte[] ReadBytesFromFile()
        {
            string path = "TaskManager.dat";

            if (!File.Exists(path))
            {
                return null;
            }

            int nRead = 0;
            byte[] b = new byte[1024];
            using (FileStream fs = File.OpenRead(path))
            {
                nRead = fs.Read(b, 0, b.Length);
            }

            if (nRead > 0)
            {
                byte[] b2 = new byte[nRead];
                Array.Copy(b, b2, nRead);
                return b2;
            }
            else
                return null;

        }

        public void WriteBytesToFile(byte[] bytes)
        {
            string path = "TaskManager.dat";

            if (File.Exists(path))
                File.Delete(path);

            using (FileStream fs = File.Create(path))
            {
                try
                {
                    fs.Write(bytes, 0, bytes.Length);
                }
                catch (Exception ex)
                {
                    ErrorEventLog.LogError(ex.Message, "WriteBytesToFile.txt");
                }
            }
        }

        /// <summary>
        /// Проверка установки бита в переменной
        /// </summary>
        /// <param name="b">переменная в которой требуется проверить установку бита</param>
        /// <param name="pos">номер бита который требуется проверить</param>
        /// <returns>если проверенный бит установлен - true иначе false</returns>
        private bool IsBitSet(int b, int pos)
        {
            //ErrorEventLog.LogError(b.ToString() + " - " + pos.ToString() + " - " + ((b & (1 << pos)) != 0).ToString(), "Events.txt");
            return (b & (1 << pos)) != 0;
        }

        /// <summary>
        /// Получение версии программы
        /// </summary>
        /// <returns>Версия программы</returns>
        public string GetProgramVersion()
        {
            Assembly assem = Assembly.GetEntryAssembly();
            AssemblyName assemName = assem.GetName();
            Version ver = assemName.Version;
            return ver.ToString();
        }

        /// <summary>
        /// выгрузка в csv
        /// </summary>
        /// <param name="dg"></param>
        /// <param name="filename"></param>
        public void ExportCSV(ListView lw, string filename)
        {

            if (File.Exists(filename))
            {
                File.Delete(filename);
            }

            try
            {
                using (StreamWriter sw = new StreamWriter(filename, true))
                {
                    for (int j = 0; j <= lw.Columns.Count - 1; j++)
                    {
                        sw.Write(lw.Columns[j].Text + "~");
                    }

                    for (int i = 0; i <= lw.Items.Count - 1; i++)
                    {
                        sw.WriteLine();
                        string str = "";

                        for (int j = 0; j <= lw.Columns.Count; j++)
                        {
                            if (j != lw.Columns.Count)
                            {
                                str = lw.Items[i].SubItems[j].Text.Replace('\n', ' ').Replace('\r', ' ') + "~";
                                sw.Write(str);
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "ExportCSV.txt");
            }

            MessageBox.Show("Выгрузка в csv файл завершена");
        }

        public void CheckNewVersion(string downloadsurl, string versionfilename)
        {
            version = update.getUpdateInfo(downloadsurl, versionfilename, Application.StartupPath + @"\", 1);            
        }
    }

    public class CheckListBoxItem
    {
        public object Tag { get; set; }
        public string Text { get; set; }

        public CheckListBoxItem(object tag, string text)
        {
            Tag = tag;
            Text = text;
        }

        public override string ToString() { return Text; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskApp
{
    public interface IMainForm
    {
        //СВОЙСТВА//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        string UpdateComment { get; set; }
        string UpdateCommentError { get; set; }
        string UpdateStatus { get; set; }
        string UpdateStatusError { get; set; }
        string CheckUpdateProgramStatus { get; set; }
        string CheckUpdateProgramStatusError { get; set; }
        int ListViewTaskVirtualListSize { get; set; }
        int ListViewCommentVirtualListSize { get; set; }
        string ListViewCommentTag { get; set; }
        string ListViewHistoryTag { get; set; }
        string FormText { get; set; }
        int ListViewHistoryVirtualListSize { get; set; }
        int ListViewTaskSelectedIndex { get; set; }
        int[] ListViewTaskSelectedIndexs { get; set; }
        int numericUpDownUpdateIntervalValue { get; }
        bool ToolStripButtonUpdateEnabled { get; set; }
        bool ListViewCommentEnabled { get; set; }
        bool ListViewHistoryEnabled { get; set; }
        bool CheckBoxAutoupdateChecked { get; set; }
        DateTimePicker DateTimePickerStartDate { get; set; }
        DateTimePicker DateTimePickerStartTime { get; set; }
        DateTimePicker DateTimePickerEndDate { get; set; }
        DateTimePicker DateTimePickerEndTime { get; set; }
        ComboBox ComboBoxCritical { get; set; }
        CheckedListBox checkedListBoxStatus { get; set; }
        CheckedListBox checkedListBoxService { get; set; }
        CheckedListBox checkedListBoxWorkGroup { get; set; }
        CheckedListBox checkedListBoxCreator { get; set; }
        CheckedListBox checkedListBoxExecutant { get; set; }

        ToolStripButton ToolStripButtonUpdate { get; set; }
        string TextBoxId { get; set; }
        int FormX1 { get; set; }
        int FormX2 { get; set; }
        int FormY1 { get; set; }
        int FormY2 { get; set; }
        int NumericUpDownUpdateInterval { get; set; }
        bool CheckBoxUpdateState { get; set; }
        int SplitContainerSpliter1Distance { get; set; }
        int SplitContainerSpliter2Distance { get; set; }
        int ListViewTaskColumnId { get; set; }
        int ListViewTaskColumnDate { get; set; }
        int ListViewTaskColumnTheme { get; set; }
        int ListViewTaskColumnDescription { get; set; }
        int ListViewTaskColumnCreator { get; set; }
        int ListViewTaskColumnExecutant { get; set; }
        int ListViewTaskColumnStatus { get; set; }
        int ListViewTaskColumnService { get; set; }
        int ListViewTaskColumnWorkGroup { get; set; }
        int[] ListViewTaskDisplayIndexsColumns { get; set; }
        int ListViewCommentColumnDate { get; set; }
        int ListViewCommentColumnUser { get; set; }
        int ListViewCommentColumnDescription { get; set; }
        int ListViewHistoryColumnDate { get; set; }
        int ListViewHistoryColumnUser { get; set; }
        int ListViewHistoryColumnDescription { get; set; }
        int ListViewTaskFontSize { get; set; }
        int ListViewHistoryFontSize { get; set; }
        int ListViewCommentFontSize { get; set; }
        Form mainForm { get; }

        //СОБЫТИЯ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        event EventHandler<EventArgs> checkBoxAutoUpdateCheckedChanged;
        event EventHandler<EventArgs> formClosing;
        event EventHandler<EventArgs> formLoad;
        event EventHandler<EventArgs> menuItemExportCSVClick;
        event EventHandler<EventArgs> asyncButtonsClick;
        event EventHandler<EventArgs> toolStripButtonConnectionSettingsClick;
        event EventHandler<EventArgs> toolStripButtonReportsClick;
        event EventHandler<EventArgs> contextMenuTaskItemsClick;
        event EventHandler<EventArgs> listViewTaskSelectedIndexChange;
        event EventHandler<EventArgs> listViewTaskDoubleClick;
        event EventHandler<EventArgs> listViewCommentDoubleClick;

        event EventHandler<TelegramEventArgs> listViewCloseSelectedTask;
        event EventHandler<EventArgs> listViewExportExcelSelectedTask;
        event EventHandler<EventArgs> listViewExportWordSelectedTask;
        
        event EventHandler<ColumnClickEventArgs> listViewTaskColumnClick;
        event EventHandler<RetrieveVirtualItemEventArgs> listViewRetrieveVirtualItem;
        event EventHandler<RetrieveVirtualItemEventArgs> listViewCommentRetrieveVirtualItem;
        event EventHandler<RetrieveVirtualItemEventArgs> listViewHistoryRetrieveVirtualItem;
        event EventHandler<DrawListViewSubItemEventArgs> listViewTaskDrawSubItem;

        //МЕТОДЫ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        void listViewTaskInvalidate();
        void listViewCommentInvalidate();
        void listViewHistoryInvalidate();
        void FullScreen();
        void CloseForm();
    }
}

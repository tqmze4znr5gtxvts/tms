﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskApp.FormEventAcknowledge
{
    public partial class EventAcknowledgeForm : Form, IEventAcknowledgeForm
    {
        public EventAcknowledgeForm()
        {
            InitializeComponent();
        }

        TextBox IEventAcknowledgeForm.textBoxAcknowledgeComment
        {
            get { return textBoxAcknowledgeComment; }
            set { textBoxAcknowledgeComment = value; }
        }

        string IEventAcknowledgeForm.textBoxEventCreationTime
        {
            get { return textBoxEventCreationTime.Text; }
            set { textBoxEventCreationTime.Text = value; }
        }

        string IEventAcknowledgeForm.textBoxEventCreator
        {
            get { return textBoxEventCreator.Text; }
            set { textBoxEventCreator.Text = value; }
        }

        string IEventAcknowledgeForm.textBoxEventDescription
        {
            get { return textBoxEventDescription.Text; }
            set { textBoxEventDescription.Text = value; }
        }

        string IEventAcknowledgeForm.textBoxEventTime
        {
            get { return textBoxEventTime.Text; }
            set { textBoxEventTime.Text = value; }
        }

        string IEventAcknowledgeForm.textBoxEventType
        {
            get { return textBoxEventType.Text; }
            set { textBoxEventType.Text = value; }
        }

        string IEventAcknowledgeForm.textBoxTaskDescription
        {
            get { return textBoxTaskDescription.Text; }
            set { textBoxTaskDescription.Text = value; }
        }

        string IEventAcknowledgeForm.textBoxTaskId
        {
            get { return textBoxTaskId.Text; }
            set { textBoxTaskId.Text = value; }
        }

        string IEventAcknowledgeForm.textBoxTaskTheme
        {
            get { return textBoxTaskTheme.Text; }
            set { textBoxTaskTheme.Text = value; }
        }

        public bool buttonOKEnable
        {
            get { return buttonOK.Enabled; }
            set { buttonOK.Enabled = value; }
        }

        public bool buttonCancelEnable
        {
            get { return buttonCancel.Enabled; }
            set { buttonCancel.Enabled = value; }
        }

        public string formText
        {
            get { return this.Text; }
            set { this.Text = value; }
        }

        public bool buttonCloseAndOffVisable
        {
            get { return buttonCloseAndOff.Visible; }
            set { buttonCloseAndOff.Visible = value; }
        }

        public event EventHandler<EventArgs> buttonsClick;
        public event EventHandler<EventArgs> formLoad;
        public event EventHandler<EventArgs> textBoxAcknowledgeCommentTextChanged;
        public event EventHandler<EventArgs> formActivated;

        private void EventAcknowledgeForm_Load(object sender, EventArgs e)
        {
            this.ActiveControl = textBoxAcknowledgeComment;
            formLoad?.Invoke(sender, e);
        }

        private void buttons_Click(object sender, EventArgs e)
        {
            buttonsClick?.Invoke(sender, e);
        }

        public void formClose()
        {
            this.Close();
        }

        private void textBoxAcknowledgeComment_TextChanged(object sender, EventArgs e)
        {
            textBoxAcknowledgeCommentTextChanged?.Invoke(sender, e);
        }

        private void EventAcknowledgeForm_Activated(object sender, EventArgs e)
        {
            formActivated?.Invoke(sender, e);
        }
    }
}

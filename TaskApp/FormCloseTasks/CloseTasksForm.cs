﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskApp.FormCloseTasks
{
    public partial class CloseTasksForm : Form, ICloseTasksForm
    {
        CheckBox ICloseTasksForm.checkBoxSendMessageTelegram
        {
            get { return checkBoxSendMessageTelegram; }
            set { checkBoxSendMessageTelegram = value; }
        }

        public CloseTasksForm()
        {
            InitializeComponent();
        }

        public event EventHandler<EventArgs> asyncButtonsClick;

        private void buttonYes_Click(object sender, EventArgs e)
        {
            asyncButtonsClick?.Invoke(sender, e);
        }

        private void buttonNo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void FormClose()
        {
             if (this.InvokeRequired)
                 this.Invoke(new Action(() => Close()));
             else
                 Close();
        }

    }
}

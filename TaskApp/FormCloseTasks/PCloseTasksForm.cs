﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskApp.FormCloseTasks
{
    class PCloseTasksForm
    {
        ICloseTasksForm _closeTasksForm;
        //MCloseTasksForm mCloseTasksForm;
        EventHandler<TelegramEventArgs> _listViewCloseSelectedTask;

        public PCloseTasksForm(CloseTasksForm closeTasksForm, EventHandler<TelegramEventArgs> listViewCloseSelectedTask)
        {
            _closeTasksForm = closeTasksForm;
            _listViewCloseSelectedTask = listViewCloseSelectedTask;
            //_pMainForm = pMainForm;
            _closeTasksForm.asyncButtonsClick += _commentForm_asyncButtonsClick;
        }

        private void _commentForm_asyncButtonsClick(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            if (button.Text == "Да")
            {
                TelegramEventArgs tea = new TelegramEventArgs();
                tea.telegramAnswer = _closeTasksForm.checkBoxSendMessageTelegram.Checked;
                //Перебор в выбранном списке и закрытие задач
                _listViewCloseSelectedTask?.Invoke(sender, tea);
                _closeTasksForm.FormClose();
            }
            else
            {
                _closeTasksForm.FormClose();
            }
        }
    }
}

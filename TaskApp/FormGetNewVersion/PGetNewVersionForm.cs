﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UpdateMe;

namespace TaskApp.FormGetNewVersion
{
    public class PGetNewVersionForm
    {
        PMainForm _pMainForm;
        MGetNewVersionForm mGetNewVersionForm;
        IGetNewVersionForm _getNewVersionForm;

        public event EventHandler<EventArgs> CloseApplication;

        public PGetNewVersionForm(IGetNewVersionForm getNewVersionForm, PMainForm pMainForm)
        {
            mGetNewVersionForm = new MGetNewVersionForm();
            _getNewVersionForm = getNewVersionForm;
            _pMainForm = pMainForm;


            _getNewVersionForm.formLoad += _getNewVersionForm_formLoad;
            _getNewVersionForm.buttonsClick += _getNewVersionForm_buttonsClick;
        }

        private void _getNewVersionForm_buttonsClick(object sender, EventArgs e)
        {
            update.installUpdateRestart(_pMainForm.model.Version[3], _pMainForm.model.Version[4], "\"" + Application.StartupPath + "\\", MGetNewVersionForm.processToEnd, MGetNewVersionForm.processToEnd, "updated", MGetNewVersionForm.updater);
            //_getNewVersionForm.CloseForm();
            CloseApplication?.Invoke(null, EventArgs.Empty);
        }

        private void _getNewVersionForm_formLoad(object sender, EventArgs e)
        {
            _getNewVersionForm.textBoxCurrentVersion = _pMainForm.model.GetProgramVersion();
            _getNewVersionForm.textBoxAvailableVersion = _pMainForm.model.Version.Count > 1 ? _pMainForm.model.Version[1] : "";

        }
    }
}

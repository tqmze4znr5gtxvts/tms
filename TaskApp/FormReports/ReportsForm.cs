﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskApp.FormReports
{
    public partial class ReportsForm : Form, IReportsForm
    {
        public ReportsForm()
        {
            InitializeComponent();
        }

        string IReportsForm.textBoxInWork
        {
            get { return textBoxInWork.Text; }
            set { textBoxInWork.Text = value; }
        }

        string IReportsForm.textBoxClosed
        {
            get { return textBoxClosed.Text; }
            set { textBoxClosed.Text = value; }
        }

        string IReportsForm.textBoxComming
        {
            get { return textBoxComming.Text; }
            set { textBoxComming.Text = value; }
        }

        CheckedListBox IReportsForm.checkedListBoxCreator
        {
            get { return checkedListBoxCreator; }
            set { checkedListBoxCreator = value; }
        }

        CheckedListBox IReportsForm.checkedListBoxExecutant
        {
            get { return checkedListBoxExecutant; }
            set { checkedListBoxExecutant = value; }
        }

        CheckedListBox IReportsForm.checkedListBoxService
        {
            get { return checkedListBoxService; }
            set { checkedListBoxService = value; }
        }

        CheckedListBox IReportsForm.checkedListBoxStatus
        {
            get { return checkedListBoxStatus; }
            set { checkedListBoxStatus = value; }
        }

        CheckedListBox IReportsForm.checkedListBoxWorkGroup
        {
            get { return checkedListBoxWorkGroup; }
            set { checkedListBoxWorkGroup = value; }
        }

        public DateTime DateBefore
        {
            get { return DateTime.Parse(dateTimePickerStartDate.Value.ToString("yyyy-MM-dd") + " " + dateTimePickerStartTime.Value.ToString("HH:mm:ss")); }
        }

        public DateTime DateAfter
        {
            get { return DateTime.Parse(dateTimePickerEndDate.Value.ToString("yyyy-MM-dd") + " " + dateTimePickerEndTime.Value.ToString("HH:mm:ss")); }
        }

        public int Critical
        {
            get { return comboBoxCritical.SelectedIndex; }
        }

        ListView IReportsForm.listViewReportTask
        {
            get { return listViewReportCome; }
            set { listViewReportCome = value; }
        }

        Button IReportsForm.buttonCreate
        {
            get { return buttonCreate; }
            set { buttonCreate = value; }
        }

        public GroupBox gb1
        {
            get { return groupBox1; }
            set { groupBox1 = value; }
        }

        public GroupBox gb2
        {
            get { return groupBox2; }
            set { groupBox2 = value; }
        }

        public GroupBox gb3
        {
            get { return groupBox3; }
            set { groupBox3 = value; }
        }

        public GroupBox gb4
        {
            get { return groupBox4; }
            set { groupBox4 = value; }
        }

        public ListView listViewReportClosed
        {
            get { return listViewFinished; }
            set { listViewFinished = value; }
        }

        ListView IReportsForm.listViewAtWork
        {
            get { return listViewAtWork; }
            set { listViewAtWork = value; }
        }

        TabControl IReportsForm.tabControlReport
        {
            get { return tabControlReport; }
            set { tabControlReport = value; }
        }

        public event EventHandler<EventArgs> buttonsClick;

        private void listViewTask_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            if(e.Header.Width == 0)
            {
                e.Header.Text = "";
            }
            else
            {
                using (StringFormat sf = new StringFormat())
                {
                    switch (e.Header.TextAlign)
                    {
                        case HorizontalAlignment.Center:
                            sf.Alignment = StringAlignment.Center;
                            break;
                        case HorizontalAlignment.Left:
                            sf.Alignment = StringAlignment.Center;
                            break;
                    }

                    Rectangle rct = e.Bounds;
                    rct.Height = rct.Height;

                    e.Graphics.FillRectangle(Brushes.DarkBlue, rct);
                    e.Graphics.DrawRectangle(new Pen(Color.Gray, 1), rct);

                    using (Font headerFont = new Font("Helvetica", 9, FontStyle.Bold))
                    {
                        e.Graphics.DrawString(e.Header.Text, headerFont,
                            Brushes.White, rct, sf);
                    }
                }
                return;
            }

        }

        private void ReportsForm_Load(object sender, EventArgs e)
        {
            comboBoxCritical.SelectedIndex = 0;
            dateTimePickerEndTime.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);

            showHidewFilterGroup();
        }

        private void buttons_Click(object sender, EventArgs e)
        {
            buttonsClick?.Invoke(sender, e);
        }

        private void listViewReportTask_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void checkedListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            CheckedListBox chlb = (CheckedListBox)sender;

            var selText = chlb.SelectedItem;

            if (selText != null)
            {
                chlb.ItemCheck -= checkedListBox_ItemCheck;
                if (e.Index == 0)
                //if (((CheckListBoxItem)selText).Text == "Все")
                {
                    for (int i = 1; i < chlb.Items.Count; i++)
                    {
                        chlb.SetItemChecked(i, e.NewValue == CheckState.Checked ? true : false);
                    }
                }
                else
                {
                    if (e.NewValue == CheckState.Unchecked)
                    {
                        chlb.SetItemChecked(0, false);
                    }
                }
                chlb.ItemCheck += checkedListBox_ItemCheck;
            }
        }

        private void listViewFinished_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void listViewAtWork_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void tabControlReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            showHidewFilterGroup();
        }

        private void showHidewFilterGroup()
        {
            if (tabControlReport.SelectedIndex==0)
            {
                groupBox1.Visible = false;
                groupBox2.Visible = false;
                groupBox3.Visible = false;
            }
            else
            {
                groupBox1.Visible = true;
                groupBox2.Visible = true;
                groupBox3.Visible = true;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskApp.FormReports
{
    public class MReportsForm
    {
        public Dictionary<int, ReportTask> ReportTaskList { get; set; }
        public Dictionary<int, ReportTask> ReportClosedTaskList { get; set; }
        public Dictionary<int, ReportTask> ReportAtWorkTaskList { get; set; }

        public MReportsForm()
        {
            ReportTaskList = new Dictionary<int, ReportTask>();
            ReportAtWorkTaskList = new Dictionary<int, ReportTask>();
        }
    }
}

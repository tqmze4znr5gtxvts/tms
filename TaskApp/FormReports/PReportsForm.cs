﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace TaskApp.FormReports
{
    public class PReportsForm
    {
        IReportsForm reportsForm;
        PMainForm _pMainForm;
        MReportsForm mReportsForm;

        public PReportsForm(IReportsForm reportsForm, PMainForm pMainForm, CheckedListBox checkedListBoxStatus, CheckedListBox checkedListBoxService, CheckedListBox checkedListBoxWorkGroup, CheckedListBox checkedListBoxCreator, CheckedListBox checkedListBoxExecutant)
        {
            this.reportsForm = reportsForm;
            _pMainForm = pMainForm;
            mReportsForm = new MReportsForm();

            //Передача списка инициаторов и исполнителей, производиться через копирование списков checkBox из основного окна
            InitCheckedListBox(checkedListBoxStatus, checkedListBoxService, checkedListBoxWorkGroup, checkedListBoxCreator, checkedListBoxExecutant);

            reportsForm.buttonsClick += ReportsForm_buttonsClick;
        }

        private void ReportsForm_buttonsClick(object sender, EventArgs e)
        {
            if (((Button)sender).Name == "buttonCreate")
            {
                reportsForm.gb1.Enabled = false;
                reportsForm.gb2.Enabled = false;
                reportsForm.gb3.Enabled = false;
                reportsForm.gb4.Enabled = false;
                List<Status> status = new List<Status>();
                List<Service> service = new List<Service>();
                List<WorkGroup> workGroup = new List<WorkGroup>();
                List<User> creatorUser = new List<User>();
                List<User> executantUser = new List<User>();

                foreach (var val in reportsForm.checkedListBoxStatus.CheckedItems)
                {
                    if (val.ToString() == "Все")
                        continue;
                    var temp = _pMainForm.model._Status.FirstOrDefault(v2 => v2.Value.Name == val.ToString());
                    if (temp.Value != null)
                        status.Add(temp.Value);
                    else
                        status.Add(new Status(0, ""));
                }

                foreach (var val in reportsForm.checkedListBoxService.CheckedItems)
                {
                    if (val.ToString() == "Все")
                        continue;
                    var temp = _pMainForm.model._Service.FirstOrDefault(v2 => v2.Value.Name == val.ToString());
                    if (temp.Value != null)
                        service.Add(temp.Value);
                    else
                        service.Add(new Service(0, ""));
                }

                foreach (var val in reportsForm.checkedListBoxWorkGroup.CheckedItems)
                {
                    if (val.ToString() == "Все")
                        continue;
                    var temp = _pMainForm.model._WorkGroup.FirstOrDefault(v2 => v2.Value.Name == val.ToString());
                    if (temp.Value != null)
                        workGroup.Add(temp.Value);
                    else
                        workGroup.Add(new WorkGroup(0, ""));
                }

                foreach (var val in reportsForm.checkedListBoxCreator.CheckedItems)
                {
                    if (val.ToString() == "Все")
                        continue;
                    var temp = _pMainForm.model._User.FirstOrDefault(v2 => v2.Value.FullName == val.ToString());
                    if (temp.Value != null)
                        creatorUser.Add(temp.Value);
                    else
                        creatorUser.Add(new User(0, "", "", "", "", 0, false, 0, false));
                }

                foreach (var val in reportsForm.checkedListBoxExecutant.CheckedItems)
                {
                    if (val.ToString() == "Все")
                        continue;
                    var temp = _pMainForm.model._ExecutantUser.FirstOrDefault(v2 => v2.Value.FullName == val.ToString());
                    if (temp.Value != null)
                        executantUser.Add(temp.Value);
                    else
                        executantUser.Add(new User(0, "", "", "", "", 0, false, 0, false));
                }

                excelExportDelegate sd = GetReportTask;
                IAsyncResult asyncRes = sd.BeginInvoke(reportsForm.DateBefore, reportsForm.DateAfter, reportsForm.Critical, status, service, workGroup, creatorUser, executantUser, new AsyncCallback(CallbackMethod), null);

               // GetReportTask(reportsForm.DateBefore, reportsForm.DateAfter, reportsForm.Critical, status, service, workGroup, creatorUser, executantUser);
               
            }
            else
            {
                if (((Button)sender).Name == "buttonExportExcel")
                {
                    Excel.Application xlApp = new Excel.Application();
                    if (xlApp == null)
                    {
                        MessageBox.Show("На данном ПК Excel не установлен. Экспорт не возможен!");
                        return;
                    }
                    else
                    {
                        SaveFileDialog sfd = new SaveFileDialog();
                        sfd.FileName = "Детализация " + DateTime.Now.ToString("dd.MM.yy");
                        if (sfd.ShowDialog() == DialogResult.OK)
                        {
                            Marshal.ReleaseComObject(xlApp);
                            reportsForm.listViewReportTask.Tag = "Поступившие";
                            reportsForm.listViewAtWork.Tag = "Общее в работе";
                            reportsForm.listViewReportClosed.Tag = "Общее устранено";
                            FormProgress.ProgressForm progressForm = new FormProgress.ProgressForm();
                            FormProgress.PProgressForm pProgressForm = new FormProgress.PProgressForm(progressForm, _pMainForm, new ListView[] { reportsForm.listViewAtWork, reportsForm.listViewReportClosed, reportsForm.listViewReportTask }, sfd.FileName + ".xlsx");
                            progressForm.ShowDialog();
                        }
                    }
                }
                else
                if (((Button)sender).Name == "buttonExportExcelFast")
                {
                        SaveFileDialog sfd = new SaveFileDialog();
                        sfd.FileName = "Детализация " + DateTime.Now.ToString("dd.MM.yy");
                        if (sfd.ShowDialog() == DialogResult.OK)
                        {
                            reportsForm.listViewReportTask.Tag = "Поступившие";
                            reportsForm.listViewAtWork.Tag = "Общее в работе";
                            reportsForm.listViewReportClosed.Tag = "Общее устранено";
                            FormProgress.ProgressForm progressForm = new FormProgress.ProgressForm();
                            FormProgress.PProgressFormFast pProgressForm = new FormProgress.PProgressFormFast(progressForm, _pMainForm, new ListView[] { reportsForm.listViewAtWork, reportsForm.listViewReportClosed, reportsForm.listViewReportTask }, sfd.FileName + ".xlsx");
                            progressForm.ShowDialog();
                        }
                }
                else
                {
                    if (((Button)sender).Name == "buttonExportCSV")
                    {
                        SaveFileDialog sfd = new SaveFileDialog();
                        sfd.FileName = "Детализация " + DateTime.Now.ToString("dd.MM.yy");
                        if (sfd.ShowDialog() == DialogResult.OK)
                        {
                            _pMainForm.model.ExportCSV((ListView)reportsForm.tabControlReport.SelectedTab.Controls[0], sfd.FileName + ".csv");
                        }
                    }
                }
            }
        }

        private void CallbackMethod(IAsyncResult asyncRes)
        {
            if (reportsForm.buttonCreate.InvokeRequired)
                reportsForm.buttonCreate.Invoke(new Action(() =>
                {
                    reportsForm.gb1.Enabled = true;
                    reportsForm.gb2.Enabled = true;
                    reportsForm.gb3.Enabled = true;
                    reportsForm.gb4.Enabled = true;

                    reportsForm.textBoxClosed = reportsForm.listViewReportClosed.Items.Count.ToString();
                    reportsForm.textBoxComming = reportsForm.listViewReportTask.Items.Count.ToString();
                    reportsForm.textBoxInWork = reportsForm.listViewAtWork.Items.Count.ToString();
                }));
            else
            {
                reportsForm.gb1.Enabled = true;
                reportsForm.gb2.Enabled = false;
                reportsForm.gb3.Enabled = false;
                reportsForm.gb4.Enabled = false;
            }
        }

        delegate void excelExportDelegate(DateTime dateBefore, DateTime dateAfter, int critical, List<Status> status, List<Service> service, List<WorkGroup> workGroup, List<User> creatorUser, List<User> executantUser);

        private bool getComments(MySqlConnection conn, MySqlCommand myCommand, Dictionary<int, ReportTask> reportTaskList)
        {
            try
            {
                using (MySqlDataReader Reader = myCommand.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        int id = Reader.GetInt32(0);
                        string tempDt = Reader.GetValue(3).ToString();
                        var dt = tempDt == "00.00.0000 0:00:00" ? new DateTime(2016, 01, 01, 0, 0, 0, 0) : DateTime.Parse(tempDt);
                        int userID = Reader.GetInt32(1);
                        var arrayUser = _pMainForm.model._User.Where(v => v.Value.Id == userID).ToArray();
                        if (arrayUser.Length <= 0)
                            continue;
                        User user = arrayUser[0].Value;
                        //var arrTask = TaskList.Where(v => v.Id == id).ToArray();
                        var arrTask = reportTaskList.Where(v => v.Key == id).Select(v => v.Value).ToArray();
                        ReportTask task = arrTask.Count() > 0 ? arrTask[0] : null;
                        if (task == null)
                            continue;
                        string text = Reader.GetValue(2).ToString();
                        Comment comment = new Comment(task, user, text, dt);
                        task.Comments.Add(comment);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "getComments.txt");
                throw ex;
            }
            return true;
        }

        private bool getHistory(MySqlConnection conn, MySqlCommand myCommand, Dictionary<int, ReportTask> reportTaskList)
        {
            try
            {
                using (MySqlDataReader Reader = myCommand.ExecuteReader())
                {
                    while (Reader.Read())
                    {
                        int id = Reader.GetInt32(0);
                        string tempDt = Reader.GetValue(1).ToString();
                        var dt = tempDt == "00.00.0000 0:00:00" ? new DateTime(2016, 01, 01, 0, 0, 0, 0) : DateTime.Parse(tempDt);
                        int userID = Reader.GetInt32(2);

                        var arrayUser = _pMainForm.model._User.Where(v => v.Value.Id == userID).ToArray();
                        if (arrayUser.Length <= 0)
                            continue;

                        User user = arrayUser[0].Value;
                        string text = Reader.GetValue(3).ToString();
                        var arrTask = reportTaskList.Where(v => v.Key == id).Select(v => v.Value).ToArray();
                        Task task = arrTask.Count() > 0 ? arrTask[0] : null;
                        if (task == null)
                            continue;
                        History history = new History(task, user, text, dt);
                        task._History.Add(history);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "getHistory.txt");
                throw ex;
            }
            return true;
        }

        private string getTasksIdForSqlWhere(string idName, Dictionary<int, ReportTask> reportTaskList)
        {
            string res = "";

            foreach (var task in reportTaskList)
            {
                res += idName + "=" + task.Value.Id + " OR ";
            }

            return res.Length > 0 ? " WHERE " + res.Substring(0, res.Length - 4) : "";
        }

        private void GetReportTask(DateTime dateBefore, DateTime dateAfter, int critical, List<Status> status, List<Service> service, List<WorkGroup> workGroup, List<User> creatorUser, List<User> executantUser)
        {
            string commandTextTask = "SELECT * FROM task WHERE CreationDate BETWEEN STR_TO_DATE('" + dateBefore.ToString("yyyy-MM-dd HH:mm:ss") + "', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('" +
                dateAfter.ToString("yyyy-MM-dd HH:mm:ss") + "', '%Y-%m-%d %H:%i:%s')";

            string commandTextClosed = "SELECT * FROM task, history WHERE task.TaskID=history.ID and (history.DateChange between STR_TO_DATE('" + dateBefore.ToString("yyyy-MM-dd HH:mm:ss") + "', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('" +
                dateAfter.ToString("yyyy-MM-dd HH:mm:ss") + "', '%Y-%m-%d %H:%i:%s')) AND (history.Description LIKE '%Новое значение: \"Закрыт\"' OR  history.Description LIKE '%новое значение: Закрыт')";

            string commandTextAtWork = "SELECT * FROM task WHERE Status <> 3";

            commandTextTask += critical == 1 ? " AND Critical='0'" : (critical == 2 ? " AND Critical='1'" : "");
            commandTextClosed += critical == 1 ? " AND Critical='0'" : (critical == 2 ? " AND Critical='1'" : "");


            if (status.Count > 0)
            {
                commandTextTask += " AND (";
                for (int i = 0; i < status.Count; i++)
                {
                    commandTextTask += "Status=" + status[i].Id.ToString() + " OR ";
                }
                commandTextTask = commandTextTask.Substring(0, commandTextTask.Length - 4);
                commandTextTask += ")";
            }

            if (service.Count > 0)
            {
                commandTextTask += " AND (";
                commandTextClosed += " AND (";
                for (int i = 0; i < service.Count; i++)
                {
                    commandTextTask += service[i].Id == 0 ? "Service is null OR " : "Service=" + service[i].Id.ToString() + " OR ";
                    commandTextClosed += service[i].Id == 0 ? "Service is null OR " : "Service=" + service[i].Id.ToString() + " OR ";
                }
                commandTextTask = commandTextTask.Substring(0, commandTextTask.Length - 4);
                commandTextClosed = commandTextClosed.Substring(0, commandTextClosed.Length - 4);
                commandTextTask += ")";
                commandTextClosed += ")";
            }

            if (workGroup.Count > 0)
            {
                commandTextTask += " AND (";
                commandTextClosed += " AND (";
                for (int i = 0; i < workGroup.Count; i++)
                {
                    commandTextTask += workGroup[i].Id == 0 ? "Workgroup is null OR " : "Workgroup=" + workGroup[i].Id.ToString() + " OR ";
                    commandTextClosed += workGroup[i].Id == 0 ? "Workgroup is null OR " : "Workgroup=" + workGroup[i].Id.ToString() + " OR ";
                }
                commandTextTask = commandTextTask.Substring(0, commandTextTask.Length - 4);
                commandTextClosed = commandTextClosed.Substring(0, commandTextClosed.Length - 4);
                commandTextTask += ")";
                commandTextClosed += ")";
            }

            if (creatorUser.Count > 0)
            {
                commandTextTask += " AND (";
                commandTextClosed += " AND (";
                for (int i = 0; i < creatorUser.Count; i++)
                {
                    commandTextTask += creatorUser[i].Id == 0 ? "CreatorUserID is null OR " : "CreatorUserID=" + creatorUser[i].Id.ToString() + " OR ";
                    commandTextClosed += creatorUser[i].Id == 0 ? "CreatorUserID is null OR " : "CreatorUserID=" + creatorUser[i].Id.ToString() + " OR ";
                }
                commandTextTask = commandTextTask.Substring(0, commandTextTask.Length - 4);
                commandTextClosed = commandTextClosed.Substring(0, commandTextClosed.Length - 4);
                commandTextTask += ")";
                commandTextClosed += ")";
            }

            if (executantUser.Count > 0)
            {
                commandTextTask += " AND (";
                commandTextClosed += " AND (";
                for (int i = 0; i < executantUser.Count; i++)
                {
                    commandTextTask += executantUser[i].Id == 0 ? "ExecutantUserID is null OR " : "ExecutantUserID=" + executantUser[i].Id.ToString() + " OR ";
                    commandTextClosed += executantUser[i].Id == 0 ? "ExecutantUserID is null OR " : "ExecutantUserID=" + executantUser[i].Id.ToString() + " OR ";
                }
                commandTextTask = commandTextTask.Substring(0, commandTextTask.Length - 4);
                commandTextClosed = commandTextClosed.Substring(0, commandTextClosed.Length - 4);
                commandTextTask += ")";
                commandTextClosed += ")";
            }

            using (MySqlConnection conn = new MySqlConnection(_pMainForm.model.ConnectionString))
            {
                try
                {
                    conn.Open();
                    MySqlCommand myCommand = new MySqlCommand(commandTextTask, conn);
                    mReportsForm.ReportTaskList = _pMainForm.model.getReports(conn, myCommand);
                    
                    string sqlComment = "SELECT taskId, userId, comentText, date FROM comment";
                    string sqlHistory = "SELECT * FROM history";

                    sqlComment += getTasksIdForSqlWhere("taskID", mReportsForm.ReportTaskList) + " ORDER BY date";
                    sqlHistory += getTasksIdForSqlWhere("ID", mReportsForm.ReportTaskList) + " ORDER BY DateChange";

                    myCommand = new MySqlCommand(sqlComment, conn);
                    getComments(conn, myCommand, mReportsForm.ReportTaskList);
                    myCommand = new MySqlCommand(sqlHistory, conn);
                    getHistory(conn, myCommand, mReportsForm.ReportTaskList);
                    
                    myCommand = new MySqlCommand(commandTextClosed, conn);
                    mReportsForm.ReportClosedTaskList = _pMainForm.model.getReports(conn, myCommand);

                    sqlComment = "SELECT taskId, userId, comentText, date FROM comment";
                    sqlHistory = "SELECT * FROM history";

                    sqlComment += getTasksIdForSqlWhere("taskID", mReportsForm.ReportClosedTaskList) + " ORDER BY date";
                    sqlHistory += getTasksIdForSqlWhere("ID", mReportsForm.ReportClosedTaskList) + " ORDER BY DateChange";

                    myCommand = new MySqlCommand(sqlComment, conn);
                    getComments(conn, myCommand, mReportsForm.ReportClosedTaskList);
                    myCommand = new MySqlCommand(sqlHistory, conn);
                    getHistory(conn, myCommand, mReportsForm.ReportClosedTaskList);
                    
                    myCommand = new MySqlCommand(commandTextAtWork, conn);
                    mReportsForm.ReportAtWorkTaskList = _pMainForm.model.getReports(conn, myCommand);

                    sqlComment = "SELECT taskId, userId, comentText, date FROM comment";
                    sqlHistory = "SELECT * FROM history";
                    sqlComment += getTasksIdForSqlWhere("taskID", mReportsForm.ReportAtWorkTaskList) + " ORDER BY date";
                    sqlHistory += getTasksIdForSqlWhere("ID", mReportsForm.ReportAtWorkTaskList) + " ORDER BY DateChange";

                    myCommand = new MySqlCommand(sqlComment, conn);
                    getComments(conn, myCommand, mReportsForm.ReportAtWorkTaskList);
                    myCommand = new MySqlCommand(sqlHistory, conn);
                    getHistory(conn, myCommand, mReportsForm.ReportAtWorkTaskList);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                    if (reportsForm.buttonCreate.InvokeRequired)
                        reportsForm.buttonCreate.Invoke(new Action(() =>
                        {
                            reportsForm.gb1.Enabled = true;
                            reportsForm.gb2.Enabled = true;
                            reportsForm.gb3.Enabled = true;
                            reportsForm.gb4.Enabled = true;
                        }));
                    else
                    {
                        reportsForm.gb1.Enabled = true;
                        reportsForm.gb2.Enabled = true;
                        reportsForm.gb3.Enabled = true;
                        reportsForm.gb4.Enabled = true;
                    }
                }

                var tempReportsClosed = mReportsForm.ReportClosedTaskList.Where(v => v.Value._Status.Name == "Закрыт").OrderBy(v=>v.Value.Id);
                var tempReportAccepted = mReportsForm.ReportTaskList.Where(v => v.Value._Status.Name == "Принят").OrderBy(v => v.Value.Id);
                
                foreach (var reportTask in mReportsForm.ReportAtWorkTaskList)
                {
                    if (reportTask.Value._History.Count > 0)
                    {
                        var accept = reportTask.Value._History.Where(v => v.Description.Contains("Новое значение: \"Принят\"") || v.Description.Contains("новое значение: Принят")).ToArray();
                        if (accept.Length > 0)
                        {
                            reportTask.Value.AcceptedDate = accept[0]._DateTime;
                        }
                    }
                }

                foreach (var reportTask in mReportsForm.ReportClosedTaskList)
                {
                    if (reportTask.Value._History.Count > 0)
                    {
                        var accept = reportTask.Value._History.Where(v => v.Description.Contains("Новое значение: \"Принят\"") || v.Description.Contains("новое значение: Принят")).ToArray();
                        if (accept.Length > 0)
                        {
                            reportTask.Value.AcceptedDate = accept[0]._DateTime;
                        }
                    }
                }

                foreach (var reportTask in tempReportsClosed)
                {
                    if (reportTask.Value.Comments.Count > 0)
                        reportTask.Value.EndUnswer = reportTask.Value.Comments[reportTask.Value.Comments.Count - 1].CommentText;
                    var history = reportTask.Value._History.Where(v => v.Description.Contains("Новое значение: \"Закрыт\"") || v.Description.Contains("новое значение: Закрыт")).ToArray();
                    if (history.Length > 0)
                        reportTask.Value.FinishDate = history[history.Length - 1]._DateTime;
                    
                }

                foreach (var reportTask in mReportsForm.ReportTaskList)
                {
                    if (reportTask.Value._History.Count > 0)
                    {
                        var accept = reportTask.Value._History.Where(v => v.Description.Contains("Новое значение: \"Принят\"") || v.Description.Contains("новое значение: Принят")).ToArray();
                        if (accept.Length > 0)
                        {
                            reportTask.Value.AcceptedDate = accept[0]._DateTime;
                        }

                        var close = reportTask.Value._History.Where(v => v.Description.Contains("Новое значение: \"Закрыт\"") || v.Description.Contains("новое значение: Закрыт")).ToArray();
                        if (close.Length > 0)
                        {
                            reportTask.Value.FinishDate = close[0]._DateTime;
                        }
                    }
                }

                //var tempReportsNotClosed = mReportsForm.ReportTaskList;//.Where(v => v.Value._Status.Name != "Закрыт");
                var tempReportsClosed2 = mReportsForm.ReportClosedTaskList.Where(v => v.Value._Status.Name == "Закрыт").OrderBy(v => v.Value.Id);

                foreach (var reportTask in mReportsForm.ReportTaskList)
                {
                    if (reportTask.Value.FinishDate.Year >= 2016)
                        reportTask.Value.ExecutingTime = (reportTask.Value.FinishDate - reportTask.Value.CreationDate).StripMilliseconds();
                }

                foreach (var reportTask in tempReportsClosed2)
                {
                    reportTask.Value.ExecutingTime = (reportTask.Value.FinishDate - reportTask.Value.CreationDate).StripMilliseconds();
                }
                
                reportsForm.listViewAtWork.Invoke(new Action(() =>
                {
                    reportsForm.listViewAtWork.Items.Clear();
                    foreach (var report in mReportsForm.ReportAtWorkTaskList)
                    {
                        reportsForm.listViewAtWork.Items.Add(new ListViewItem(new string[] { report.Value.Id.ToString(), report.Value._WorkGroup == null ? "" : report.Value._WorkGroup.Name, report.Value._Source.Name, report.Value.Theme,
                            report.Value._Service == null ? "" : report.Value._Service.Name, report.Value.Text, report.Value.Critical ? "Да" : "Нет", report.Value._Status.Name, report.Value.EndUnswer, report.Value.CreatorUser.FullName,
                            report.Value.ExecutantUser == null ? "" : report.Value.ExecutantUser.FullName, report.Value.CreationDate.ToString("dd-MM-yyyy HH:mm:ss"), report.Value.AcceptedDate.ToString("dd-MM-yyyy HH:mm:ss"), report.Value.FinishDate.ToString("dd-MM-yyyy HH:mm:ss"), report.Value.ExecutingTime.ToString()}));
                    }
                }));

                reportsForm.listViewReportClosed.Invoke(new Action(() =>
                {
                    reportsForm.listViewReportClosed.Items.Clear();
                    foreach (var report in tempReportsClosed2)
                    {
                        reportsForm.listViewReportClosed.Items.Add(new ListViewItem(new string[] { report.Value.Id.ToString(), report.Value._WorkGroup == null ? "" : report.Value._WorkGroup.Name, report.Value._Source.Name, report.Value.Theme,
                        report.Value._Service == null ? "" : report.Value._Service.Name, report.Value.Text, report.Value.Critical ? "Да" : "Нет", report.Value._Status.Name, report.Value.EndUnswer, report.Value.CreatorUser.FullName,
                        report.Value.ExecutantUser == null ? "" : report.Value.ExecutantUser.FullName, report.Value.CreationDate.ToString("dd-MM-yyyy HH:mm:ss"), report.Value.AcceptedDate.ToString("dd-MM-yyyy HH:mm:ss"), report.Value.FinishDate.ToString("dd-MM-yyyy HH:mm:ss"), report.Value.ExecutingTime.ToString()}));
                    }
                }));

                reportsForm.listViewReportTask.Invoke(new Action(() =>
                {
                    reportsForm.listViewReportTask.Items.Clear();
                    foreach (var report in mReportsForm.ReportTaskList)
                    {
                        reportsForm.listViewReportTask.Items.Add(new ListViewItem(new string[] { report.Value.Id.ToString(), report.Value._WorkGroup == null ? "" : report.Value._WorkGroup.Name, report.Value._Source.Name, report.Value.Theme,
                        report.Value._Service == null ? "" : report.Value._Service.Name, report.Value.Text, report.Value.Critical ? "Да" : "Нет", report.Value._Status.Name, report.Value.EndUnswer, report.Value.CreatorUser.FullName,
                        report.Value.ExecutantUser == null ? "" : report.Value.ExecutantUser.FullName, report.Value.CreationDate.ToString("dd-MM-yyyy HH:mm:ss"), report.Value.AcceptedDate.ToString("dd-MM-yyyy HH:mm:ss"), report.Value.FinishDate.ToString("dd-MM-yyyy HH:mm:ss"), report.Value.ExecutingTime.ToString()}));
                    }
                }));
            }
        }

        private void InitCheckedListBox(CheckedListBox checkedListBoxStatus, CheckedListBox checkedListBoxService, CheckedListBox checkedListBoxWorkGroup, CheckedListBox checkedListBoxCreator, CheckedListBox checkedListBoxExecutant)
        {
            foreach (var item in checkedListBoxStatus.Items)
            {
                reportsForm.checkedListBoxStatus.Items.Add(item, true);
            }
            foreach (var item in checkedListBoxService.Items)
            {
                reportsForm.checkedListBoxService.Items.Add(item, true);
            }
            foreach (var item in checkedListBoxWorkGroup.Items)
            {
                reportsForm.checkedListBoxWorkGroup.Items.Add(item, true);
            }
            foreach (var item in checkedListBoxCreator.Items)
            {
                reportsForm.checkedListBoxCreator.Items.Add(item, true);
            }
            foreach (var item in checkedListBoxExecutant.Items)
            {
                reportsForm.checkedListBoxExecutant.Items.Add(item, true);
            }
        }
    }

    public static class TimeExtensions
    {
        public static TimeSpan StripMilliseconds(this TimeSpan time)
        {
            return new TimeSpan(time.Days, time.Hours, time.Minutes, time.Seconds);
        }
    }
}

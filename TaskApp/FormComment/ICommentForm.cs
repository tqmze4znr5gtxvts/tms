﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskApp.FormComment
{
    public interface ICommentForm
    {
        //СВОЙСТВА//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        string TextBoxCommentText { get; set; }
        bool TextBoxCommentEnabled { get; set; }
        bool ButtonOkEnabled { get; set; }
        bool PictureBoxVisable { get; set; }
        string FormText { set; }
        RadioButton radioButtonText { get; set; }
        RadioButton radioButtonFilePath { get; set; }
        TextBox textBoxFilePath { get; set; }
        Button buttonSelectFile { get; set; }

        //СОБЫТИЯ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        event EventHandler<EventArgs> asyncButtonsClick;

        //МЕТОДЫ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        void formClose(bool flag = false);

    }
}

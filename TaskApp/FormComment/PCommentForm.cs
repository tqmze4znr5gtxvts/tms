﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskApp.FormComment
{
    public class PCommentForm
    {
        PMainForm _pMainForm;
        ICommentForm _commentForm;
        MCommentForm mCommentForm;


        public PCommentForm(CommentForm commentForm, PMainForm pMainForm, Task task)
        {
            _commentForm = commentForm;
            _pMainForm = pMainForm;
            mCommentForm = new MCommentForm(task);
            _commentForm.FormText = "Комментарий к задаче: " + task.Id;
            _commentForm.asyncButtonsClick += _commentForm_asyncButtonsClick;

            _commentForm.radioButtonFilePath.Enabled = true;
            _commentForm.radioButtonText.Enabled = true;

            _commentForm.radioButtonFilePath.CheckedChanged += RadioButtonFilePath_CheckedChanged;

            _commentForm.radioButtonText.Checked = true;
        }

        private void RadioButtonFilePath_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = (RadioButton)sender;

            if (rb.Checked)
            {
                _commentForm.TextBoxCommentEnabled = false;
                _commentForm.textBoxFilePath.Enabled = true;
                _commentForm.buttonSelectFile.Enabled = true;
            }
            else
            {
                _commentForm.TextBoxCommentEnabled = true;
                _commentForm.textBoxFilePath.Enabled = false;
                _commentForm.buttonSelectFile.Enabled = false;
            }
        }

        public PCommentForm(CommentForm commentForm, PMainForm pMainForm, Task task, Comment comment)
        {
            _commentForm = commentForm;
            _pMainForm = pMainForm;
            mCommentForm = new MCommentForm(task, comment);
            _commentForm.FormText = "Комментарий к задаче: " + task.Id;
            _commentForm.TextBoxCommentText = comment.CommentText;
            _commentForm.ButtonOkEnabled = false;
            _commentForm.asyncButtonsClick += _commentForm_asyncButtonsClick;
        }

        public delegate void Delegate(Task task, string commentText);
        private void _commentForm_asyncButtonsClick(object sender, EventArgs e)
        {

            Button button = (Button)sender;
            if (button.Text == "ОК")
            {

                if (_commentForm.radioButtonText.Checked)
                {
                    _commentForm.TextBoxCommentEnabled = false;
                    _commentForm.ButtonOkEnabled = false;
                    _commentForm.PictureBoxVisable = true;

                    Delegate del = new Delegate(_pMainForm.model.addComment);
                    System.Threading.Tasks.Task newTask = System.Threading.Tasks.Task.Factory.StartNew(() => del.BeginInvoke(mCommentForm._Task, _commentForm.TextBoxCommentText, new AsyncCallback(AddCommentCallBackFunction), null));
                }
                else
                {
                    if (_commentForm.textBoxFilePath.Text != "")
                    {
                        _commentForm.radioButtonText.Enabled = false;
                        _commentForm.radioButtonFilePath.Enabled = false;
                        _commentForm.textBoxFilePath.Enabled = false;
                        _commentForm.buttonSelectFile.Enabled = false;
                        _commentForm.PictureBoxVisable = true;

                        Delegate del = new Delegate(_pMainForm.model.addAttachment);
                        System.Threading.Tasks.Task newTask = System.Threading.Tasks.Task.Factory.StartNew(() => del.BeginInvoke(mCommentForm._Task, _commentForm.textBoxFilePath.Text, new AsyncCallback(AddCommentCallBackFunction), null));
                    }
                    else
                    {
                        MessageBox.Show("Файл не выбран.");
                    }
                }
            }
            else
            {
                if (button.Text == "Отмена")
                {
                    _commentForm.formClose();
                }
                else
                {
                    OpenFileDialog fbd = new OpenFileDialog();
                    DialogResult dr = fbd.ShowDialog();

                    if (dr == DialogResult.OK)
                    {
                        _commentForm.textBoxFilePath.Text = fbd.FileName;
                    }
                }
            }
        }

        private void AddCommentCallBackFunction(IAsyncResult aRes)
        {
            _commentForm.formClose();
        }
    }
}

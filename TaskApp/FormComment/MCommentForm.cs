﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TaskApp.FormComment
{
    public class MCommentForm
    {
        public Task _Task { get; set; }
        public Comment _Comment { get; set; }

        public MCommentForm(Task task)
        {
            this._Task = task;
        }

        public MCommentForm(Task task, Comment comment)
        {
            _Task = task;
            _Comment = comment;
        }
    }
}

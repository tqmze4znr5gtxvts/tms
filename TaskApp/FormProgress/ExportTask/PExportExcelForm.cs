﻿using System;
using System.Windows.Forms;

using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Linq;
using System.Collections.Generic;

namespace TaskApp.FormProgress.ExportTask
{
    class PExportExcelForm
    {
        IProgressForm progressForm;
        //PMainForm _pMainForm;
        delegate void excelExportDelegate();

        SpreadsheetDocument spreadsheetDocument;
        WorkbookPart workbookpart;
        Sheets sheets;
        WorksheetPart worksheetPart;
        SheetData sheetData;

        bool breakFlag = false;

        List<Task> tasks;

        string fileName;

        //PMainForm pMainForm, 
        public PExportExcelForm(IProgressForm progressForm, List<Task> _tasks, string fileName)
        {
            this.fileName = fileName;
            this.progressForm = progressForm;
            //_pMainForm = pMainForm;
            this.tasks = _tasks;

            // Create a spreadsheet document by supplying the file name.
            spreadsheetDocument = SpreadsheetDocument.Create(fileName, SpreadsheetDocumentType.Workbook);

            // Add a WorkbookPart to the document.
            workbookpart = spreadsheetDocument.AddWorkbookPart();
            workbookpart.Workbook = new Workbook();

            // Add Sheets to the Workbook.
            sheets = spreadsheetDocument.WorkbookPart.Workbook.AppendChild<Sheets>(new Sheets());

            addStyles();

            /*PageSetup pageSetup = new PageSetup();
            pageSetup.Orientation = OrientationValues.Landscape;
            pageSetup.FitToHeight = 2;
            pageSetup.HorizontalDpi = 200;
            pageSetup.VerticalDpi = 200;
            worksheetPart.Worksheet.AppendChild(pageSetup);*/

            excelExportDelegate sd = ExcelExport;
            IAsyncResult asyncRes = sd.BeginInvoke(new AsyncCallback(CallbackMethod), null);

            progressForm.buttonsClick += ProgressForm_buttonsClick;
        }

        private void ProgressForm_buttonsClick(object sender, EventArgs e)
        {
            breakFlag = true;
        }

        private void CallbackMethod(IAsyncResult asyncRes)
        {
            try
            {
                if (!breakFlag)
                {
                    // Close the document.
                    spreadsheetDocument.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();
            progressForm.FormClose();
        }

        private static double GetWidth(string font, int fontSize, string text)
        {
            System.Drawing.Font stringFont = new System.Drawing.Font(font, fontSize);
            return GetWidth(stringFont, text);
        }

        private static double GetWidth(System.Drawing.Font stringFont, string text)
        {
            // This formula is based on this article plus a nudge ( + 0.2M )
            // http://msdn.microsoft.com/en-us/library/documentformat.openxml.spreadsheet.column.width.aspx
            // Truncate(((256 * Solve_For_This + Truncate(128 / 7)) / 256) * 7) = DeterminePixelsOfString

            System.Drawing.Size textSize = TextRenderer.MeasureText(text, stringFont);
            double width = (double)(((textSize.Width / (double)7) * 256) - (128 / 7)) / 256;
            width = (double)decimal.Round((decimal)width + 0.2M, 2);

            return width;
        }

        struct TextSize
        {
            public TextSize(double _width, double _height) { width = _width; height = _height; }

            public double width;
            public double height;
        }

        static System.Drawing.Size proposedSize = new System.Drawing.Size(int.MaxValue, int.MaxValue);

        //System.Drawing.Font stringFont = new System.Drawing.Font("Calibri", 11);//font, fontSize
        //double width = GetWidth(stringFont, strText);
        private static TextSize GetSizeText(System.Drawing.Font stringFont, string text)
        {
            // This formula is based on this article plus a nudge ( + 0.2M )
            // http://msdn.microsoft.com/en-us/library/documentformat.openxml.spreadsheet.column.width.aspx
            // Truncate(((256 * Solve_For_This + Truncate(128 / 7)) / 256) * 7) = DeterminePixelsOfString

            //TextFormatFlags.SingleLine
            //TextFormatFlags.VerticalCenter
            System.Drawing.Size textSize = TextRenderer.MeasureText(text, stringFont, proposedSize, TextFormatFlags.NoPadding);
            double width = (double)(((textSize.Width / (double)7) * 256) - (128 / 7)) / 256;
            width = (double)decimal.Round((decimal)width + 0.2M, 2);

            return new TextSize(width, textSize.Height);
        }

        void addStyles()
        {
            var stylesPart = spreadsheetDocument.WorkbookPart.AddNewPart<WorkbookStylesPart>();
            stylesPart.Stylesheet = new Stylesheet();

            // Font list
            // Create a bold font
            stylesPart.Stylesheet.Fonts = new Fonts();
            Font bold_font = new Font();         // Bold font
            Bold bold = new Bold();
            bold_font.Append(bold);

            // Add fonts to list
            stylesPart.Stylesheet.Fonts.AppendChild(new Font());
            stylesPart.Stylesheet.Fonts.AppendChild(bold_font); // Bold gets fontid = 1
            stylesPart.Stylesheet.Fonts.Count = 2;

            // Create fills list
            stylesPart.Stylesheet.Fills = new Fills();

            // create red fill for failed tests
            var formatRed = new PatternFill() { PatternType = PatternValues.Solid };
            formatRed.ForegroundColor = new ForegroundColor { Rgb = HexBinaryValue.FromString("FF5500") }; // red fill
            formatRed.BackgroundColor = new BackgroundColor { Indexed = 64 };

            // Create green fill for passed tests
            var formatGreen = new PatternFill() { PatternType = PatternValues.Solid };
            formatGreen.ForegroundColor = new ForegroundColor { Rgb = HexBinaryValue.FromString("99DD00") }; // green fill
            formatGreen.BackgroundColor = new BackgroundColor { Indexed = 64 };

            // Create blue fill
            var formatBlue = new PatternFill() { PatternType = PatternValues.Solid };
            formatBlue.ForegroundColor = new ForegroundColor { Rgb = HexBinaryValue.FromString("66AAFF") };
            formatBlue.BackgroundColor = new BackgroundColor { Indexed = 64 };

            // Create Light Green fill
            var formatLightGreen = new PatternFill() { PatternType = PatternValues.Solid };
            formatLightGreen.ForegroundColor = new ForegroundColor { Rgb = HexBinaryValue.FromString("F1F8E0") };
            formatLightGreen.BackgroundColor = new BackgroundColor { Indexed = 64 };

            // Append fills to list
            stylesPart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = new PatternFill { PatternType = PatternValues.None } }); // required, reserved by Excel
            stylesPart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = new PatternFill { PatternType = PatternValues.Gray125 } }); // required, reserved by Excel
            stylesPart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = formatRed }); // Red gets fillid = 2
            stylesPart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = formatGreen }); // Green gets fillid = 3
            stylesPart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = formatBlue }); // Blue gets fillid = 4, old format1
            stylesPart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = formatLightGreen }); // LightGreen gets fillid = 5, old format2
            stylesPart.Stylesheet.Fills.Count = 6;

            // Create border list
            stylesPart.Stylesheet.Borders = new Borders();

            // Create thin borders for passed/failed tests and default cells
            LeftBorder leftThin = new LeftBorder() { Style = BorderStyleValues.Thin };
            RightBorder rightThin = new RightBorder() { Style = BorderStyleValues.Thin };
            TopBorder topThin = new TopBorder() { Style = BorderStyleValues.Thin };
            BottomBorder bottomThin = new BottomBorder() { Style = BorderStyleValues.Thin };

            Border borderThin = new Border();
            borderThin.Append(leftThin);
            borderThin.Append(rightThin);
            borderThin.Append(topThin);
            borderThin.Append(bottomThin);

            // Create thick borders for headings
            LeftBorder leftThick = new LeftBorder() { Style = BorderStyleValues.Thick };
            RightBorder rightThick = new RightBorder() { Style = BorderStyleValues.Thick };
            TopBorder topThick = new TopBorder() { Style = BorderStyleValues.Thick };
            BottomBorder bottomThick = new BottomBorder() { Style = BorderStyleValues.Thick };

            Border borderThick = new Border();
            borderThick.Append(leftThick);
            borderThick.Append(rightThick);
            borderThick.Append(topThick);
            borderThick.Append(bottomThick);

            Border borderThickOnlyTop = new Border();
            borderThickOnlyTop.Append(new TopBorder() { Style = BorderStyleValues.Thin } );

            // Add borders to list
            stylesPart.Stylesheet.Borders.AppendChild(new Border());
            stylesPart.Stylesheet.Borders.AppendChild(borderThin);//1
            stylesPart.Stylesheet.Borders.AppendChild(borderThick);//2
            stylesPart.Stylesheet.Borders.AppendChild(borderThickOnlyTop);//3
            stylesPart.Stylesheet.Borders.Count = 4;


            //Добавление форматов даты
            stylesPart.Stylesheet.NumberingFormats = new NumberingFormats();

            NumberingFormat dateStyle = new NumberingFormat();
            dateStyle.NumberFormatId = UInt32Value.FromUInt32(3);
            dateStyle.FormatCode = StringValue.FromString("yyyy-MM-dd hh:mm:ss");
            stylesPart.Stylesheet.NumberingFormats.Append(dateStyle);


            // Create blank cell format list
            stylesPart.Stylesheet.CellStyleFormats = new CellStyleFormats();
            stylesPart.Stylesheet.CellStyleFormats.Count = 1;
            stylesPart.Stylesheet.CellStyleFormats.AppendChild(new CellFormat());

            // Create cell format list
            stylesPart.Stylesheet.CellFormats = new CellFormats();
            // empty one for index 0, seems to be required
            stylesPart.Stylesheet.CellFormats.AppendChild(new CellFormat());


            // cell format for failed tests, Styleindex = 1, Red fill and bold text
            stylesPart.Stylesheet.CellFormats.AppendChild(new CellFormat { FormatId = 0, FontId = 1, BorderId = 2, FillId = 2, ApplyFill = true }).AppendChild(new Alignment { Horizontal = HorizontalAlignmentValues.Center });

            // cell format for passed tests, Styleindex = 2, Green fill and bold text
            stylesPart.Stylesheet.CellFormats.AppendChild(new CellFormat { FormatId = 0, FontId = 1, BorderId = 2, FillId = 3, ApplyFill = true }).AppendChild(new Alignment { Horizontal = HorizontalAlignmentValues.Center });

            // cell format for blue background, Styleindex = 3, blue fill and bold text
            stylesPart.Stylesheet.CellFormats.AppendChild(new CellFormat { FormatId = 0, FontId = 1, BorderId = 1, FillId = 4, ApplyFill = true }).AppendChild(new Alignment { Horizontal = HorizontalAlignmentValues.Center });

            // default cell style and rest default, Styleindex = 4
            stylesPart.Stylesheet.CellFormats.AppendChild(new CellFormat { }).AppendChild(new Alignment { Horizontal = HorizontalAlignmentValues.Left, Vertical = VerticalAlignmentValues.Top });

            // default cell style and rest default, Styleindex = 5
            stylesPart.Stylesheet.CellFormats.AppendChild(new CellFormat { }).AppendChild(new Alignment { Horizontal = HorizontalAlignmentValues.Left, Vertical = VerticalAlignmentValues.Top, WrapText = true });

            // default cell style and rest default, Styleindex = 6 
            stylesPart.Stylesheet.CellFormats.AppendChild(new CellFormat { NumberFormatId = dateStyle.NumberFormatId, ApplyNumberFormat = BooleanValue.FromBoolean(true), ApplyFont = true }).AppendChild(new Alignment { Horizontal = HorizontalAlignmentValues.Center, Vertical = VerticalAlignmentValues.Top });

            // default cell style and rest default, Styleindex =7
            stylesPart.Stylesheet.CellFormats.AppendChild(new CellFormat { BorderId = 3 }).AppendChild(new Alignment { Horizontal = HorizontalAlignmentValues.Left, Vertical = VerticalAlignmentValues.Top });

            //stylesPart.Stylesheet.CellFormats.Count = 7;//Работает и так
            stylesPart.Stylesheet.Save();

            /*
            //жирность
            (sheets[l].Cells[2, i] as Excel.Range).Font.Bold = true;
            //размер шрифта
            (sheets[l].Cells[2, i] as Excel.Range).Font.Size = 12;
            //название шрифта
            (sheets[l].Cells[2, i] as Excel.Range).Font.Name = "Times New Roman";
            //цвет текста
            (sheets[l].Cells[2, i] as Excel.Range).Font.Color = Color.White;
            //стиль границы
            (sheets[l].Cells[2, i] as Excel.Range).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlDouble;
            //толщина границы
            (sheets[l].Cells[2, i] as Excel.Range).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight = Excel.XlBorderWeight.xlMedium;
            //выравнивание по горизонтали
            (sheets[l].Cells[2, i] as Excel.Range).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            (sheets[l].Cells[2, i] as Excel.Range).Interior.Color = Color.DarkBlue;
            //выравнивание по вертикали
            (sheets[l].Cells[2, i] as Excel.Range).VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
            (sheets[l].Cells[2, i] as Excel.Range).Borders.Color = Color.LightGray;*/

            // Save the stylesheet formats
            stylesPart.Stylesheet.Save();
        }

        int[] widthColumns = new int[] { 100, 25};
        Boolean[] flagBestFit = new Boolean[] { true, true};

        void addColumns()
        {
            // Create custom widths for columns
            Columns lstColumns = worksheetPart.Worksheet.GetFirstChild<Columns>();
            Boolean needToInsertColumns = false;
            if (lstColumns == null)
            {
                lstColumns = new Columns();
                needToInsertColumns = true;
            }
            //Ширина колонок задается отдельно
            //Min = 1, Max = 7 - задаются номера колонок с по
            // Min = 1, Max = 1 ==> Apply this to column 1 (A)
            // Min = 2, Max = 2 ==> Apply this to column 2 (B)
            // Width = 25 ==> Set the width to 25
            // CustomWidth = true ==> Tell Excel to use the custom width
            /*lstColumns.Append(new Column() { Min = 1, Max = 1, Width = 7, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 2, Max = 2, Width = 19, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 3, Max = 3, Width = 25, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 4, Max = 4, Width = 35, CustomWidth = true, BestFit = false });
            lstColumns.Append(new Column() { Min = 5, Max = 5, Width = 40, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 6, Max = 6, Width = 40, CustomWidth = true, BestFit = false });
            lstColumns.Append(new Column() { Min = 7, Max = 7, Width = 15, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 8, Max = 8, Width = 13, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 9, Max = 9, Width = 10, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 10, Max = 10, Width = 37, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 11, Max = 11, Width = 37, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 12, Max = 12, Width = 19, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 13, Max = 13, Width = 19, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 14, Max = 14, Width = 19, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 15, Max = 15, Width = 17, CustomWidth = true, BestFit = true });
            */

            for (uint i = 0; i < widthColumns.Length; i++)
            {
                uint a = i + 1;
                lstColumns.Append(new Column() { Min = a, Max = a, Width = widthColumns[i], CustomWidth = true, BestFit = flagBestFit[i] });
            }

            // Only insert the columns if we had to create a new columns element
            if (needToInsertColumns)
                worksheetPart.Worksheet.InsertAt(lstColumns, 0);
        }

        //Каждая ячейка вставляется один раз в таблицу - поэтому нет необходимости проверять наличие ячейки или строки
        private Cell CreateCell(Row row, String columnName, UInt32 rowIndex)
        {
            Cell cellResult = new Cell();
            cellResult.CellReference = columnName + rowIndex;

            Cell refCell = null;
            row.InsertBefore(cellResult, refCell);
            return cellResult;
        }
        private Row AddRow(UInt32 rowIndex)
        {
            Row row = new Row();
            row.RowIndex = rowIndex;
            //wsData.Append(row);
            sheetData.Append(row);
            return row;
        }

        private string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;

            while (dividend > 0)
            {
                int mod = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + mod).ToString() + columnName;
                dividend = (int)((dividend - mod) / 26);
            }

            return columnName;
        }

        private String ClearString(String text)
        {
            text = text.Replace("\b", "").Replace("\t", " ").Replace("\v", " ").Replace("\f", " ").Replace("\r", " ");
            text = text.Replace("\n\n", "\n");
            text = text.Replace("\n ", "\n");
            text = text.Replace(" \n", "\n");
            text = text.Replace("  ", " ");
            return text;
        }

        private double getRealHeightText(System.Drawing.Font stringFont, string text)
        {
            //Лучше разделять по переносам и считать размер каждой строки
            //Строки могут быть длинными и короткими
            //Сюда поместить строку включающую в себя все используемые символы А-яа-я0-9
            double minHeight = GetSizeText(stringFont, text.Replace("\n", " ")).height;
            int countNeedRow = 0;
            foreach (String partText in text.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries))
            {
                TextSize real_textSize = GetSizeText(stringFont, text);
                //double realHeight = real_textSize.height;//Высота будет одна - т.к уже нет переноса символов
                double realWidth = real_textSize.width;

                int k1 = (Int32)Math.Ceiling(realWidth / widthColumns[0]);
                countNeedRow += (k1 > 1 ? k1 : 1);
            }
            return minHeight * countNeedRow;
        }

        public void ExcelExport()
        {
            try
            {
                System.Drawing.Font stringFont = new System.Drawing.Font("Calibri", 11);//font, fontSize

                uint sheetId = 1;
                {
                    // Add a WorksheetPart to the WorkbookPart.
                    worksheetPart = workbookpart.AddNewPart<WorksheetPart>();

                    sheetData = new SheetData();
                    worksheetPart.Worksheet = new Worksheet(sheetData);

                    PageMargins pageMargins1 = new PageMargins();
                    pageMargins1.Left = 0.25D;
                    pageMargins1.Right = 0.25D;
                    pageMargins1.Top = 0.25D;
                    pageMargins1.Bottom = 0.25D;
                    pageMargins1.Header = 0.0D;//0.3D=0.8см
                    pageMargins1.Footer = 0.0D;//0.3D
                    worksheetPart.Worksheet.AppendChild(pageMargins1);


                    // Get a unique ID for the new worksheet.
                    if (sheets.Elements<Sheet>().Count() > 0)
                    {
                        sheetId = sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                    }

                    // Append a new worksheet and associate it with the workbook.
                    Sheet sheet = new Sheet()
                    {
                        Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart),
                        SheetId = sheetId,
                        Name = "Лист Задач"
                    };

                    sheets.Append(sheet);

                    addColumns();

                    uint numRow = 0;

                    Row curRow = AddRow(numRow);

                    //Если данные для отображения в списке есть, то запускается цикл прорисовки текста и фона ячеек
                    int count = tasks.Count;
                    int i = 0;
                    foreach (Task curTask in tasks)
                    {
                        Row useRow = AddRow(++numRow);

                        progressForm.progressBar.Invoke(new Action(() => progressForm.progressBar.Value = (i * 100) / count)); i++;
                        if (breakFlag)
                        {
                            break;
                        }
                        else
                        {
                            String text = "Номер: " + curTask.Id.ToString() + "       Дата: " + curTask.CreationDate.ToShortDateString() + "         " + curTask._WorkGroup.Name;

                            Cell cell = CreateCell(useRow, GetExcelColumnName(1), numRow);//Номер строки должен совпадать с номером уже созданной строки
                            cell.StyleIndex = (uint)7;
                            cell.DataType = CellValues.String;
                            cell.CellValue = new CellValue(text);

                            {
                                useRow = AddRow(++numRow);
                                cell = CreateCell(useRow, GetExcelColumnName(1), numRow);//Номер строки должен совпадать с номером уже созданной строки
                                cell.StyleIndex = (uint)5;
                                cell.DataType = CellValues.String;
                                text = ClearString(curTask.Theme);
                                cell.CellValue = new CellValue(text);

                                TextSize textSize = GetSizeText(stringFont, text.Replace("\n", " "));
                                double k = Math.Ceiling(textSize.width / widthColumns[0]);
                                double real_height = textSize.height * (k > 1 ? k : 1);
                                //double real_height = 15 * (k > 1 ? k : 1);
                                useRow.Height = real_height;
                                useRow.CustomHeight = true;

                                useRow = AddRow(++numRow);
                                cell = CreateCell(useRow, GetExcelColumnName(1), numRow);//Номер строки должен совпадать с номером уже созданной строки
                                cell.StyleIndex = (uint)5;
                                cell.DataType = CellValues.String;
                                text = ClearString(curTask.Text);
                                cell.CellValue = new CellValue(text);
                                useRow.Height = getRealHeightText(stringFont, text);
                                useRow.CustomHeight = true;

                                foreach (Comment comment in curTask.Comments)
                                {
                                    useRow = AddRow(++numRow);
                                    cell = CreateCell(useRow, GetExcelColumnName(1), numRow);//Номер строки должен совпадать с номером уже созданной строки
                                    cell.StyleIndex = (uint)5;
                                    cell.DataType = CellValues.String;
                                    text = ClearString(comment._User.FullName + "   " + comment._DateTime.ToShortDateString() + " : " + ClearString(comment.CommentText));
                                    cell.CellValue = new CellValue(text);
                                    useRow.Height = getRealHeightText(stringFont, text);
                                    useRow.CustomHeight = true;
                                }

                                //012345
                                //0123456789012345
                                //012
                                //0123456789012345   r_H r_W

                                //0123456789012345012345678901234501234567890123450123456789012345 min_H max_W

                                //01234
                                //01234567890
                                //12345
                                //012
                                //0123456789012345   r_H r_W

                                ++numRow;//Пропуск строки
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            if (breakFlag)
            {
                try
                {
                    // Close the document.
                    spreadsheetDocument.Close();
                }
                catch { }
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskApp.FormProgress
{
    public partial class ProgressForm : Form, IProgressForm
    {
        public ProgressForm()
        {
            InitializeComponent();
        }

        string IProgressForm.labelSheet
        {
            get { return labelSheet.Text; }
            set
            {
                if (labelSheet.InvokeRequired)
                    labelSheet.Invoke(new Action(() => labelSheet.Text = value));
                else
                    labelSheet.Text = value;
            }
        }

        ProgressBar IProgressForm.progressBar
        {
            get { return progressBar; }
            set
            {
                if (progressBar.InvokeRequired)
                    progressBar.Invoke(new Action(() => progressBar = value));
                else
                    progressBar = value;
            }
        }

        public event EventHandler<EventArgs> buttonsClick;

        public void FormClose()
        {
            if (this.InvokeRequired)
                this.Invoke(new Action(() => this.Close()));
            else
                Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            buttonsClick?.Invoke(sender, e);
        }
    }
}

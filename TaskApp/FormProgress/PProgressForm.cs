﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Drawing;
using System.Reflection;

namespace TaskApp.FormProgress
{
    public class PProgressForm
    {
        IProgressForm progressForm;
        PMainForm _pMainForm;
        delegate void excelExportDelegate();
        Excel.Application application;
        Excel.Workbook workbook;
        Excel.Worksheet[] sheets;

        bool breakFlag = false;

        ListView[] listViewArr;

        string fileName;

        public PProgressForm(IProgressForm progressForm, PMainForm pMainForm, ListView[] listView, string fileName)
        {
            this.fileName = fileName;
            this.progressForm = progressForm;
            _pMainForm = pMainForm;
            this.listViewArr = listView;
            sheets = new Excel.Worksheet[listViewArr.Length];
            application = new Excel.Application();
            workbook = application.Workbooks.Add(Type.Missing);

            workbook.Windows[1].Caption = fileName;
            
            excelExportDelegate sd = ExcelExport;
            IAsyncResult asyncRes = sd.BeginInvoke(new AsyncCallback(CallbackMethod), null);

            progressForm.buttonsClick += ProgressForm_buttonsClick;
        }

        private void ProgressForm_buttonsClick(object sender, EventArgs e)
        {
            breakFlag = true;
        }

        private void CallbackMethod(IAsyncResult asyncRes)
        {
            try
            {
                if (!breakFlag)
                    workbook.SaveAs(fileName);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            for (int i = 0; i < sheets.Length; i++)
            {
                if (sheets[i] != null)
                Marshal.ReleaseComObject(sheets[i]);
            }
            
            if (workbook != null)
                Marshal.ReleaseComObject(workbook);
            if (application != null)
                Marshal.ReleaseComObject(application);
            GC.Collect();
            GC.WaitForPendingFinalizers();
            progressForm.FormClose();
        }

        public void ExcelExport()
        {
            try
            {
                while(true)
                {
                    if (workbook.Sheets.Count < listViewArr.Length)
                        workbook.Sheets.Add(Type.Missing);
                    else
                        break;
                }

                for (int i = 0; i < listViewArr.Length; i++)
                {

                  //MessageBox.Show("Лист " + i.ToString() + "      " + workbook.Sheets.Count);
                    sheets[i] = workbook.Sheets[i + 1];
                 // MessageBox.Show("Лист " + i + 1);
                    sheets[i].Name = listViewArr[i].Tag.ToString();
                    
                  //MessageBox.Show(listViewArr[i].Tag.ToString());

                }
            //    MessageBox.Show("Листы созданы");
                ////////////////////////////////////////////////////////////////////////////////////////////////////////"Результат"

                for (int l = 0; l < listViewArr.Length; l++)
                {
                    progressForm.labelSheet = listViewArr[l].Tag.ToString();
                    int lvColumnsCount = listViewArr[l].Columns.Count;
                    //Цикл формирования заголовков столбцов
                    for (int i = 1; i < lvColumnsCount + 1; i++)
                    {
                        if (breakFlag)
                        {
                            break;
                        }
                        else
                        {
                            listViewArr[l].Invoke(new Action(() =>
                            {
                                sheets[l].Cells[2, i] = listViewArr[l].Columns[i - 1].Text;
                                ((Excel.Range)sheets[l].Columns[i]).ColumnWidth = 40;
                            }));

                            //жирность
                            (sheets[l].Cells[2, i] as Excel.Range).Font.Bold = true;
                            //размер шрифта
                            (sheets[l].Cells[2, i] as Excel.Range).Font.Size = 12;
                            //название шрифта
                            (sheets[l].Cells[2, i] as Excel.Range).Font.Name = "Times New Roman";
                            //цвет текста
                            (sheets[l].Cells[2, i] as Excel.Range).Font.Color = Color.White;
                            //стиль границы
                            (sheets[l].Cells[2, i] as Excel.Range).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlDouble;
                            //толщина границы
                            (sheets[l].Cells[2, i] as Excel.Range).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight = Excel.XlBorderWeight.xlMedium;
                            //выравнивание по горизонтали
                            (sheets[l].Cells[2, i] as Excel.Range).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                            (sheets[l].Cells[2, i] as Excel.Range).Interior.Color = Color.DarkBlue;
                            //выравнивание по вертикали
                            (sheets[l].Cells[2, i] as Excel.Range).VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                            (sheets[l].Cells[2, i] as Excel.Range).Borders.Color = Color.LightGray;
                            
                        }
                    }

                    //Если данные для отображения в списке есть, то запускается цикл прорисовки текста и фона ячеек
                    int count = listViewArr[l].Items.Count;

                    for (int i = 0; i < count; i++)
                    {

                        progressForm.progressBar.Invoke(new Action(() => progressForm.progressBar.Value = (i * 100) / count));
                        if (breakFlag)
                        {
                            break;
                        }
                        else
                        {
                            listViewArr[l].Invoke(new Action(() =>
                            {
                                var msg = listViewArr[l].Items[i];
                                sheets[l].Cells[i + 3, 1].NumberFormat = "@";
                                sheets[l].Cells[i + 3, 1] = msg.Text;

                                for (int j = 1; j < lvColumnsCount; j++)
                                {
                                    sheets[l].Cells[i + 3, j + 1].NumberFormat = "@";
                                    sheets[l].Cells[i + 3, j + 1] = msg.SubItems[j].Text;
                                }

                            }));
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            if (breakFlag)
            {
                try
                {
                    application.Quit();
                }
                catch { }
            }
            else
                application.Visible = true;

        }
    }
}

﻿namespace TaskApp.FormProgress
{
    partial class ProgressForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProgressForm));
            this.progressPanel = new System.Windows.Forms.Panel();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBoxWaitIndicator = new System.Windows.Forms.PictureBox();
            this.labelSheet = new System.Windows.Forms.Label();
            this.progressPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWaitIndicator)).BeginInit();
            this.SuspendLayout();
            // 
            // progressPanel
            // 
            this.progressPanel.Controls.Add(this.labelSheet);
            this.progressPanel.Controls.Add(this.buttonCancel);
            this.progressPanel.Controls.Add(this.progressBar);
            this.progressPanel.Controls.Add(this.label1);
            this.progressPanel.Controls.Add(this.pictureBoxWaitIndicator);
            this.progressPanel.Location = new System.Drawing.Point(3, 4);
            this.progressPanel.Name = "progressPanel";
            this.progressPanel.Size = new System.Drawing.Size(294, 90);
            this.progressPanel.TabIndex = 9;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(198, 58);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(88, 23);
            this.buttonCancel.TabIndex = 52;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(11, 58);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(181, 23);
            this.progressBar.TabIndex = 51;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(58, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(186, 13);
            this.label1.TabIndex = 50;
            this.label1.Text = "Данные выгружаются, ждите.";
            // 
            // pictureBoxWaitIndicator
            // 
            this.pictureBoxWaitIndicator.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pictureBoxWaitIndicator.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBoxWaitIndicator.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxWaitIndicator.Image")));
            this.pictureBoxWaitIndicator.Location = new System.Drawing.Point(13, 12);
            this.pictureBoxWaitIndicator.Name = "pictureBoxWaitIndicator";
            this.pictureBoxWaitIndicator.Size = new System.Drawing.Size(37, 40);
            this.pictureBoxWaitIndicator.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxWaitIndicator.TabIndex = 49;
            this.pictureBoxWaitIndicator.TabStop = false;
            // 
            // labelSheet
            // 
            this.labelSheet.AutoSize = true;
            this.labelSheet.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labelSheet.Location = new System.Drawing.Point(59, 39);
            this.labelSheet.Name = "labelSheet";
            this.labelSheet.Size = new System.Drawing.Size(9, 13);
            this.labelSheet.TabIndex = 53;
            this.labelSheet.Text = "|";
            // 
            // ProgressForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 98);
            this.Controls.Add(this.progressPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ProgressForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ProgressForm";
            this.progressPanel.ResumeLayout(false);
            this.progressPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWaitIndicator)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel progressPanel;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBoxWaitIndicator;
        private System.Windows.Forms.Label labelSheet;
    }
}
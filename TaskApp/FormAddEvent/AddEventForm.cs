﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskApp.FormAddEvent
{
    public partial class AddEventForm : Form, IAddEventForm
    {
        ListView IAddEventForm.listViewUsers
        {
            get { return listViewUsers; }
            set { listViewUsers = value; }
        }

        TextBox IAddEventForm.textBoxEvent
        {
            get { return textBoxEvent; }
            set { textBoxEvent = value; }
        }

        public DateTimePicker DateTimePickerEventDate
        {
            get { return dateTimePickerEventDate; }
            set { dateTimePickerEventDate = value; }
        }

        public DateTimePicker DateTimePickerEventTime
        {
            get { return dateTimePickerEventTime; }
            set { dateTimePickerEventTime = value; }
        }

        ListView IAddEventForm.listViewEvent
        {
            get { return listViewEvent; }
            set { listViewEvent = value; }
        }

        ListView IAddEventForm.listViewEventUsers
        {
            get { return listViewEventUsers; }
            set { listViewEventUsers = value; }
        }

        ComboBox IAddEventForm.comboBoxType
        {
            get { return comboBoxType; }
            set { comboBoxType = value; }
        }

        ContextMenuStrip IAddEventForm.contextMenuStripEvent
        {
            get { return contextMenuStripEvent; }
            set { contextMenuStripEvent = value; }
        }

        public AddEventForm()
        {
            InitializeComponent();
        }

        public event EventHandler<EventArgs> formLoad;
        public event EventHandler<EventArgs> buttonsClick;

        private void listViewReportTask_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void listView1_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void listView1_DrawColumnHeader_1(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void AddEventForm_Load(object sender, EventArgs e)
        {
            formLoad?.Invoke(sender, e);
        }

        private void listViewUsers_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            buttonsClick?.Invoke(sender, e);
        }

        private void listViewEventUsers_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            e.DrawDefault = true;
        }
        
        private void listViewEventUsers_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            e.DrawDefault = true;
        }
    }
}

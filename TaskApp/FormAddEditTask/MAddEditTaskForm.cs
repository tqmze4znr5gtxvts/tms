﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskApp.FormAddEditTask
{
    public class MAddEditTaskForm
    {
        public Task _Task { get; set; }

        public MAddEditTaskForm()
        {

        }

        public MAddEditTaskForm(Task task)
        {
            _Task = task;
        }
    }
}

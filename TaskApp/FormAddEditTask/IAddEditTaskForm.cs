﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskApp.FormAddEditTask
{
    public interface IAddEditTaskForm
    {
        //СВОЙСТВА//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        string TextBoxThemeText { get; set; }
        string RichTextBoxDescriptionText { get; set; }
        int ComboBoxStatusSelectedIndex { get; set; }
        Object ComboBoxReasonSelectedItem { get; set;  }
        int ComboBoxWorkGroupSelectedIndex { get; set; }
        int ComboBoxServiceSelectedIndex { get; set; }
        int ComboBoxFullNameSelectedIndex { get; set; }
        string StatusComboboxValue { get; set; }
        string ServiceComboboxValue { get; set; }
        string WorkGroupComboboxValue { get; set; }
        int ServiceComboboxSelectedIndex { get; set; }
        string FormText { get; set; }
        string ExecutantComboBoxValue { set; get; }
        bool CheckBoxCritical { get; set; }
        bool comboBoxStatusEnabled { get; set; }
        bool checkBoxInfo { get; set; }
        bool CheckBoxTaskIncident { get; set; }

        //СОБЫТИЯ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        event EventHandler<EventArgs> buttonsClick;
        event EventHandler<EventArgs> formLoad;

        event EventHandler<EventArgs> changeService;

        //МЕТОДЫ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        void ShowForm();//Метод запуска формы
        void CloseForm();
        void ComponentsEnabled(bool enable);
    }
}

﻿namespace TaskApp.FormAddEditTask
{
    partial class AddEditTaskForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddEditTaskForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pictureBoxWait = new System.Windows.Forms.PictureBox();
            this.richTextBoxDescription = new System.Windows.Forms.RichTextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBoxStatus = new System.Windows.Forms.ComboBox();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBoxTheme = new System.Windows.Forms.TextBox();
            this.groupBoxExecutor = new System.Windows.Forms.GroupBox();
            this.comboBoxFullName = new System.Windows.Forms.ComboBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.comboBoxService = new System.Windows.Forms.ComboBox();
            this.checkBoxCritical = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.comboBoxWorkGroup = new System.Windows.Forms.ComboBox();
            this.checkBoxInfo = new System.Windows.Forms.CheckBox();
            this.groupBoxRealCustomer = new System.Windows.Forms.GroupBox();
            this.comboBoxRealCustomer = new System.Windows.Forms.ComboBox();
            this.checkBoxTaskIncident = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.comboBoxReason = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWait)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBoxExecutor.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBoxRealCustomer.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.tabControl1);
            this.groupBox1.Location = new System.Drawing.Point(3, 43);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(727, 288);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Описание задачи";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 16);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(721, 269);
            this.tabControl1.TabIndex = 54;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.pictureBoxWait);
            this.tabPage1.Controls.Add(this.richTextBoxDescription);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(713, 243);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Text";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // pictureBoxWait
            // 
            this.pictureBoxWait.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBoxWait.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxWait.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxWait.Image")));
            this.pictureBoxWait.Location = new System.Drawing.Point(3, 3);
            this.pictureBoxWait.MinimumSize = new System.Drawing.Size(80, 80);
            this.pictureBoxWait.Name = "pictureBoxWait";
            this.pictureBoxWait.Size = new System.Drawing.Size(707, 237);
            this.pictureBoxWait.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxWait.TabIndex = 53;
            this.pictureBoxWait.TabStop = false;
            this.pictureBoxWait.Visible = false;
            // 
            // richTextBoxDescription
            // 
            this.richTextBoxDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxDescription.Location = new System.Drawing.Point(3, 3);
            this.richTextBoxDescription.Name = "richTextBoxDescription";
            this.richTextBoxDescription.Size = new System.Drawing.Size(707, 237);
            this.richTextBoxDescription.TabIndex = 0;
            this.richTextBoxDescription.Text = "";
            this.richTextBoxDescription.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.richTextBoxDescription_LinkClicked);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.webBrowser1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(713, 314);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Web";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(3, 3);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(707, 308);
            this.webBrowser1.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.comboBoxStatus);
            this.groupBox2.Location = new System.Drawing.Point(15, 337);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(201, 42);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Статус";
            // 
            // comboBoxStatus
            // 
            this.comboBoxStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxStatus.Enabled = false;
            this.comboBoxStatus.FormattingEnabled = true;
            this.comboBoxStatus.Location = new System.Drawing.Point(3, 16);
            this.comboBoxStatus.Name = "comboBoxStatus";
            this.comboBoxStatus.Size = new System.Drawing.Size(195, 21);
            this.comboBoxStatus.TabIndex = 10;
            this.comboBoxStatus.SelectedIndexChanged += new System.EventHandler(this.comboBoxStatus_SelectedIndexChanged);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.Location = new System.Drawing.Point(530, 403);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(183, 28);
            this.buttonOk.TabIndex = 5;
            this.buttonOk.Text = "ОК";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.ButtonsClick);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Location = new System.Drawing.Point(530, 437);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(183, 28);
            this.buttonCancel.TabIndex = 6;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.ButtonsClick);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.textBoxTheme);
            this.groupBox3.Location = new System.Drawing.Point(3, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(642, 42);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Тема (255 max)";
            // 
            // textBoxTheme
            // 
            this.textBoxTheme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxTheme.Location = new System.Drawing.Point(3, 16);
            this.textBoxTheme.MaxLength = 255;
            this.textBoxTheme.Name = "textBoxTheme";
            this.textBoxTheme.Size = new System.Drawing.Size(636, 20);
            this.textBoxTheme.TabIndex = 0;
            // 
            // groupBoxExecutor
            // 
            this.groupBoxExecutor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBoxExecutor.Controls.Add(this.comboBoxFullName);
            this.groupBoxExecutor.Location = new System.Drawing.Point(225, 380);
            this.groupBoxExecutor.Name = "groupBoxExecutor";
            this.groupBoxExecutor.Size = new System.Drawing.Size(278, 42);
            this.groupBoxExecutor.TabIndex = 3;
            this.groupBoxExecutor.TabStop = false;
            this.groupBoxExecutor.Text = "Исполнитель";
            // 
            // comboBoxFullName
            // 
            this.comboBoxFullName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxFullName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFullName.FormattingEnabled = true;
            this.comboBoxFullName.Location = new System.Drawing.Point(3, 16);
            this.comboBoxFullName.MaxDropDownItems = 20;
            this.comboBoxFullName.Name = "comboBoxFullName";
            this.comboBoxFullName.Size = new System.Drawing.Size(272, 21);
            this.comboBoxFullName.TabIndex = 4;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox5.Controls.Add(this.comboBoxService);
            this.groupBox5.Location = new System.Drawing.Point(16, 380);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(200, 42);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Сервис";
            // 
            // comboBoxService
            // 
            this.comboBoxService.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxService.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxService.FormattingEnabled = true;
            this.comboBoxService.Location = new System.Drawing.Point(3, 16);
            this.comboBoxService.MaxDropDownItems = 15;
            this.comboBoxService.Name = "comboBoxService";
            this.comboBoxService.Size = new System.Drawing.Size(194, 21);
            this.comboBoxService.TabIndex = 3;
            this.comboBoxService.SelectedIndexChanged += new System.EventHandler(this.comboBoxService_SelectedIndexChanged);
            // 
            // checkBoxCritical
            // 
            this.checkBoxCritical.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxCritical.AutoSize = true;
            this.checkBoxCritical.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBoxCritical.ForeColor = System.Drawing.Color.Red;
            this.checkBoxCritical.Location = new System.Drawing.Point(651, 19);
            this.checkBoxCritical.Name = "checkBoxCritical";
            this.checkBoxCritical.Size = new System.Drawing.Size(73, 17);
            this.checkBoxCritical.TabIndex = 4;
            this.checkBoxCritical.Text = "Критично";
            this.checkBoxCritical.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox6.Controls.Add(this.comboBoxWorkGroup);
            this.groupBox6.Location = new System.Drawing.Point(225, 337);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(278, 42);
            this.groupBox6.TabIndex = 4;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Рабочая группа";
            // 
            // comboBoxWorkGroup
            // 
            this.comboBoxWorkGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxWorkGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxWorkGroup.FormattingEnabled = true;
            this.comboBoxWorkGroup.Location = new System.Drawing.Point(3, 16);
            this.comboBoxWorkGroup.MaxDropDownItems = 15;
            this.comboBoxWorkGroup.Name = "comboBoxWorkGroup";
            this.comboBoxWorkGroup.Size = new System.Drawing.Size(272, 21);
            this.comboBoxWorkGroup.TabIndex = 3;
            this.comboBoxWorkGroup.SelectedIndexChanged += new System.EventHandler(this.comboBoxWorkGroup_SelectedIndexChanged);
            // 
            // checkBoxInfo
            // 
            this.checkBoxInfo.AutoSize = true;
            this.checkBoxInfo.Checked = true;
            this.checkBoxInfo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxInfo.ForeColor = System.Drawing.Color.Blue;
            this.checkBoxInfo.Location = new System.Drawing.Point(533, 376);
            this.checkBoxInfo.Name = "checkBoxInfo";
            this.checkBoxInfo.Size = new System.Drawing.Size(197, 17);
            this.checkBoxInfo.TabIndex = 8;
            this.checkBoxInfo.Text = "Отправить уведомление Telegram";
            this.checkBoxInfo.UseVisualStyleBackColor = true;
            // 
            // groupBoxRealCustomer
            // 
            this.groupBoxRealCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBoxRealCustomer.Controls.Add(this.comboBoxRealCustomer);
            this.groupBoxRealCustomer.Location = new System.Drawing.Point(225, 423);
            this.groupBoxRealCustomer.Name = "groupBoxRealCustomer";
            this.groupBoxRealCustomer.Size = new System.Drawing.Size(278, 42);
            this.groupBoxRealCustomer.TabIndex = 9;
            this.groupBoxRealCustomer.TabStop = false;
            this.groupBoxRealCustomer.Text = "Заказчик";
            // 
            // comboBoxRealCustomer
            // 
            this.comboBoxRealCustomer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxRealCustomer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRealCustomer.FormattingEnabled = true;
            this.comboBoxRealCustomer.Items.AddRange(new object[] {
            "Агрономы",
            "Директор",
            "Кассы",
            "Кейтеринг",
            "Клининг",
            "Клининг (освещение)",
            "Охрана",
            "СГИ",
            "ФК"});
            this.comboBoxRealCustomer.Location = new System.Drawing.Point(3, 16);
            this.comboBoxRealCustomer.MaxDropDownItems = 20;
            this.comboBoxRealCustomer.Name = "comboBoxRealCustomer";
            this.comboBoxRealCustomer.Size = new System.Drawing.Size(272, 21);
            this.comboBoxRealCustomer.TabIndex = 4;
            this.comboBoxRealCustomer.SelectedIndexChanged += new System.EventHandler(this.comboBoxRealCustomer_SelectedIndexChanged);
            // 
            // checkBoxTaskIncident
            // 
            this.checkBoxTaskIncident.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkBoxTaskIncident.AutoSize = true;
            this.checkBoxTaskIncident.Location = new System.Drawing.Point(533, 353);
            this.checkBoxTaskIncident.Name = "checkBoxTaskIncident";
            this.checkBoxTaskIncident.Size = new System.Drawing.Size(62, 17);
            this.checkBoxTaskIncident.TabIndex = 10;
            this.checkBoxTaskIncident.Text = "Задача";
            this.checkBoxTaskIncident.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox4.Controls.Add(this.comboBoxReason);
            this.groupBox4.Location = new System.Drawing.Point(16, 423);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(200, 42);
            this.groupBox4.TabIndex = 11;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Причина";
            // 
            // comboBoxReason
            // 
            this.comboBoxReason.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxReason.FormattingEnabled = true;
            this.comboBoxReason.Items.AddRange(new object[] {
            "",
            "Порча",
            "Износ"});
            this.comboBoxReason.Location = new System.Drawing.Point(3, 16);
            this.comboBoxReason.MaxDropDownItems = 20;
            this.comboBoxReason.Name = "comboBoxReason";
            this.comboBoxReason.Size = new System.Drawing.Size(194, 21);
            this.comboBoxReason.TabIndex = 4;
            // 
            // AddEditTaskForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(733, 473);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.checkBoxTaskIncident);
            this.Controls.Add(this.groupBoxRealCustomer);
            this.Controls.Add(this.checkBoxInfo);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.checkBoxCritical);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBoxExecutor);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddEditTaskForm";
            this.Text = "Создать задачу";
            this.Load += new System.EventHandler(this.AddEditTaskForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWait)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBoxExecutor.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBoxRealCustomer.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox richTextBoxDescription;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboBoxStatus;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBoxTheme;
        private System.Windows.Forms.GroupBox groupBoxExecutor;
        private System.Windows.Forms.ComboBox comboBoxFullName;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox comboBoxService;
        private System.Windows.Forms.CheckBox checkBoxCritical;
        private System.Windows.Forms.PictureBox pictureBoxWait;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox comboBoxWorkGroup;
        private System.Windows.Forms.CheckBox checkBoxInfo;
        private System.Windows.Forms.GroupBox groupBoxRealCustomer;
        private System.Windows.Forms.ComboBox comboBoxRealCustomer;
        private System.Windows.Forms.CheckBox checkBoxTaskIncident;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox comboBoxReason;
    }
}
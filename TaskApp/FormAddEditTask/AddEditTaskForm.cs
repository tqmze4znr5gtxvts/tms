﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaskApp.FormAddEditTask;

namespace TaskApp.FormAddEditTask
{
    public partial class AddEditTaskForm : Form, IAddEditTaskForm
    {
        public AddEditTaskForm()
        {
            InitializeComponent();

            //Для обычного пользователя скрыты ComboBox Заказчик и CheckBox Задача
            groupBoxRealCustomer.Visible = SELECT_VERSION.visibleRealCustomer;
            checkBoxTaskIncident.Visible = SELECT_VERSION.visibleRealCustomer;
        }

        public int ComboBoxFullNameSelectedIndex
        {
            get { return comboBoxFullName.SelectedIndex; }
            set { comboBoxFullName.SelectedIndex = value; }
        }

        public int ComboBoxStatusSelectedIndex
        {
            get { return comboBoxStatus.SelectedIndex; }
            set { comboBoxStatus.SelectedIndex = value; }
        }

        public Object ComboBoxReasonSelectedItem
        {
            get { return comboBoxReason.SelectedItem; }
            set
            {
                String str = value.ToString();
                foreach(Object item in comboBoxReason.Items)
                {
                    if ((item.ToString().Length>0) &&
                        (str.Contains(item.ToString())))
                    {
                        comboBoxReason.SelectedItem = item;
                        break;
                    }
                }
            }
        }

        public string RichTextBoxDescriptionText
        {
            get
            {
                string returnText = "";
                if (richTextBoxDescription.InvokeRequired)
                    richTextBoxDescription.Invoke(new Action(() => returnText = richTextBoxDescription.Text));
                else
                    returnText = richTextBoxDescription.Text;
                return returnText;
            }
            set
            {
                if (richTextBoxDescription.InvokeRequired)
                    richTextBoxDescription.Invoke(new Action(()=> richTextBoxDescription.Text = value));
                else
                    richTextBoxDescription.Text = value;
            }
        }

        public string TextBoxThemeText
        {
            get { return textBoxTheme.Text; }
            set { textBoxTheme.Text = value; }
        }

        public string StatusComboboxValue
        {
            get { return comboBoxStatus.Text; }
            set { comboBoxStatus.Items.Add(value); }
        }

        public string ExecutantComboBoxValue
        {
            get { return comboBoxFullName.Text; }
            set { comboBoxFullName.Items.Add(value); }
        }

        public string ServiceComboboxValue
        {
            get { return comboBoxService.Text; }
            set { comboBoxService.Items.Add(value); }
        }

        public int ComboBoxServiceSelectedIndex
        {
            get { return comboBoxService.SelectedIndex; } 
            set { comboBoxService.SelectedIndex = value; }
        }

        public bool CheckBoxCritical
        {
            get { return checkBoxCritical.Checked; }
            set { checkBoxCritical.Checked = value; }
        }

        public bool comboBoxStatusEnabled
        {
            get { return comboBoxStatus.Enabled; }
            set { comboBoxStatus.Enabled = value; }
        }

        public string FormText
        {
            get { return Text; }
            set { Text = value; }
        }

        public int ServiceComboboxSelectedIndex
        {
            get { return comboBoxService.SelectedIndex; }
            set { comboBoxService.SelectedIndex = value; }
        }

        public int ComboBoxWorkGroupSelectedIndex
        {
            get { return comboBoxWorkGroup.SelectedIndex; }
            set { comboBoxWorkGroup.SelectedIndex = value; }
        }

        public string WorkGroupComboboxValue
        {
            get { return comboBoxWorkGroup.Text; }
            set { comboBoxWorkGroup.Items.Add(value); }
        }

        bool IAddEditTaskForm.checkBoxInfo
        {
            get { return checkBoxInfo.Checked; }
            set { checkBoxInfo.Checked = value; }
        }

        public bool CheckBoxTaskIncident
        {
            get { return checkBoxTaskIncident.Checked; }
            set { checkBoxTaskIncident.Checked = value; }
        }





        public event EventHandler<EventArgs> buttonsClick;
        public event EventHandler<EventArgs> formLoad;
        public event EventHandler<EventArgs> changeService;

        public void ShowForm()
        {
            Show();
        }

        private void ButtonsClick(object sender, EventArgs e)
        {
            buttonsClick?.Invoke(sender, e);
        }

        private void AddEditTaskForm_Load(object sender, EventArgs e)
        {
            this.ActiveControl = textBoxTheme;
            formLoad?.Invoke(sender, e);
        }

        public void CloseForm()
        {
            if (this.InvokeRequired)
                this.Invoke(new Action(() =>
                {
                    try
                    {
                        Close();
                    }
                    catch { }
                }));
            else
                Close();
        }

        void IAddEditTaskForm.ComponentsEnabled(bool enable)
        {
            comboBoxStatus.Enabled = enable;
            textBoxTheme.Enabled = enable;
            checkBoxCritical.Enabled = enable;
            richTextBoxDescription.Enabled = enable;
            comboBoxService.Enabled = enable;
            comboBoxFullName.Enabled = enable;
            buttonOk.Enabled = enable;
            pictureBoxWait.Visible = !enable;
        }

        private void richTextBoxDescription_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(e.LinkText);
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            webBrowser1.DocumentText = richTextBoxDescription.Text;
        }

        //checkBoxTaskIncident

        private void comboBoxRealCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            string newCustomer = "Заказчик: " + comboBoxRealCustomer.Text;
            //Если выбран какой-то текст, но при этом не выбора всего поля - заменяем его
            if ((textBoxTheme.SelectedText.Length > 0) && (textBoxTheme.SelectedText.Length!= textBoxTheme.Text.Length))
            {
                textBoxTheme.SelectedText = newCustomer;
                return;
            }
            else
            {
                int index = textBoxTheme.Text.ToLower().IndexOf("заказчик:");
                if (index!=-1)
                {
                    textBoxTheme.Text = textBoxTheme.Text.Substring(0, index) + newCustomer;
                }
                else
                {
                    textBoxTheme.Text = (textBoxTheme.Text + " " + newCustomer);//.Trim();
                }
            }
        }

        /// <summary>
        /// Ограничение числа исполнителей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxService_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxFullName.SelectedIndex = -1;
            comboBoxFullName.Items.Clear();

            changeService?.Invoke(sender, e);
        }

        private void comboBoxWorkGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxFullName.SelectedIndex = -1;
            comboBoxFullName.Items.Clear();

            changeService?.Invoke(sender, e);
        }

        private void comboBoxStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxFullName.SelectedIndex = -1;
            comboBoxFullName.Items.Clear();

            if (comboBoxStatus.Text.Equals("Согласование"))
            {
                groupBoxExecutor.Text = "Согласующий";
            }
            else
            {
                groupBoxExecutor.Text = "Исполнитель";
            }

            changeService?.Invoke(sender, e);
        }

    }
}
